package structs;

import java.awt.Color;

public class NbIntersectionsManager {

	private static NbIntersectionsManager instance = null;
	private static int currentNbIntersections = 0;
	private static final int maxIntersections = 17000;	
	
	public static int MaxSoFar;

	protected NbIntersectionsManager() {
	}

	public static NbIntersectionsManager getInstance() {
		if (instance == null) {
			instance = new NbIntersectionsManager();
		}
		return instance;
	}

	public static void increaseNbIntersections() {
		currentNbIntersections++;
	}

	public static Color getColorFromNbIntersections() {		
		MaxSoFar = Math.max(currentNbIntersections, MaxSoFar);
		
		int r = 0;
		int g = 0;
		int b = 0;
		
		float percent = ((float)currentNbIntersections/maxIntersections);
		
		if(percent < 0.25f)
		{
			b = (int) ((float)percent*4*255);
		}
		else if(percent < 0.5f)
		{
			g = (int)((float)percent*2*255);
		}
		else
		{
			r = (int) ((float)percent*255);
		}
		
		if(percent > 1)
		{
			r = 255;
			g = 255;
			b = 255;
		}
		
		currentNbIntersections = 0;

		return new Color(r, g, b);
	}

}
