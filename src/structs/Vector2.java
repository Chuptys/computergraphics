package structs;

/**
 * Represents a 2d vector in Cartesian coordinates.
 * @author Simon
 *
 */
public class Vector2 {
	public float X;
	public float Y;
	
	public Vector2(float x, float y)
	{
		X = x;
		Y = y;
	}
	
	public static float Dot(Vector2 a, Vector2 b)
	{
		return a.X*b.X + a.Y*b.Y;
	}
	
	public void multiply(float a)
	{
		X *= a;
		Y *= a;
	}
}
