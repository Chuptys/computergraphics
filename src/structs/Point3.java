package structs;

public class Point3 {

	public int X;
	public int Y;
	public int Z;

	public Point3(int x, int y, int z) {
		X = x;
		Y = y;
		Z = z;
	}

	public static Point3 fromVector3(Vector3 v) {
		return new Point3((int)v.X,(int)v.Y,(int)v.Z);
	}

}
