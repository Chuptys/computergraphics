package structs;

import java.awt.Color;

/**
 * Represents a 3d vector in cartesian coordinates.
 * 
 * @author Simon
 * 
 */
public class Vector3 {
	public float X;
	public float Y;
	public float Z;

	public Vector3(float x, float y, float z) {
		X = x;
		Y = y;
		Z = z;
	}

	public Vector3 multiply(float a) {
		return new Vector3(a * X, a * Y, a * Z);
	}

	public float dot(Vector3 v) {
		return X * v.X + Y * v.Y + Z * v.Z;
	}

	public Vector3 cross(Vector3 v) {
		return new Vector3(Y * v.Z - Z * v.Y, Z * v.X - X * v.Z, X * v.Y - Y * v.X);
	}

	public Vector3 plus(Vector3 a) {
		return new Vector3(X + a.X, Y + a.Y, Z + a.Z);
	}

	public Vector3 minus(Vector3 a) {
		return plus(a.multiply(-1));
	}

	public Vector3 transform(Matrix m) {
		float[][] data = m.getData();
		float retX = data[0][0] * X + data[0][1] * Y + data[0][2] * Z + data[0][3];
		float retY = data[1][0] * X + data[1][1] * Y + data[1][2] * Z + data[1][3];
		float retZ = data[2][0] * X + data[2][1] * Y + data[2][2] * Z + data[2][3];

		Vector3 ret = new Vector3(retX, retY, retZ);
		ret.discretize();
		return ret;
	}

	private void discretize() {
		if (Math.abs(X) < 0.01f)
			X = 0;
		if (Math.abs(Y) < 0.01f)
			Y = 0;
		if (Math.abs(Z) < 0.01f)
			Z = 0;
	}

	public float length() {
		return (float) Math.sqrt(X * X + Y * Y + Z * Z);
	}

	public void normalize() {
		float length = length();
		X = X / length;
		Y = Y / length;
		Z = Z / length;
	}

	public Vector3(Color c) {
		X = c.getRed();
		Y = c.getGreen();
		Z = c.getBlue();
	}
	
	public void copyTo(Vector3 v)
	{
		v.X = X;
		v.Y = Y;
		v.Z = Z;
	}

}
