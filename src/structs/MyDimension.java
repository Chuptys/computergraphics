package structs;

public class MyDimension {
	
	public int width;
	public int height;
	
	public MyDimension(int w, int h)
	{
		width = w + 16;
		height = h + 38;
	}

}
