package structs;
import java.awt.Color;


public class StandardValues {

	public static final Color DIFFUSE_COLOR = new Color(200,200,200);
	public static final Color SPECULAR_COLOR = new Color(25,25,25);
	public static final float SPECULAR_INTENSITY = 16;
	public static final Vector3 REFRACTIVE_ATTENUATION = new Vector3(0.1f,0.1f,0.1f);
	public static final float REFRACTIVE_INDEX = 2;
	public static final Color BACKGROUNDCOLOR_01 = new Color(170,223,233);
	public static final Color BACKGROUNDCOLOR_02 = new Color(100,220,200);
	public static final float GLOSS_AMOUNT = 0.1f;
}
