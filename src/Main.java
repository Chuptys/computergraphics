import test.TestAnimation;
import test.TestAntialiasing;
import test.TestBSPTree;
import test.TestBVH;
import test.TestCompactGrid;
import test.TestDepthOfField;
import test.TestEnvironmentMapping;
import test.TestFalseColorImage;
import test.TestGlossyReflection;
import test.TestMatrix;
import test.TestObjParser;
import test.TestPostProcessing;
import test.TestRasterizer;
import test.TestRaytracer;
import test.TestRefraction;
import test.TestSceneGraph;
import test.TestShading;
import test.TestSoftShadows;
import test.TestTexturing;

/**
 * The main entry point for this application.
 * @author Simon
 */
public class Main {

	/**
	 * Main entry point
	 */
	public static void main(String[] args) {
//		testMatrix();
		//testRasterizer();
//		testRaytracer();
		//testObjParser();
//		testShading();
//		testPostProcessing();
//		testCompactGrid();
//		testAntialiasing();
//		testSoftShadows();
//		testDepthOfField();		
//		testBVHNodes();
//		testBSPTree(); //Deprecated		
//		testFalseColorImage();		
//		testTexturing();		
//		testAnimation();
//		testRefraction();
//		testGlossyReflection();
//		testEnvironmentMapping();
//		testSceneGraph();
		
		demo();
	}


	private static void demo() {
		Demo demo = new Demo();
//		demo.stonehenge();
//		demo.reventon();
//		demo.accStatistics01();
//		demo.accStatistics02();
//		demo.accCorrectness();
		demo.basics();
		
	}
	
	private static void testSceneGraph()
	{
		TestSceneGraph test = new TestSceneGraph();
		test.testBasics();
	}

	private static void testEnvironmentMapping() {
		TestEnvironmentMapping test = new TestEnvironmentMapping();
		test.testBasics();
	}

	private static void testGlossyReflection() {
		TestGlossyReflection test = new TestGlossyReflection();
		test.testBasics();
		
	}

	private static void testRefraction() {
		TestRefraction test = new TestRefraction();
		test.testBasics();
//		test.testTriangleMesh();
//		test.test01();
	}
	
	private static void testAnimation() {
		TestAnimation test = new TestAnimation();
//		test.testBasics();
//		test.testTriangleMesh();
//		test.testTriangleMeshInGrid();
		test.testRodwen();
	}

	private static void testTexturing() {
		TestTexturing test = new TestTexturing();
//		test.testTextureSphere();
//		test.testTextureBasicTriangleMesh();
//		test.testRenderTriangleMesh();
//		test.testBasicBump();
//		test.testDiffuseBump();
		test.testSphereBump();
		
	}
	
	private static void testFalseColorImage() {
		TestFalseColorImage test = new TestFalseColorImage();
//		test.basicTest();
		test.basicTest02();
//		test.basicTest03();
		
	}
	
	private static void testBVHNodes()
	{
		TestBVH test = new TestBVH();
//		test.testRenderObjectsInGrid();
		test.stressTest();
//		test.testAddTriangleMesh();
	}
	
	@Deprecated
	private static void testBSPTree()
	{
		TestBSPTree test = new TestBSPTree();
//		test.testInitialize();
		test.testRenderObjectsInTree();
	}
	
	private static void testDepthOfField()
	{
		TestDepthOfField test = new TestDepthOfField();
		test.basicTest();
	}
	
	private static void testSoftShadows()
	{
		TestSoftShadows test  = new TestSoftShadows();
		test.testAreaLight();
	}
	
	private static void testAntialiasing()
	{
		TestAntialiasing test = new TestAntialiasing();
		test.basicTest();	
	}

	private static void testCompactGrid() {
		TestCompactGrid test = new TestCompactGrid();
		
//		test.testAddObjectsAndInitialize();
//		test.testRenderObjectsInGrid();
//		test.testShowObjectsAdded();
//		test.stressTest();
//		test.testAddTriangleMesh();
		test.testDisableSomeShadows();
//		test.uberTest();
		
//		test.testBug();
		
	}

	private static void testPostProcessing()
	{
		TestPostProcessing test = new TestPostProcessing();
//		test.testGodRays();
		test.stonehenge();
	}
	
	private static void testShading()
	{
		TestShading test = new TestShading();
//		test.testLambertianShading();
//		test.testLambertianShadingObj();
//		test.testAmbientLight();
//		test.testPhongShading();
		test.testShadows();
//		test.testPointLight();
//		test.testReflection();		
//		test.testBug();
		
//		test.uberTest();
	}
	
	private static void testObjParser()
	{
		TestObjParser test = new TestObjParser();
		test.testParseBasicObj();
	}
	
	private static void testRaytracer()
	{
		TestRaytracer test = new TestRaytracer();
		//test.testHalfCube();
		//test.testSphere();
		//test.testImportedObj();
		test.testScene();
	}
	
	private static void testRasterizer()
	{
		TestRasterizer test = new TestRasterizer();
		//test.RenderOneTriangle();
		//test.RenderTwoAdjacentTriangles();		
		test.RenderTwoAdjacentTrianglesOnOffScreenPointLine();
	}

	private static void testMatrix()
	{
		TestMatrix test = new TestMatrix();
		//test.testMultiply();
		//test.testVectorInteraction();
		test.testTranslateObj();
	}
}
