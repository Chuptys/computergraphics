package acceleration;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.Vector3;
import worldobjects.BoundingBox;
import worldobjects.Plane;

@Deprecated
public class BSPNode implements IBSPElement {

	private Plane plane;
	private IBSPElement left;
	private IBSPElement right;
	private int axis; // (x,y,z) = (0,1,2)
	private float D;
	private BoundingBox box;

	public BSPNode(BoundingBox bb, int depthLeft, MyWorldObjectList objects) {
		box = bb;
		axis = depthLeft % 3;
		float D = (getVectorPart(axis, bb.maximum) - getVectorPart(axis, bb.minimum)) * 0.5f;
		setPlane(bb);

		BoundingBox lbb = new BoundingBox(bb.minimum, getVectorWithPartSet(axis, D, bb.maximum));
		BoundingBox rbb = new BoundingBox(getVectorWithPartSet(axis, D, bb.minimum), bb.maximum);
		if (depthLeft != 0) {
			left = new BSPNode(lbb, depthLeft - 1, objects);
			right = new BSPNode(rbb, depthLeft - 1, objects);
		} else {
			left = new BSPLeaf(lbb, objects);
			right = new BSPLeaf(rbb, objects);
		}
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if(!box.isHit(ray, minDist, maxDist, result))
			return false;
		
		// http://pages.cpsc.ucalgary.ca/~mario/courses/591-691-W06/PR/3-ray-tracing/2-modeling-acceleration/readings/havran98fast.pdf
		
		//float[] dists = getAllIntersectDists(ray, minDist, maxDist); 
		float a = 0; // dist from origin to entrypoint
		float b = 0; // " to exitpoint
		
		
		
		float s = getDistanceToMidPlane(ray); // to midplane

		IBSPElement near = right;
		IBSPElement far = left;

		if (getVectorPart(axis, ray.Origin) < D) {
			near = left;
			far = right;
		}

		if (s < 0 || s > b)
			return near.isHit(ray, minDist, maxDist, result);
		
		else
		{
			if(s < a)
				return far.isHit(ray, minDist, maxDist, result);
			
			if(near.isHit(ray, minDist, maxDist, result))
				return true;
			
			return far.isHit(ray, minDist, maxDist, result);				
		}
		
		
		
		

//		float dirAxis = getVectorPart(axis, ray.Direction);
//		float u = getVectorPart(axis, ray.Origin) + minDist * dirAxis;
//
//		if (u < D) {
//			if (dirAxis < 0)
//				return left.isHit(ray, minDist, maxDist, result);
//
//			if (left.isHit(ray, minDist, maxDist, result))
//				return true;
//
//			return right.isHit(ray, minDist, maxDist, result);
//		} else {
//			if (dirAxis > 0)
//				return right.isHit(ray, minDist, maxDist, result);
//
//			if (right.isHit(ray, minDist, maxDist, result))
//				return true;
//
//			return left.isHit(ray, minDist, maxDist, result);
//		}
	}

	private float getDistanceToMidPlane(Ray ray) {
		
		RaycastResult res = new RaycastResult();
		plane.isHit(ray, 0, Float.POSITIVE_INFINITY, res);
		
		return res.distance;
	}

	private float getVectorPart(int nb, Vector3 v) {
		if (nb == 0)
			return v.X;
		if (nb == 1)
			return v.Y;
		return v.Z;
	}

	private Vector3 getVectorWithPartSet(int nb, float f, Vector3 v) {
		Vector3 ret = new Vector3(v.X, v.Y, v.Z);
		if (nb == 0)
			ret.X = f;
		else if (nb == 1)
			ret.Y = f;
		else
			ret.Z = f;

		return ret;
	}

	private Vector3 getNormalAlongAxis() {
		if (axis == 0)
			return new Vector3(1, 0, 0);
		if (axis == 1)
			return new Vector3(0, 1, 0);
		return new Vector3(0, 0, 1);
	}

	private void setPlane(BoundingBox bb) {
		plane = new Plane(getVectorWithPartSet(axis, D, bb.minimum), getNormalAlongAxis());
	}

	@Override
	public int nbObjectReferences() {
		return left.nbObjectReferences() + right.nbObjectReferences();
	}

	@Override
	public int nbNodes() {
		return left.nbNodes() + right.nbNodes() + 1;
	}

}
