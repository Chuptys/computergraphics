package acceleration;

import raytracing.Ray;
import raytracing.RaycastResult;

@Deprecated
public interface IBSPElement {
	
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result);
	public int nbNodes();
	public int nbObjectReferences();
}
