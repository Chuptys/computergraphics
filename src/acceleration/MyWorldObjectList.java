package acceleration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import worldobjects.IWorldObject;
import worldobjects.TriangleMesh;

/**
 * Acts as a list of worldobjects, where triangles of a trianglemesh can be stored as references
 * @author Simon
 *
 */
public class MyWorldObjectList {
	
	/**
	 * List containing all worldobjects (IWorldObject) and triangle-references (Integer)
	 */
	private ArrayList<Object> list = new ArrayList<Object>();
	
	/**
	 * List containing all added worldObjects (for copying purposes in CompactGrid)
	 */
	private ArrayList<IWorldObject> worldObjects = new ArrayList<IWorldObject>();
	
	/**
	 * Map of triangle-reference-indices to TriangleMeshes
	 */
	private HashMap<Integer, TriangleMesh> triangleToMeshMap = new HashMap<Integer, TriangleMesh>();
	
	public void addWorldObject(IWorldObject worldObject)
	{
		list.add(worldObject);
		worldObjects.add(worldObject);
	}
	
	/**
	 * Puts a trianglemesh in the list by storing it's triangle by reference
	 * @param mesh
	 * @return the number of triangles stored as reference
	 */
	public int addTrianglesAsReference(TriangleMesh mesh)
	{
		worldObjects.add(mesh);
		
		List<Integer> triangles = mesh.getTriangleIndexList();
		for(Integer t : triangles)
		{
			list.add(t);
			int listIndex = list.size()-1; //index of added triangle reference in the list
			triangleToMeshMap.put(listIndex, mesh);
		}
		return triangles.size();
	}
	
	public IWorldObject getObject(int index)
	{
		Object obj = list.get(index);
		if(obj instanceof IWorldObject)
		{
			return (IWorldObject) obj;
		}
		
		//else it is a referenced triangle (thus an integer)
		TriangleMesh mesh = triangleToMeshMap.get(index);
				
		return mesh.getTriangleByIndex((Integer)obj);
	}
	
	public int length()
	{
		return list.size();
	}
	
	public ArrayList<Integer> getIndexList()
	{
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for(int i=0; i<length(); i++)
		{
			ret.add(i);
		}
		return ret;
	}
	
	public ArrayList<IWorldObject> getWorldObjectList()
	{
		return worldObjects;
	}

}
