package acceleration;

import java.util.ArrayList;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.Vector3;
import worldobjects.BoundingBox;
import worldobjects.IWorldObject;

@Deprecated
public class BSPLeaf implements IBSPElement {

	private ArrayList<Integer> objectsInLeaf = new ArrayList<Integer>();
	private MyWorldObjectList objects;
	private BoundingBox box;

	public BSPLeaf(BoundingBox bb, MyWorldObjectList objs) {
		
		box = bb;
		objects = objs;
		for (int i=0; i<objs.length(); i++) {
			
			if (overlaps(objs.getObject(i).getBoundingBox(), bb))
				objectsInLeaf.add(i);
		}
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		
		result.distance = Float.POSITIVE_INFINITY;
		for (Integer index : objectsInLeaf) {

			IWorldObject cObject = objects.getObject(index);
			RaycastResult tempRes = new RaycastResult();

			if (cObject.isHit(ray, minDist, maxDist, tempRes)) {
				if (tempRes.distance > 0 && tempRes.distance < result.distance && isInLeaf(ray.Origin.plus(ray.Direction.multiply(tempRes.distance)))) {
					tempRes.copyResultTo(result);
				}
			}
		}

		return result.distance != Float.POSITIVE_INFINITY;
	}

	private boolean isInLeaf(Vector3 v) {
		Vector3 min = box.minimum;
		Vector3 max = box.maximum;
		return v.X > min.X && v.X < max.X && v.Y > min.Y && v.Y < max.Y && v.Z > min.Z && v.Z < max.Z;
	}

	private boolean overlaps(BoundingBox bb01, BoundingBox bb02) {
		Vector3 min01 = bb01.minimum;
		Vector3 min02 = bb02.minimum;
		Vector3 max01 = bb01.maximum;
		Vector3 max02 = bb02.maximum;

		return (min01.X < max02.X) && (max01.X > min02.X) && (min01.Y < max02.Y) && (max01.Y > min02.Y) && (min01.Z < max02.Z) && (max01.Z > min02.Z);
	}

	@Override
	public int nbObjectReferences() {
		return objectsInLeaf.size();
	}

	@Override
	public int nbNodes() {
		return 1;
	}

}
