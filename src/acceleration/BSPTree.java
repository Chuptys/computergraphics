package acceleration;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.Vector3;
import worldobjects.BoundingBox;
import worldobjects.IWorldObject;
import worldobjects.TriangleMesh;

@Deprecated
public class BSPTree implements IWorldObject {

	private BoundingBox totalBox = new BoundingBox(new Vector3(0, 0, 0), new Vector3(0, 0, 0));
	private MyWorldObjectList objects = new MyWorldObjectList();
	private long time;

	private final float NODESIZE = 1f;

	private BSPNode rootNode;

	private boolean initialized;

	public void addObject(IWorldObject obj) {
		// maybe check if object already added?
		Vector3 min = obj.getBoundingBox().minimum;
		Vector3 max = obj.getBoundingBox().maximum;

		totalBox.minimum.X = Math.min(totalBox.minimum.X, min.X);
		totalBox.minimum.Y = Math.min(totalBox.minimum.Y, min.Y);
		totalBox.minimum.Z = Math.min(totalBox.minimum.Z, min.Z);

		totalBox.maximum.X = Math.max(totalBox.maximum.X, max.X);
		totalBox.maximum.Y = Math.max(totalBox.maximum.Y, max.Y);
		totalBox.maximum.Z = Math.max(totalBox.maximum.Z, max.Z);

		if (obj instanceof TriangleMesh)
			objects.addTrianglesAsReference((TriangleMesh) obj);
		else
			objects.addWorldObject(obj);
	}

	public void initialize() throws Exception {
		if (objects.length() == 0)
			throw new Exception("Initialization error: No objects added to compactgrid!");

		time = System.nanoTime();

		int treeDepth = (int) ((float) (totalBox.maximum.X - totalBox.minimum.X) / NODESIZE);
		rootNode = new BSPNode(totalBox, treeDepth, objects);

		time = System.nanoTime() - time;
		System.out.println("------------------------------");
		System.out.println("BSPTree building completed!");
		System.out.println("Build Duration: " + time * 0.000001 + " ms");
		System.out.println("Tree 3D Proportions: min(" + totalBox.minimum.X + ", " + totalBox.minimum.Y + ", " + totalBox.minimum.Z + ") max(" + totalBox.maximum.X + ", " + totalBox.maximum.Y + ", " + totalBox.maximum.Z + ")");
		System.out.println("Tree Depth: " + (treeDepth + 2));
		System.out.println("nb Tree Nodes: " + rootNode.nbNodes());
		System.out.println("nb Objects: " + objects.length());
		System.out.println("nb Object-References: " + rootNode.nbObjectReferences());
		System.out.println("------------------------------");

		initialized = true;
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if(!initialized)
		{
			try {
				throw new Exception("CompactGrid error: Not initialized!");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		
		if(!totalBox.isHit(ray, minDist, maxDist, result))
			return false;
		
		return rootNode.isHit(ray, minDist, maxDist, result);
	}

	@Override
	public BoundingBox getBoundingBox() {
		return totalBox;
	}

	@Override
	public boolean castsShadow() {
		return true;
	}
}
