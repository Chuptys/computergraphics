package acceleration;

import java.util.ArrayList;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.Vector3;
import worldobjects.BoundingBox;
import worldobjects.IWorldObject;

public class BVHNode implements IWorldObject {

	private BVHNode left;
	private BVHNode right;
	private BoundingBox bb;

	private Integer leftObjRef = -1;
	private Integer rightObjRef = -1;
	private MyWorldObjectList objectList;

	public BVHNode(MyWorldObjectList objects, ArrayList<Integer> indices, int axis) {
		int N = indices.size();
		objectList = objects;

		if (N == 1) {
			leftObjRef = indices.get(0);
			bb = getLeft().getBoundingBox();
		} else if (N == 2) {
			leftObjRef = indices.get(0);
			rightObjRef = indices.get(1);
			bb = combine(getLeft().getBoundingBox(), getRight().getBoundingBox());
		} else {
			BoundingBox totalBox = getBoundingBoxFromList(objects, indices);
			float m = (getVectorPart(axis, totalBox.minimum) + getVectorPart(axis, totalBox.maximum)) * 0.5f;
			ArrayList<Integer> leftIndices = new ArrayList<Integer>();
			ArrayList<Integer> rightIndices = new ArrayList<Integer>();
			partitionList(objects, indices, leftIndices, rightIndices, m, axis);

			if (leftIndices.size() == 0 || rightIndices.size() == 0) {
				leftIndices.clear();
				rightIndices.clear();
				
				for(int i =0; i<indices.size();i++)
				{
					if(i < (int) (N * 0.5f))
						leftIndices.add(indices.get(i));
					else
						rightIndices.add(indices.get(i));
				}
			}
			left = new BVHNode(objects, leftIndices, (axis + 1) % 3);
			right = new BVHNode(objects, rightIndices, (axis + 1) % 3);
			bb = combine(getLeft().getBoundingBox(), getRight().getBoundingBox());

		}
	}

	private IWorldObject getLeft() {
		if (left != null)
			return left;

		if (leftObjRef != -1)
			return objectList.getObject(leftObjRef);

		return null;
	}

	private IWorldObject getRight() {
		if (right != null)
			return right;

		if (rightObjRef != -1)
			return objectList.getObject(rightObjRef);

		return null;
	}

	private void partitionList(MyWorldObjectList objects, ArrayList<Integer> indices, ArrayList<Integer> leftIndices, ArrayList<Integer> rightIndices, float m, int axis) {

		for (int i = 0; i < indices.size(); i++) {
			int cIndex = indices.get(i);
			BoundingBox cBox = objects.getObject(cIndex).getBoundingBox();
			float toCompare = (getVectorPart(axis, cBox.minimum) + getVectorPart(axis, cBox.maximum)) * 0.5f;
			if (toCompare < m)
				leftIndices.add(cIndex);
			else
				rightIndices.add(cIndex);
		}
	}

	private BoundingBox combine(BoundingBox b1, BoundingBox b2) {
		Vector3 min1 = b1.minimum;
		Vector3 max1 = b1.maximum;
		Vector3 min2 = b2.minimum;
		Vector3 max2 = b2.maximum;

		float minX = Math.min(min1.X, min2.X);
		float minY = Math.min(min1.Y, min2.Y);
		float minZ = Math.min(min1.Z, min2.Z);
		float maxX = Math.max(max1.X, max2.X);
		float maxY = Math.max(max1.Y, max2.Y);
		float maxZ = Math.max(max1.Z, max2.Z);

		return new BoundingBox(new Vector3(minX, minY, minZ), new Vector3(maxX, maxY, maxZ));
	}

	private BoundingBox getBoundingBoxFromList(MyWorldObjectList objects, ArrayList<Integer> indices) {
		BoundingBox ret = objects.getObject(indices.get(0)).getBoundingBox();

		for (int i = 1; i < indices.size(); i++) {
			ret = combine(ret, objects.getObject(indices.get(i)).getBoundingBox());
		}

		return ret;
	}

	private float getVectorPart(int nb, Vector3 v) {
		if (nb == 0)
			return v.X;
		if (nb == 1)
			return v.Y;
		return v.Z;
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if (!bb.isHit(ray, minDist, maxDist, result))
			return false;

		RaycastResult lRes = new RaycastResult();
		RaycastResult rRes = new RaycastResult();
		boolean leftHit = getLeft() != null && getLeft().isHit(ray, minDist, maxDist, lRes);
		boolean rightHit = getRight() != null && getRight().isHit(ray, minDist, maxDist, rRes);

		if (leftHit && rightHit) {
			if (lRes.distance < rRes.distance)
				lRes.copyResultTo(result);
			else
				rRes.copyResultTo(result);
			return true;
		} else if (leftHit) {
			lRes.copyResultTo(result);
			return true;
		} else if (rightHit) {
			rRes.copyResultTo(result);
			return true;
		}

		return false;
	}

	@Override
	public BoundingBox getBoundingBox() {
		return bb;
	}

	@Override
	public boolean castsShadow() {
		return true;
	}

	public int nbNodes() {
		int ret = 0;

		if (left != null)
			ret += left.nbNodes();
		if (right != null)
			ret += right.nbNodes();

		return ret + 1;
	}

}
