package acceleration;

import java.awt.Color;
import java.util.ArrayList;
import raytracing.Ray;
import raytracing.RaycastResult;
import structs.Point3;
import structs.Vector3;
import worldobjects.BoundingBox;
import worldobjects.IWorldObject;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;

public class CompactGrid implements IWorldObject {

	public int density = 4;

	private BoundingBox totalBox = new BoundingBox(new Vector3(0, 0, 0), new Vector3(0, 0, 0));

	/**
	 * All objects in the grid
	 */
	private MyWorldObjectList objects = new MyWorldObjectList();

	/**
	 * Concatenation of all object-lists (one object-list per cell in C)
	 */
	private int[] L;

	/**
	 * List containing all cells
	 */
	private int[] C;

	private int N; // # objects
	private int Mx; // # cells in x-direction
	private int My; // # cells in y-direction
	private int Mz; // # cells in z-direction

	private Vector3 cellSize;
	private long time;

	private boolean initialized;

	public void addObject(IWorldObject obj) {
		// maybe check if object already added?
		Vector3 min = obj.getBoundingBox().minimum;
		Vector3 max = obj.getBoundingBox().maximum;

		totalBox.minimum.X = Math.min(totalBox.minimum.X, min.X);
		totalBox.minimum.Y = Math.min(totalBox.minimum.Y, min.Y);
		totalBox.minimum.Z = Math.min(totalBox.minimum.Z, min.Z);

		totalBox.maximum.X = Math.max(totalBox.maximum.X, max.X);
		totalBox.maximum.Y = Math.max(totalBox.maximum.Y, max.Y);
		totalBox.maximum.Z = Math.max(totalBox.maximum.Z, max.Z);

		if (obj instanceof TriangleMesh) {
			N += objects.addTrianglesAsReference((TriangleMesh) obj);
		} else {
			objects.addWorldObject(obj);
			N++;
		}
	}

	public void rebuild() throws Exception {
		ArrayList<IWorldObject> oriObjects = objects.getWorldObjectList();
		objects = new MyWorldObjectList();
		N = 0;

		for (IWorldObject o : oriObjects)
			addObject(o);

		initialize();
	}

	public void initialize() throws Exception {
		if (N == 0)
			throw new Exception("Initialization error: No objects added to compactgrid!");

		time = System.nanoTime();

		totalBox.maximum = totalBox.maximum.multiply(1.01f);

		float sizeX = totalBox.maximum.X - totalBox.minimum.X;
		float sizeY = totalBox.maximum.Y - totalBox.minimum.Y;
		float sizeZ = totalBox.maximum.Z - totalBox.minimum.Z;
		float coeff = (float) Math.cbrt(density * N / (sizeX * sizeY * sizeZ));

		Mx = (int) Math.ceil(sizeX * coeff);
		My = (int) Math.ceil(sizeY * coeff);
		Mz = (int) Math.ceil(sizeZ * coeff);

		cellSize = new Vector3(sizeX / Mx, sizeY / My, sizeZ / Mz);

		C = new int[Mx * My * Mz + 1];

		// Store for every cell i the size of its object list in C[i]
		for (int i = 0; i < objects.length(); i++) {
			IWorldObject obj = objects.getObject(i);
			Point3 min = gridCoordinate(obj.getBoundingBox().minimum);
			Point3 max = gridCoordinate(obj.getBoundingBox().maximum);

			for (int x = min.X; x <= max.X; x++) {
				for (int y = min.Y; y <= max.Y; y++) {
					for (int z = min.Z; z <= max.Z; z++) {
						C[toCellIndex(x, y, z)]++;
					}
				}
			}
		}

		for (int i = 1; i < C.length; i++) {
			C[i] += C[i - 1];
		}

		L = new int[C[C.length - 1]];
		for (int m = 0; m < L.length; m++)
			L[m] = -1;

		for (int j = N - 1; j >= 0; j--) {
			IWorldObject obj = objects.getObject(j);
			Point3 min = gridCoordinate(obj.getBoundingBox().minimum);
			Point3 max = gridCoordinate(obj.getBoundingBox().maximum);

			for (int x = min.X; x <= max.X; x++) {
				for (int y = min.Y; y <= max.Y; y++) {
					for (int z = min.Z; z <= max.Z; z++) {
						L[--C[toCellIndex(x, y, z)]] = j;
					}
				}
			}
		}

		time = System.nanoTime() - time;
		System.out.println("------------------------------");
		System.out.println("COMPACTGRID building completed!");
		System.out.println("Grid density: " + density);
		System.out.println("Build Duration: " + time * 0.000001 + " ms");
		System.out.println("GridProportions: min(" + totalBox.minimum.X + ", " + totalBox.minimum.Y + ", " + totalBox.minimum.Z + ") max(" + totalBox.maximum.X + ", " + totalBox.maximum.Y + ", "
				+ totalBox.maximum.Z + ")");
		System.out.println("CellProportions: (" + cellSize.X + ", " + cellSize.Y + ", " + cellSize.Z + ")");
		System.out.println("nbCells: (" + Mx + ", " + My + ", " + Mz + ")");
		System.out.println("nbObjects: " + N);
		System.out.println("nbObjects references: " + L.length);
		System.out.println("------------------------------");

		initialized = true;
	}

	private int toCellIndex(int x, int y, int z) {
		return (((My * z) + y) * Mx) + x;
	}

	/**
	 * Returns a point3 containing the coordinates in grid with axes on gridMin
	 */
	private Point3 gridCoordinate(Vector3 v) {
		float offset = 0.0001f; // offset so rounded cellcoords on the totalbox
								// are in the grid
		float x = (v.X - totalBox.minimum.X) / cellSize.X - offset;
		float y = (v.Y - totalBox.minimum.Y) / cellSize.Y - offset;
		float z = (v.Z - totalBox.minimum.Z) / cellSize.Z - offset;

		int xCell = (int) x;
		int yCell = (int) y;
		int zCell = (int) z;

		if (xCell < 0)
			xCell = 0;
		if (yCell < 0)
			yCell = 0;
		if (zCell < 0)
			zCell = 0;
		if (xCell >= Mx)
			xCell = Mx - 1;
		if (yCell >= My)
			yCell = My - 1;
		if (zCell >= Mz)
			zCell = Mz - 1;

		return new Point3(xCell, yCell, zCell);
	}

	/**
	 * Method split in x, y & z for performance reasons (xyz together not
	 * needed)
	 */
	private float worldCoordinateX(int x) {
		return x * cellSize.X + totalBox.minimum.X;
	}

	private float worldCoordinateY(int y) {
		return y * cellSize.Y + totalBox.minimum.Y;
	}

	private float worldCoordinateZ(int z) {
		return z * cellSize.Z + totalBox.minimum.Z;
	}

	/**
	 * Grid-traversal algorithm from:
	 * http://www.cse.yorku.ca/~amana/research/grid.pdf
	 */
	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if (!initialized) {
			try {
				throw new Exception("CompactGrid error: Not initialized!");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!totalBox.isHit(ray, minDist, maxDist, null))
			return false;

		Vector3 startLocation = ray.Origin;
		if (!insideGrid(startLocation)) {
			startLocation = totalBox.getClosestIntersectionPoint(ray, 0, maxDist);
		}

		Vector3 raydir = ray.Direction;

		int stepX = 1;
		int stepY = 1;
		int stepZ = 1;

		if (raydir.X < 0)
			stepX = -1;
		if (raydir.Y < 0)
			stepY = -1;
		if (raydir.Z < 0)
			stepZ = -1;

		Vector3 tMax = new Vector3(0, 0, 0);
		Vector3 cPos = startLocation;

		Point3 cCell = gridCoordinate(cPos);

		if (raydir.X < 0.0)
			tMax.X = (worldCoordinateX(cCell.X) - cPos.X) / raydir.X;
		if (raydir.X > 0.0)
			tMax.X = (worldCoordinateX((cCell.X + 1)) - cPos.X) / raydir.X;
		if (raydir.Y < 0.0)
			tMax.Y = (worldCoordinateY(cCell.Y) - cPos.Y) / raydir.Y;
		if (raydir.Y > 0.0)
			tMax.Y = (worldCoordinateY((cCell.Y + 1)) - cPos.Y) / raydir.Y;
		if (raydir.Z < 0.0)
			tMax.Z = (worldCoordinateZ(cCell.Z) - cPos.Z) / raydir.Z;
		if (raydir.Z > 0.0)
			tMax.Z = (worldCoordinateZ((cCell.Z + 1)) - cPos.Z) / raydir.Z;

		Vector3 tDelta = new Vector3(cellSize.X / Math.abs(raydir.X), cellSize.Y / Math.abs(raydir.Y), cellSize.Z / Math.abs(raydir.Z));

		while (insideGrid(cCell)) {

			RaycastResult cCellResult = new RaycastResult();
			if (isCellHit(ray, minDist, maxDist, cCellResult, cCell)) {
				cCellResult.copyResultTo(result);
				return true; // automatically is closest hit
			}

			if (tMax.X < tMax.Y) {
				if (tMax.X < tMax.Z) {
					cCell.X += stepX;
					tMax.X += tDelta.X;
				} else {
					cCell.Z += stepZ;
					tMax.Z += tDelta.Z;
				}
			} else {
				if (tMax.Y < tMax.Z) {
					cCell.Y += stepY;
					tMax.Y += tDelta.Y;
				} else {
					cCell.Z += stepZ;
					tMax.Z += tDelta.Z;
				}
			}
		}

		return false;
	}

	/**
	 * Returns whether an object in the cell at given location is hit and if so,
	 * stores the closest RaycastResult in given result.
	 */
	private boolean isCellHit(Ray ray, float minDist, float maxDist, RaycastResult result, Point3 cCell) {

		result.distance = Float.POSITIVE_INFINITY;
		int cellIndex = toCellIndex(cCell.X, cCell.Y, cCell.Z);

		for (int i = C[cellIndex]; i < C[cellIndex + 1]; i++) {

			IWorldObject cObject = objects.getObject(L[i]);
			RaycastResult tempRes = new RaycastResult();

			if (cObject.isHit(ray, minDist, maxDist, tempRes)) {
				if (tempRes.distance > 0 && tempRes.distance < result.distance) {
					tempRes.copyResultTo(result);
				}
			}
		}

		Point3 hitPos = gridCoordinate(ray.Origin.plus(ray.Direction.multiply(result.distance)));
		if (!(hitPos.X == cCell.X && hitPos.Y == cCell.Y && hitPos.Z == cCell.Z)) // Check
																					// closest
																					// intersection
																					// occurs
																					// in
																					// cCell
			return false;

		return result.distance != Float.POSITIVE_INFINITY;
	}

	private boolean insideGrid(Vector3 pos) {
		return totalBox.contains(pos);
	}

	private boolean insideGrid(Point3 cCell) {
		return 0 <= cCell.X && cCell.X < Mx && 0 <= cCell.Y && cCell.Y < My && 0 <= cCell.Z && cCell.Z < Mz;
	}

	@Override
	public BoundingBox getBoundingBox() {
		return totalBox;
	}

	@Override
	public boolean castsShadow() {
		return true; // Let the objects in the grid decide whether to cast
						// shadows
	}

	/**
	 * FOR TESTING PURPOSES ONLY!!
	 */
	public ArrayList<Sphere> getTestSpheres() {

		ArrayList<Sphere> ret = new ArrayList<Sphere>();

		for (int x = 0; x < Mx; x++) {
			for (int y = 0; y < My; y++) {
				for (int z = 0; z < Mz; z++) {

					Sphere sDummy = new Sphere(new Vector3(worldCoordinateX(x), worldCoordinateY(y), worldCoordinateZ(z)), 0.05f);
					sDummy.diffuseColor = new Color(100, 100, 100);
					sDummy.castsShadow = false;
					ret.add(sDummy);

					int cellIndex = toCellIndex(x, y, z);
					for (int i = C[cellIndex]; i < C[cellIndex + 1]; i++) {

						Sphere cObject = (Sphere) objects.getObject(L[i]);
						Sphere s = new Sphere(new Vector3(worldCoordinateX(x), worldCoordinateY(y), worldCoordinateZ(z)), 0.1f);
						s.diffuseColor = cObject.diffuseColor;
						s.castsShadow = false;
						ret.add(s);
					}
				}
			}
		}

		return ret;
	}

}
