package acceleration;

import raytracing.Ray;
import raytracing.RaycastResult;
import worldobjects.BoundingBox;
import worldobjects.IWorldObject;
import worldobjects.TriangleMesh;

public class BVHNodeTree implements IWorldObject{
	
	private BVHNode topNode;
	private MyWorldObjectList objects = new MyWorldObjectList();
	private long time;
	private boolean initialized;
	
	public void addObject(IWorldObject obj) {				
		if(obj instanceof TriangleMesh)
		{
			objects.addTrianglesAsReference((TriangleMesh)obj);			 
		}
		else
		{
			objects.addWorldObject(obj);
		}		
	}
	
	public void initialize()
	{
		time = System.nanoTime();
		
		topNode = new BVHNode(objects, objects.getIndexList(), 0);
		
		time = System.nanoTime() - time;
		System.out.println("------------------------------");
		System.out.println("BVHNodeTree building completed!");
		System.out.println("Build Duration: " + time * 0.000001 + " ms");
		System.out.println("Tree Proportions: min(" + topNode.getBoundingBox().minimum.X + ", " + topNode.getBoundingBox().minimum.Y + ", " + topNode.getBoundingBox().minimum.Z + ") max(" + topNode.getBoundingBox().maximum.X + ", " + topNode.getBoundingBox().maximum.Y + ", " + topNode.getBoundingBox().maximum.Z + ")");
		
		System.out.println("nbNodes: " + topNode.nbNodes());
		System.out.println("nbObjects: " + objects.length());
		System.out.println("------------------------------");
		
		initialized = true;
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if(!initialized)
		{
			try {
				throw new Exception("BVHNodeTree error: Not initialized!");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return topNode.isHit(ray, minDist, maxDist, result);
	}

	@Override
	public BoundingBox getBoundingBox() {
		return topNode.getBoundingBox();
	}

	@Override
	public boolean castsShadow() {
		return true;
	}

}
