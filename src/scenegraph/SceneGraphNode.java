package scenegraph;

import java.util.ArrayList;

import structs.Matrix;
import worldobjects.TriangleMesh;

public class SceneGraphNode {
	
	private Matrix local;
	
	private SceneGraphNode parent;
	private ArrayList<SceneGraphNode> childs = new ArrayList<SceneGraphNode>();
	
	private ArrayList<TriangleMesh> objects = new ArrayList<TriangleMesh>();;
	
	public SceneGraphNode(Matrix localMat)
	{
		local = localMat;
	}
	
	public void addChild(SceneGraphNode child)
	{
		childs.add(child);
		child.parent = this;
		child.update();
	}
	
	public void addWorldObject(TriangleMesh o)
	{
		objects.add(o);
		o.setWorldMatrix(getWorldMatrix());
	}
	
	private Matrix getWorldMatrix()
	{
		if(parent == null)
			return local;
		
		return parent.getWorldMatrix().multiply(local);
	}
	
	public void update()
	{
		Matrix world = getWorldMatrix();
		for(TriangleMesh mesh : objects)
		{
			mesh.setWorldMatrix(world);
		}
	}
	
	public void setLocalMatrix(Matrix localMat)
	{
		local = localMat;
		update();
	}
}
