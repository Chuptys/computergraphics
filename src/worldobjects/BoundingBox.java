package worldobjects;

import java.util.ArrayList;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.NbIntersectionsManager;
import structs.Vector3;

/**
 * Not intended for rendering, just for intersection-testing
 * 
 * @author Simon
 * 
 */
public class BoundingBox implements IWorldObject {

	public Vector3 minimum = new Vector3(0, 0, 0);
	public Vector3 maximum = new Vector3(0, 0, 0);
	
	public BoundingBox(Vector3 min, Vector3 max) {
		minimum = min;
		maximum = max;
	}

	/**
	 * Does not calculate hitLocation, ignore raycastResult.
	 */
	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		NbIntersectionsManager.getInstance().increaseNbIntersections();
		
		float InvDirX = 1 / ray.Direction.X;
		float InvDirY = 1 / ray.Direction.Y;
		float InvDirZ = 1 / ray.Direction.Z;

		float tXMin;
		float tXMax;
		float tYMin;
		float tYMax;
		float tZMin;
		float tZMax;

		if (InvDirX > 0) {
			tXMin = (minimum.X - ray.Origin.X) * InvDirX;
			tXMax = (maximum.X - ray.Origin.X) * InvDirX;
		} else {
			tXMin = (maximum.X - ray.Origin.X) * InvDirX;
			tXMax = (minimum.X - ray.Origin.X) * InvDirX;
		}
		if (InvDirY > 0) {
			tYMin = (minimum.Y - ray.Origin.Y) * InvDirY;
			tYMax = (maximum.Y - ray.Origin.Y) * InvDirY;
		} else {
			tYMin = (maximum.Y - ray.Origin.Y) * InvDirY;
			tYMax = (minimum.Y - ray.Origin.Y) * InvDirY;
		}
		if (InvDirZ > 0) {
			tZMin = (minimum.Z - ray.Origin.Z) * InvDirZ;
			tZMax = (maximum.Z - ray.Origin.Z) * InvDirZ;
		} else {
			tZMin = (maximum.Z - ray.Origin.Z) * InvDirZ;
			tZMax = (minimum.Z - ray.Origin.Z) * InvDirZ;
		}

		float tMin;
		float tMax;

		if (tXMin > tYMax || tYMin > tXMax)
			return false;

		tMin = Math.max(tXMin, tYMin);
		tMax = Math.min(tXMax, tYMax);

		if (tMin > tZMax || tZMin > tMax)
			return false;

		tMin = Math.max(tMin, tZMin);
		tMax = Math.min(tMax, tZMax);

		return tMin < maxDist && tMax > minDist;
	}

	@Override
	public BoundingBox getBoundingBox() {
		return this;
	}

	@Override
	public boolean castsShadow() {
		return false;
	}

	/**
	 * If only checking for a hit, use isHit. Use this method when the exact
	 * hitLocation is wanted.
	 */
	public Vector3 getClosestIntersectionPoint(Ray ray, float minDist, float maxDist) {
		Plane top = new Plane(maximum, new Vector3(0, 1, 0));
		Plane bottom = new Plane(minimum, new Vector3(0, -1, 0));
		Plane left = new Plane(minimum, new Vector3(0, 0, -1));
		Plane right = new Plane(maximum, new Vector3(0, 0, 1));
		Plane front = new Plane(minimum, new Vector3(-1, 0, 0));
		Plane back = new Plane(maximum, new Vector3(1, 0, 0));

		Plane[] planes = { top, bottom, left, right, front, back };

		float closest = Float.MAX_VALUE;

		for (Plane p : planes) {
			RaycastResult tempRes = new RaycastResult();
			if (p.isHit(ray, minDist, maxDist, tempRes)) {
				if (tempRes.distance > 0 && tempRes.distance < closest && isOnBox(ray, tempRes.distance)) {
					closest = tempRes.distance;
				}
			}
		}

		if (closest == Float.MAX_VALUE)
			return null;

		return ray.Origin.plus(ray.Direction.multiply(closest));
	}
	
	public ArrayList<RaycastResult> getAllIntersections(Ray ray, float minDist, float maxDist)
	{
		ArrayList<RaycastResult> ret = new ArrayList<RaycastResult>();
		
		Plane top = new Plane(maximum, new Vector3(0, 1, 0));
		Plane bottom = new Plane(minimum, new Vector3(0, -1, 0));
		Plane left = new Plane(minimum, new Vector3(0, 0, -1));
		Plane right = new Plane(maximum, new Vector3(0, 0, 1));
		Plane front = new Plane(minimum, new Vector3(-1, 0, 0));
		Plane back = new Plane(maximum, new Vector3(1, 0, 0));

		Plane[] planes = { top, bottom, left, right, front, back };

		for (Plane p : planes) {
			RaycastResult tempRes = new RaycastResult();
			if (p.isHit(ray, minDist, maxDist, tempRes) && isOnBox(ray, tempRes.distance)) {
				RaycastResult res = new RaycastResult();
				tempRes.copyResultTo(res);
				ret.add(res);
			}
		}		
		
		return ret;
	}

	private boolean isOnBox(Ray ray, float distance) {
		Vector3 p = ray.Origin.plus(ray.Direction.multiply(distance));
		float eps = 0.001f;
		if (p.X < minimum.X - eps || p.X > maximum.X + eps || p.Y < minimum.Y - eps || p.Y > maximum.Y + eps || p.Z < minimum.Z - eps || p.Z > maximum.Z + eps)
			return false;

		return true;
	}

	public boolean contains(Vector3 pos) {
		return minimum.X <= pos.X && minimum.Y <= pos.Y && minimum.Z <= pos.Z && maximum.X >= pos.X && maximum.Y >= pos.Y && maximum.Z >= pos.Z;
	}
}
