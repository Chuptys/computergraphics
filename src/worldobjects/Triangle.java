package worldobjects;

import java.awt.Color;
import java.awt.image.BufferedImage;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.NbIntersectionsManager;
import structs.StandardValues;
import structs.Vector3;

public class Triangle implements IWorldObject {

	public Vector3 v1;
	public Vector3 v2;
	public Vector3 v3;
	public Vector3 n1;
	public Vector3 n2;
	public Vector3 n3;
	public Vector3 tex1;
	public Vector3 tex2;
	public Vector3 tex3;

	public Color diffuseColor;
	public Color specularColor;
	public float specularIntensity;
	public boolean reflective;
	public boolean castsShadow = true;
	
	public boolean refractive;
	public Vector3 refractiveAttenuation = StandardValues.REFRACTIVE_ATTENUATION;
	public float refractionIndex = StandardValues.REFRACTIVE_INDEX;
	
	public float selfIllumination = 1;
	/*
	 * SELF ILLUMINATION NOTE: range [0..infinity], will be multiplied with the
	 * final calculated color 0 means this object will appear black. 1 means
	 * this objects has no 'self illumination'
	 */

	private BoundingBox bb;

	private BufferedImage diffuseMap;
	private boolean hasDiffuseMap;

	private BufferedImage bumpMap;
	private boolean hasBumpMap;
	
	public boolean glossy;
	public float glossAmount = StandardValues.GLOSS_AMOUNT;

	public Triangle(Vector3 a, Vector3 b, Vector3 c) {
		v1 = a;
		v2 = b;
		v3 = c;
		diffuseColor = StandardValues.DIFFUSE_COLOR;
		specularColor = StandardValues.SPECULAR_COLOR;
		specularIntensity = StandardValues.SPECULAR_INTENSITY;

		Vector3 min = new Vector3(Math.min(v1.X, Math.min(v2.X, v3.X)), Math.min(v1.Y, Math.min(v2.Y, v3.Y)), Math.min(v1.Z, Math.min(v2.Z, v3.Z)));
		Vector3 max = new Vector3(Math.max(v1.X, Math.max(v2.X, v3.X)), Math.max(v1.Y, Math.max(v2.Y, v3.Y)), Math.max(v1.Z, Math.max(v2.Z, v3.Z)));
		bb = new BoundingBox(min, max);
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if (!castsShadow() && ray.isShadowRay)
			return false;

		if (getBoundingBox().isHit(ray, minDist, maxDist, new RaycastResult()) == false)
			return false; // If bb misses, skip this object.

		NbIntersectionsManager.getInstance().increaseNbIntersections();

		result.distance = Integer.MAX_VALUE;

		float a = v1.X - v2.X;
		float b = v1.Y - v2.Y;
		float c = v1.Z - v2.Z;
		float d = v1.X - v3.X;
		float e = v1.Y - v3.Y;
		float f = v1.Z - v3.Z;
		float g = ray.Direction.X;
		float h = ray.Direction.Y;
		float i = ray.Direction.Z;
		float j = v1.X - ray.Origin.X;
		float k = v1.Y - ray.Origin.Y;
		float l = v1.Z - ray.Origin.Z;

		float beta = Integer.MAX_VALUE;
		float gamma = Integer.MAX_VALUE;
		float t = Integer.MAX_VALUE;

		float eiMinushf = e * i - h * f;
		float gfMinusdi = g * f - d * i;
		float dhMinuseg = d * h - e * g;
		float akMinusjb = a * k - j * b;
		float jcMinusal = j * c - a * l;
		float blMinuskc = b * l - k * c;
		float M = a * eiMinushf + b * gfMinusdi + c * dhMinuseg;

		t = -(f * akMinusjb + e * jcMinusal + d * blMinuskc) / M;
		if (t < minDist || t > maxDist)
			return false;

		gamma = (i * akMinusjb + h * jcMinusal + g * blMinuskc) / M;
		if (gamma < 0 || gamma > 1)
			return false;

		beta = (j * eiMinushf + k * gfMinusdi + l * dhMinuseg) / M;
		if (beta < 0 || beta > 1 - gamma)
			return false;

		result.distance = t;
		result.ray = ray;
		float alpha = 1 - beta - gamma;
		result.normal = interpolateNormal(alpha, beta, gamma);

		result.diffuseColor = getColor(beta, gamma);
		result.specularColor = specularColor;
		result.specularIntensity = specularIntensity;
		result.selfllumination = selfIllumination;
		result.reflective = reflective;
		result.refractive = refractive;
		result.refractionIndex = refractionIndex;
		result.refractiveAttenuation = refractiveAttenuation;
		setGlossySpecs(result);		

		return true;
	}
	
	private void setGlossySpecs(RaycastResult result) {
		if(!glossy)
			return;
		
		result.glossy = glossy;
		Vector3 tangent = new Vector3(0,0,0);
		Vector3 bitangent = new Vector3(0,0,0);
		getOrthonormalBasis(tangent, bitangent);	
		
		result.orthnormBasis[0] = tangent;
		result.orthnormBasis[1] = bitangent;
		
		result.glossAmount = glossAmount;
	}

	private void getOrthonormalBasis(Vector3 tan, Vector3 bitan) {
		Vector3 Edge1 = v2.minus(v1);
		Vector3 Edge2 = v3.minus(v1);
		Vector3 Edge2uv = tex2.minus(tex1);
		Vector3 Edge1uv = tex3.minus(tex1);
		Vector3 tangent = new Vector3(0,0,0);
		Vector3 bitangent = new Vector3(0,0,0);

		float cp = Edge1uv.Y * Edge2uv.X - Edge1uv.X * Edge2uv.Y;
		if (cp != 0.0f) {
			float mul = 1.0f / cp;
			tangent = (Edge1.multiply(-Edge2uv.Y).plus(Edge2.multiply(Edge1uv.Y))).multiply(mul);
			bitangent = (Edge1.multiply(-Edge2uv.X).plus(Edge2.multiply(Edge1uv.X))).multiply(mul);

			tangent.normalize();
			bitangent.normalize();
		}		
		
		tangent.copyTo(tan);
		bitangent.copyTo(bitan);
	}

	private Color getColorFromMap(float beta, float gamma, BufferedImage img) {
		float u = tex1.X + beta * (tex2.X - tex1.X) + gamma * (tex3.X - tex1.X);
		float v = tex1.Y + beta * (tex2.Y - tex1.Y) + gamma * (tex3.Y - tex1.Y);

		int x = (int) ((float) u * img.getWidth());
		int y = img.getHeight() + (int) ((float) v * img.getHeight()) - 1;

		if (y < 0)
			y = 0;
		if (x < 0)
			x = 0;
		if (y >= img.getHeight())
			y = img.getHeight() - 1;
		if (x >= img.getWidth())
			x = img.getWidth() - 1;

		int rgb = img.getRGB(x, y);
		return new Color(rgb);
	}

	private Color getColor(float beta, float gamma) {
		if (!hasDiffuseMap)
			return diffuseColor;

		return getColorFromMap(beta, gamma, diffuseMap);
	}

	private Vector3 interpolateNormal(float alpha, float beta, float gamma) {
		float x = n1.X * alpha + n2.X * beta + n3.X * gamma;
		float y = n1.Y * alpha + n2.Y * beta + n3.Y * gamma;
		float z = n1.Z * alpha + n2.Z * beta + n3.Z * gamma;
		Vector3 ret = new Vector3(x, y, z);

		if (hasBumpMap) {
			Vector3 tangent = new Vector3(0, 0, 0);
			Vector3 bitangent = new Vector3(0, 0, 0);
			getOrthonormalBasis(tangent, bitangent);			

			Color c = getColorFromMap(beta, gamma, bumpMap);
			Vector3 bump = new Vector3((float) c.getRed() / 255f, (float) c.getGreen() / 255f, (float) c.getBlue() / 255f);
			bump = bump.minus(new Vector3(0.5f, 0.5f, 0.5f)).multiply(2);
			bump.normalize();

			ret = tangent.multiply(bump.X).plus(bitangent.multiply(bump.Y)).plus(ret.multiply(bump.Z));
		}

		ret.normalize();
		return ret;
	}

	public void setNormalMap(BufferedImage img) {
		hasBumpMap = true;
		bumpMap = img;
	}

	@Override
	public BoundingBox getBoundingBox() {
		return bb;
	}

	@Override
	public boolean castsShadow() {
		return castsShadow;
	}

	public void setDiffuseMap(BufferedImage img) {
		hasDiffuseMap = true;
		diffuseMap = img;
	}
}
