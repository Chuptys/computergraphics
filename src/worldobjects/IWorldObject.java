package worldobjects;

import raytracing.Ray;
import raytracing.RaycastResult;

public interface IWorldObject {

	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result);
	public BoundingBox getBoundingBox();
	public boolean castsShadow();
	
	
}
