package worldobjects;

import java.awt.Color;
import java.awt.image.BufferedImage;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.NbIntersectionsManager;
import structs.StandardValues;
import structs.Vector3;

public class Sphere implements IWorldObject {
	public Vector3 center;
	public float radius;
	public Color diffuseColor;
	public Color specularColor;
	public float specularIntensity;
	public boolean reflective;
	public boolean castsShadow = true;
	public float selfIllumination = 1;

	public boolean refractive;
	public Vector3 refractiveAttenuation = StandardValues.REFRACTIVE_ATTENUATION;
	public float refractionIndex = StandardValues.REFRACTIVE_INDEX;

	private BufferedImage diffuseMap;
	private boolean hasDiffuseMap;;

	private BufferedImage bumpMap;
	private boolean hasBumpMap;
	
	public boolean glossy;
	public float glossAmount = StandardValues.GLOSS_AMOUNT;

	private BoundingBox bb;

	/*
	 * SELF ILLUMINATION NOTE: range [0..infinity], will be multiplied with the
	 * final calculated color 0 means this object will appear black. 1 means
	 * this objects has no 'self illumination'
	 */

	public Sphere(Vector3 pos, float rad) {
		center = pos;
		radius = rad;
		diffuseColor = StandardValues.DIFFUSE_COLOR;
		specularColor = StandardValues.SPECULAR_COLOR;
		specularIntensity = StandardValues.SPECULAR_INTENSITY;

		Vector3 min = new Vector3(pos.X - rad, pos.Y - rad, pos.Z - rad);
		Vector3 max = new Vector3(pos.X + rad, pos.Y + rad, pos.Z + rad);
		bb = new BoundingBox(min, max);
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if (!castsShadow() && ray.isShadowRay)
			return false;

		if (getBoundingBox().isHit(ray, minDist, maxDist, new RaycastResult()) == false)
			return false; // If bb misses, skip this object.

		NbIntersectionsManager.getInstance().increaseNbIntersections();

		result.distance = Integer.MAX_VALUE;

		Vector3 eMinusc = ray.Origin.plus(center.multiply(-1));
		float discriminantOver4 = ray.Direction.dot(eMinusc) * ray.Direction.dot(eMinusc) - ray.Direction.dot(ray.Direction) * (eMinusc.dot(eMinusc) - radius * radius);

		if (discriminantOver4 < 0)
			return false;

		float dist01 = (ray.Direction.multiply(-1).dot(eMinusc) + (float) Math.sqrt(discriminantOver4)) / ray.Direction.dot(ray.Direction);
		float dist02 = (ray.Direction.multiply(-1).dot(eMinusc) - (float) Math.sqrt(discriminantOver4)) / ray.Direction.dot(ray.Direction);

		if (dist01 < 0 && dist02 < 0)
			return false;

		if (dist01 > minDist && dist01 < dist02 && dist01 < maxDist)
			result.distance = dist01;
		else if (dist02 > minDist && dist02 < dist01 && dist02 < maxDist)
			result.distance = dist02;

		if (result.distance < minDist || result.distance >= maxDist)
			return false;

		result.ray = ray;
		result.normal = calculateNormal(ray.Origin.plus(ray.Direction.multiply(result.distance)));

		result.diffuseColor = getDiffuseColor(ray.Origin.plus(ray.Direction.multiply(result.distance)));
		result.specularColor = specularColor;
		result.specularIntensity = specularIntensity;
		result.reflective = reflective;
		result.selfllumination = selfIllumination;
		result.refractive = refractive;
		result.refractionIndex = refractionIndex;
		result.refractiveAttenuation = refractiveAttenuation;
		setGlossySpecs(result); // call after normal is set in result!!

		return true;
	}

	private void setGlossySpecs(RaycastResult result) {
		if(!glossy)
			return;
		
		result.glossy = glossy;
		Vector3 tangent = new Vector3(0,0,0);
		Vector3 bitangent = new Vector3(0,0,0);
		getOrthonormalBasis(result.normal, tangent, bitangent);	
		
		result.orthnormBasis[0] = tangent;
		result.orthnormBasis[1] = bitangent;
		
		result.glossAmount = glossAmount;
	}

	private Vector3 calculateNormal(Vector3 intersectPoint) {
		Vector3 normal = intersectPoint.plus(center.multiply(-1));

		if (hasBumpMap) {
			Color c = getColorFromMap(intersectPoint, bumpMap);
			Vector3 bump = new Vector3((float) c.getRed() / 255f, (float) c.getGreen() / 255f, (float) c.getBlue() / 255f);
			bump = bump.minus(new Vector3(0.5f, 0.5f, 0.5f)).multiply(2);
			bump.normalize();

			Vector3 tangent = new Vector3(0,0,0);
			Vector3 bitangent = new Vector3(0,0,0);
			getOrthonormalBasis(normal, tangent, bitangent);			

			normal = tangent.multiply(bump.X).plus(bitangent.multiply(bump.Y)).plus(normal.multiply(bump.Z));
		}

		normal.normalize();
		return normal;
	}
	
	private void getOrthonormalBasis(Vector3 normal, Vector3 tan, Vector3 bitan)
	{
		Vector3 tcrossw = changeSmallestComponentTo(normal, 1).cross(normal);
		Vector3 tangent = tcrossw.multiply(1 / tcrossw.length());
		tangent.normalize();
		Vector3 bitangent = normal.cross(tangent);
		bitangent.normalize();
		
		tangent.copyTo(tan);
		bitangent.copyTo(bitan);
	}

	private Vector3 changeSmallestComponentTo(Vector3 v, float f) {
		if (v.X < v.Y && v.X < v.Z)
			return new Vector3(f, v.Y, v.Z);
		if (v.Y < v.X && v.Y < v.Z)
			return new Vector3(v.X, f, v.Z);
		return new Vector3(v.X, v.Y, f);
	}

	@Override
	public BoundingBox getBoundingBox() {
		return bb;
	}

	@Override
	public boolean castsShadow() {
		return castsShadow;
	}

	private Color getColorFromMap(Vector3 pos, BufferedImage img) {
		float theta = (float) Math.acos((pos.Y - center.Y) / radius);
		float phi = -(float) Math.atan2(pos.Z - center.Z, pos.X - center.X);
		if (phi < 0)
			phi += (float) Math.PI * 2;

		float u = (float) (phi / (2 * Math.PI));
		float v = (float) ((Math.PI - theta) / Math.PI);
		int x = (int) ((float) u * img.getWidth());
		int y = img.getHeight() - (int) ((float) v * img.getHeight());

//		if (x < 0)
//			x = 0;
//		if (y < 0)
//			y = 0;
//		if (x >= img.getWidth())
//			x = img.getWidth() - 1;
//		if (y >= img.getHeight())
//			y = img.getHeight() - 1;

		int rgb = img.getRGB(x, y);
		return new Color(rgb);
	}

	private Color getDiffuseColor(Vector3 pos) {
		if (!hasDiffuseMap)
			return diffuseColor;

		return getColorFromMap(pos, diffuseMap);
	}

	public void setDiffuseMap(BufferedImage img) {
		if(img == null)
			return;
		
		diffuseMap = img;
		hasDiffuseMap = true;
	}

	public void setBumpMap(BufferedImage img) {
		if(img == null)
			return;
		
		bumpMap = img;
		hasBumpMap = true;
	}

}
