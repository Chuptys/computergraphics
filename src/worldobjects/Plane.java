package worldobjects;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.NbIntersectionsManager;
import structs.Vector3;

/**
 * Not intended for rendering, just for intersection-testing
 * 
 * @author Simon
 * 
 */
public class Plane implements IWorldObject {

	private Vector3 inPlane;
	private Vector3 normal;

	public Plane(Vector3 p, Vector3 n) {
		inPlane = p;
		normal = n;
		normal.normalize();
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		NbIntersectionsManager.getInstance().increaseNbIntersections();

		if (Math.abs(normal.dot(ray.Direction)) < 0.001f)
			return false;

		float dist = normal.dot(inPlane.minus(ray.Origin)) / normal.dot(ray.Direction);
		
		if (dist < minDist || dist > maxDist)
			return false;

		result.distance = dist;
		result.ray = ray;
		result.normal = normal;

		return true;
	}

	@Override
	public boolean castsShadow() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BoundingBox getBoundingBox() {
		// TODO Auto-generated method stub
		return null;
	}
}
