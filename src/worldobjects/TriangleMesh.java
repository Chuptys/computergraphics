package worldobjects;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import raytracing.Ray;
import raytracing.RaycastResult;
import structs.Matrix;
import structs.NbIntersectionsManager;
import structs.StandardValues;
import structs.Vector3;

public class TriangleMesh implements IWorldObject {

	private BoundingBox bb;
	public Matrix worldMatrix = Matrix.identity();

	public Vector3[] oriVerts; // positions in own coordinate system
	public Vector3[] oriNormals;
	public Vector3[] verts; // translated positions
	public Vector3[] normals;
	public Vector3[] texCoords;

	public int[][] triangleVerts; // each entry represents a triangle storing
									// three vertex-references
	public int[][] triangleNormals; // each entry represents a triangle storing
									// three normal-references
	public int[][] triangleTexCoords; // each entry represents a triangle
										// storing three texCoord-references

	private int nbVerts;
	private int nbNormals;
	private int nbTexCoords;
	private int nbTris;

	public boolean reflective;
	public boolean castsShadow = true;

	public boolean refractive;
	public Vector3 refractiveAttenuation = StandardValues.REFRACTIVE_ATTENUATION;
	public float refractionIndex = StandardValues.REFRACTIVE_INDEX;

	public Color diffuseColor = StandardValues.DIFFUSE_COLOR;
	public Color specularColor = StandardValues.SPECULAR_COLOR;
	public float specularIntensity = StandardValues.SPECULAR_INTENSITY;
	public float selfIllumination = 1;
	/*
	 * SELF ILLUMINATION NOTE: range [0..infinity], will be multiplied with the
	 * final calculated color 0 means this object will appear black. 1 means
	 * this objects has no 'self illumination'
	 */

	private BufferedImage diffuseMap;
	public boolean hasDiffuseMap;

	private BufferedImage bumpMap;
	public boolean hasBumpMap;

	public TriangleMesh(int nbVerts, int nbNormals, int nbTris, int nbTexCoords) {
		verts = new Vector3[nbVerts];
		normals = new Vector3[nbNormals];
		oriVerts = new Vector3[nbVerts];
		oriNormals = new Vector3[nbNormals];
		texCoords = new Vector3[nbTexCoords];

		triangleVerts = new int[nbTris][3];
		triangleNormals = new int[nbTris][3];
		triangleTexCoords = new int[nbTris][3];

		this.nbVerts = nbVerts;
		this.nbNormals = nbNormals;
		this.nbTexCoords = nbTexCoords;
		this.nbTris = nbTris;
	}

	public void setWorldMatrix(Matrix m) {
		worldMatrix = m;
		for (int i = 0; i < nbVerts; i++) {
			verts[i] = oriVerts[i].transform(m);
		}

		Matrix normalMatrix = m.getInverseTimesDet().getTranspose();
		for (int i = 0; i < nbNormals; i++) {
			normals[i] = oriNormals[i].transform(normalMatrix);
		}

		recalculateBoundingBox();
	}

	private void recalculateBoundingBox() {
		Vector3 min;
		Vector3 max;
		float minX = Integer.MAX_VALUE;
		float maxX = Integer.MIN_VALUE;
		float minY = Integer.MAX_VALUE;
		float maxY = Integer.MIN_VALUE;
		float minZ = Integer.MAX_VALUE;
		float maxZ = Integer.MIN_VALUE;

		for (Vector3 v : verts) {
			if (minX > v.X)
				minX = v.X;
			if (maxX < v.X)
				maxX = v.X;
			if (minY > v.Y)
				minY = v.Y;
			if (maxY < v.Y)
				maxY = v.Y;
			if (minZ > v.Z)
				minZ = v.Z;
			if (maxZ < v.Z)
				maxZ = v.Z;
		}

		// Make sure there is no 'flat' boundingbox
		if (Math.abs(minX - maxX) < 0.0001f)
			maxX += 0.01f;
		if (Math.abs(minY - maxY) < 0.0001f)
			maxY += 0.01f;
		if (Math.abs(minZ - maxZ) < 0.0001f)
			maxZ += 0.01f;

		min = new Vector3(minX, minY, minZ);
		max = new Vector3(maxX, maxY, maxZ);

		bb = new BoundingBox(min, max);
	}

	@Override
	public boolean isHit(Ray ray, float minDist, float maxDist, RaycastResult result) {
		if (!castsShadow() && ray.isShadowRay)
			return false;

		if (!getBoundingBox().isHit(ray, minDist, maxDist, new RaycastResult()))
			return false; // If bb misses, skip this object.

		NbIntersectionsManager.getInstance().increaseNbIntersections();

		result.distance = Integer.MAX_VALUE;

		for (int i = 0; i < nbTris; i++) {
			Triangle triangle = getTriangleByIndex(i);

			RaycastResult tempRes = new RaycastResult();

			if (triangle.isHit(ray, minDist, maxDist, tempRes)) {
				if (tempRes.distance > 0 && tempRes.distance < result.distance) {
					// result.distance = tempRes.distance;
					// result.ray = tempRes.ray;
					// result.diffuseColor = tempRes.diffuseColor;
					// result.normal = tempRes.normal;
					// result.specularColor = tempRes.specularColor;
					// result.specularIntensity = tempRes.specularIntensity;
					// result.reflective = tempRes.reflective;

					tempRes.copyResultTo(result);
				}
			}
		}

		result.selfllumination = selfIllumination;

		return result.distance != Integer.MAX_VALUE;
	}

	@Override
	public BoundingBox getBoundingBox() {
		return bb;
	}

	@Override
	public boolean castsShadow() {
		return castsShadow;
	}

	public Triangle getTriangleByIndex(int i) {
		Triangle triangle = new Triangle(verts[triangleVerts[i][0] - 1], verts[triangleVerts[i][1] - 1], verts[triangleVerts[i][2] - 1]);
		triangle.n1 = normals[triangleNormals[i][0] - 1];
		triangle.n2 = normals[triangleNormals[i][1] - 1];
		triangle.n3 = normals[triangleNormals[i][2] - 1];
		if (texCoords.length != 0) {
			triangle.tex1 = texCoords[triangleTexCoords[i][0] - 1];
			triangle.tex2 = texCoords[triangleTexCoords[i][1] - 1];
			triangle.tex3 = texCoords[triangleTexCoords[i][2] - 1];
		}

		setDiffuse(triangle);
		setBump(triangle);

		triangle.castsShadow = castsShadow;
		triangle.selfIllumination = selfIllumination;
		triangle.specularColor = specularColor;
		triangle.specularIntensity = specularIntensity;
		triangle.reflective = reflective;
		triangle.refractive = refractive;
		triangle.refractionIndex = refractionIndex;
		triangle.refractiveAttenuation = refractiveAttenuation;

		return triangle;
	}

	private void setBump(Triangle t) {
		if (hasBumpMap)
			t.setNormalMap(bumpMap);
	}

	private void setDiffuse(Triangle triangle) {
		triangle.diffuseColor = diffuseColor;

		if (hasDiffuseMap)
			triangle.setDiffuseMap(diffuseMap);

	}

	public ArrayList<Integer> getTriangleIndexList() {
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for (int i = 0; i < nbTris; i++)
			ret.add(i);

		return ret;
	}

	public void setDiffuseMap(BufferedImage img) {
		if (img == null)
			return;

		hasDiffuseMap = true;
		diffuseMap = img;
	}

	public void setNormalMap(BufferedImage img) {
		if (img == null)
			return;

		hasBumpMap = true;
		bumpMap = img;
	}

}
