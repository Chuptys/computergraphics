package utils;

import java.awt.Color;

import structs.Vector3;

public class SimpleBackGround implements IBackGround {

	private Color color;
	
	public SimpleBackGround(Color c)
	{
		color = c;
	}
	
	@Override
	public Color getColorFromDirection(Vector3 dir) {
		return color;
	}

}
