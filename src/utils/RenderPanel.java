package utils;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import structs.MyDimension;

public class RenderPanel implements MouseListener{

	private JFrame frame;
	private CgPanel panel;

	public RenderPanel(MyDimension dim) {
		panel = new CgPanel();
		frame = new JFrame();
		frame.setSize(dim.width, dim.height);
		frame.getContentPane().add(panel);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setTitle("New RenderPanel");
		
		panel.addMouseListener(this);

		wait(100);
	}

	/**
	 * (0,0) is left-bottom, colors are in range 0-255
	 */
	public void drawPixel(int row, int column, Color rgb) {
		float r = (float)rgb.getRed() / 255;
		float g = (float)rgb.getGreen() / 255;
		float b = (float)rgb.getBlue() / 255;
		drawPixel(row, column, r, g, b);
	}

	/*
	 * (0,0) is left-bottom
	 */
	public void drawPixel(int row, int column, float r, float g, float b) {
		panel.drawPixel(column, getHeight() - row, r, g, b);
	}

	public int getWidth() {
		return panel.getWidth();
	}

	public int getHeight() {
		return panel.getHeight();
	}

	public void setTitle(String name) {
		frame.setTitle(name);
	}

	public void clear() {
		panel.clear(0, 0, 0);
	}

	public void redraw() {
		panel.repaint();
		panel.flush();
	}
	
	public void saveImage(String file)
	{
		panel.saveImage(file);
	}

	private void wait(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Point p = e.getPoint();
		frame.setTitle("row: "+(getHeight()-p.y+" col: "+p.x));
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
