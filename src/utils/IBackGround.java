package utils;

import java.awt.Color;

import structs.Vector3;

public interface IBackGround {
	
	public Color getColorFromDirection(Vector3 dir);	

}
