package utils;

import java.awt.Color;
import java.awt.image.BufferedImage;

import structs.Vector3;

public class ImageBackGround implements IBackGround{

	private BufferedImage img;
	
	public float yRot = 0;
	
	public ImageBackGround(BufferedImage img)
	{
		this.img = img;
	}
	
	@Override
	public Color getColorFromDirection(Vector3 dir) {		
		float radius = 1;
		dir.normalize();
		float theta = (float) Math.acos((dir.Y) / radius);
		float phi = -(float) Math.atan2(dir.Z, dir.X);
		phi += (float) Math.PI - yRot;
		if (phi < 0)
			phi += (float) Math.PI * 2;

		float u = (float) (phi / (2 * Math.PI));
		float v = (float) ((Math.PI - theta) / Math.PI);
		int x = img.getWidth() - (int) ((float) u * img.getWidth());
		int y = img.getHeight() - (int) ((float) v * img.getHeight());

		if (x < 0)
			x = 0;
		if (y < 0)
			y = 0;
		if (x >= img.getWidth())
			x = img.getWidth() - 1;
		if (y >= img.getHeight())
			y = img.getHeight() - 1;

		int rgb = img.getRGB(x, y);
		return new Color(rgb);
	}

}
