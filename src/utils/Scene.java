package utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import lights.AreaLight;
import lights.ILight;
import raytracing.ICamera;
import raytracing.Ray;
import raytracing.RaycastResult;
import shading.AmbientShader;
import shading.IShader;
import structs.NbIntersectionsManager;
import structs.StandardValues;
import structs.Vector3;
import worldobjects.IWorldObject;

public class Scene {

	public ArrayList<IWorldObject> worldObjects;
	public ArrayList<ICamera> cameras;
	public ArrayList<IShader> shaders;
	public ArrayList<ILight> lights;
	
	public IBackGround backGround;
	
	public boolean shadowsEnabled;
	public boolean reflectionsEnabled;
	public int reflectionsDepth;
	public boolean falseColorEnabled;

	private int antialiasing = 1;
	private float OneOverAntialising = 1;
	private boolean antialiasingActive = false;
	private Random rg;
	private NbIntersectionsManager falseColorManager = NbIntersectionsManager.getInstance();
	private long time;
	public boolean printInfo = true;
	
	public int glossQuality = 16; //nb of rays to shoot for calculating glossy reflections

	public Scene() {
		worldObjects = new ArrayList<IWorldObject>();
		cameras = new ArrayList<ICamera>();
		shaders = new ArrayList<IShader>();
		lights = new ArrayList<ILight>();
		backGround = new SimpleBackGround(new Color(0,0,0));
		rg = new Random();
	}

	public void render(RenderPanel panel, ICamera camera) {
		render(panel, camera, "Data/autosave/autosave.png");
	}

	public void render(RenderPanel panel, ICamera camera, String path) {

		panel.clear();

		time = System.nanoTime();
		for (int col = 0; col < panel.getWidth(); col++) {
			for (int row = 0; row <= panel.getHeight(); row++) {
				
				if (row == 260 && col == 195) {
					if (printInfo)
						System.out.println("Reached row " + row + ", col " + col);
				}

				panel.drawPixel(row, col, getColorAtPixel(camera, row, col));

				if (falseColorEnabled)
					panel.drawPixel(row, col, falseColorManager.getColorFromNbIntersections());
				
//				if (row == 408 && col == 370) {
//					panel.drawPixel(row, col, new Color(255,0,0));
//				}
			}
		}

		// panel.redraw();

		time = System.nanoTime() - time;

		if (printInfo) {
			System.out.println("--------------------");
			System.out.println("Done rendering!");
			System.out.println("Time spent rendering: " + time * 0.000001f + " ms");
			System.out.println("--------------------");
			if (falseColorEnabled)
				System.out.println("Max intersections: " + falseColorManager.MaxSoFar);
		}

		panel.saveImage(path);
	}

	private Color getColorAtPixel(ICamera camera, int row, int col) {
		Ray[] rays;
		Vector3 color = new Vector3(0, 0, 0);

		if (antialiasingActive) {
			float ksi01;
			float ksi02;
			for (int p = 0; p < antialiasing; p++) {
				for (int q = 0; q < antialiasing; q++) {

					ksi01 = rg.nextFloat();
					ksi02 = rg.nextFloat();

					rays = camera.getRay(row + (p + ksi01) * OneOverAntialising, col + (q + ksi02) * OneOverAntialising);

					for (Ray ray : rays) {

						RaycastResult result = raycastScene(ray, 0, Integer.MAX_VALUE);
						Color temp;

						if (result != null)
							temp = getShadingColor(result);
						else
							temp = backGround.getColorFromDirection(ray.Direction);

						color.X += temp.getRed();
						color.Y += temp.getGreen();
						color.Z += temp.getBlue();
					}

					float OneOverNbRays = (float) 1 / rays.length;
					color.X = color.X * OneOverNbRays;
					color.Y = color.Y * OneOverNbRays;
					color.Z = color.Z * OneOverNbRays;
				}
			}
			color = color.multiply(OneOverAntialising * OneOverAntialising);
			color = capColor(color);
			
			return new Color((int) color.X, (int) color.Y, (int) color.Z);

		} else {
			rays = camera.getRay(row, col);

			for (Ray ray : rays) {

				RaycastResult result = raycastScene(ray, 0, Integer.MAX_VALUE);
				Color temp;

				if (result != null) {
					temp = getShadingColor(result);
				} else
					temp = backGround.getColorFromDirection(ray.Direction);

				color.X += temp.getRed();
				color.Y += temp.getGreen();
				color.Z += temp.getBlue();
			}

			float OneOverNbRays = (float) 1 / rays.length;
			color.X = color.X * OneOverNbRays;
			color.Y = color.Y * OneOverNbRays;
			color.Z = color.Z * OneOverNbRays;
			color = capColor(color);
			
			return new Color((int) color.X, (int) color.Y, (int) color.Z);
		}
	}
	
	private Vector3 capColor(Vector3 c)
	{
		float r = c.X;
		if(r > 255)
			r = 255;
		float g = c.Y;
		if(g > 255)
			g = 255;
		float b = c.Z;
		if(b > 255)
			b = 255;
		
		return new Vector3(r,g,b);
	}

	private RaycastResult raycastScene(Ray ray, float minDist, float maxDist) {
		RaycastResult result = new RaycastResult();

		for (IWorldObject obj : worldObjects) {
			RaycastResult tempRes = new RaycastResult();
			if (obj.isHit(ray, minDist, maxDist, tempRes)) {
				if (tempRes.distance > 0 && tempRes.distance < result.distance)
					result = tempRes;
			}
		}

		if (result.distance == Integer.MAX_VALUE)
			return null;
		return result;
	}

	private Color getShadingColor(RaycastResult res) {
		if (shaders.size() == 0 || lights.size() == 0)
			return res.diffuseColor;

		Vector3 color = new Vector3(0, 0, 0);
		Vector3 hitPoint = res.ray.Origin.plus(res.ray.Direction.multiply(res.distance));

		for (ILight light : lights) {

			int nbSamples = 1;
			if (light instanceof AreaLight)
				nbSamples = ((AreaLight) light).nbSamples; // for
															// softShadows-effect

			Vector3 tempColor = new Vector3(0, 0, 0);

			for (int N = 0; N < nbSamples; N++) {

				Vector3 lightDir = light.getLightDirection(hitPoint);

				boolean isInShadow = false;
				if (shadowsEnabled && res.selfllumination <= 1) {
					Ray shadowRay = new Ray();
					shadowRay.Origin = hitPoint;
					shadowRay.setDirection(lightDir);
					shadowRay.isShadowRay = true;
					isInShadow = hitSceneForShadows(shadowRay, 0.001f, light.getDistanceToLight(hitPoint));
				}

				for (IShader shader : shaders) {

					if (shader instanceof AmbientShader) { // Ambient is always
															// calculated
						Color shade = shader.getShading(res, lightDir, light.getColor());
						tempColor = tempColor.plus(new Vector3(shade.getRed(), shade.getGreen(), shade.getBlue()));
					} else if (!isInShadow) // Only calculate other shaders when
											// not in shadow
					{
						Color shade = shader.getShading(res, lightDir, light.getColor());
						tempColor = tempColor.plus(new Vector3(shade.getRed(), shade.getGreen(), shade.getBlue()));

					}
				}
			}

			color = color.plus(tempColor.multiply((float) 1 / nbSamples));
		}

		if (reflectionsEnabled && res.refractive) {
			calculateRefractions(res, color);
		} else if (reflectionsEnabled && res.reflective) {
			calculateReflections(res, color);
		}

		if (res.selfllumination != 1) {
			color = color.multiply(res.selfllumination);
		}

		if (color.X > 255)
			color.X = 255;
		if (color.Y > 255)
			color.Y = 255;
		if (color.Z > 255)
			color.Z = 255;

		return new Color((int) color.X, (int) color.Y, (int) color.Z);
	}

	private boolean hitSceneForShadows(Ray shadowRay, float minValue, float maxValue) {
		for (IWorldObject obj : worldObjects) {
			if (!obj.castsShadow())
				continue;

			if (obj.isHit(shadowRay, minValue, maxValue, new RaycastResult())) {
				return true;
			}
		}
		return false;
	}

	private void calculateReflections(RaycastResult result, Vector3 currentColor) {

		if (reflectionsDepth <= 0)
			return;

		if (result.specularColor.getRed() == 0 && result.specularColor.getGreen() == 0 && result.specularColor.getBlue() == 0)
			return;		

		reflectionsDepth--;
		Vector3 intersectPoint = result.ray.Origin.plus(result.ray.Direction.multiply(result.distance));
		Vector3 newDirection = result.ray.Direction.plus(result.normal.multiply(-2 * result.ray.Direction.dot(result.normal)));
		
		int nbRays = 1;
		if(result.glossy)
			nbRays = glossQuality;
		
		Vector3 color = new Vector3(0,0,0);
		for(int i = 0; i< nbRays ; i++)
		{
			Ray newRay = new Ray();
			newRay.Origin = intersectPoint;
			newRay.setDirection(newDirection);
			
			if(result.glossy)
			{
				float a = result.glossAmount;
				float uPert = -0.5f*a + rg.nextFloat()*a; //perturbation in u-direction
				float vPert = -0.5f*a + rg.nextFloat()*a;
				newRay.Direction.X += uPert*result.orthnormBasis[0].X + vPert*result.orthnormBasis[1].X;
				newRay.Direction.Y += uPert*result.orthnormBasis[0].Y + vPert*result.orthnormBasis[1].Y;
				newRay.Direction.Z += uPert*result.orthnormBasis[0].Z + vPert*result.orthnormBasis[1].Z;
			}
			
			RaycastResult reflResult = raycastScene(newRay, 0.001f, Integer.MAX_VALUE);

			Color reflectionColor = backGround.getColorFromDirection(newRay.Direction);
			if (reflResult != null) {
				reflectionColor = getShadingColor(reflResult);
			}
			
			color.X += reflectionColor.getRed();
			color.Y += reflectionColor.getGreen();
			color.Z += reflectionColor.getBlue();
		}

		float OneOverNbRays = (float) 1 / nbRays;
		color.X = color.X * OneOverNbRays;
		color.Y = color.Y * OneOverNbRays;
		color.Z = color.Z * OneOverNbRays;		

		currentColor.X += (float) result.specularColor.getRed() / 255 * color.X;
		currentColor.Y += (float) result.specularColor.getGreen() / 255 * color.Y;
		currentColor.Z += (float) result.specularColor.getBlue() / 255 * color.Z;

		reflectionsDepth++;
	}

	public void setAntialiasing(int n) {
		antialiasing = n;
		OneOverAntialising = (float) 1 / n;
		antialiasingActive = true;
	}

	private void calculateRefractions(RaycastResult result, Vector3 color) {
		Vector3 d = result.ray.Direction;
		Vector3 n = result.normal;
		float kr = 0;
		float kg = 0;
		float kb = 0;
		float c = 0;

		Vector3 reflectionColor = new Vector3(0, 0, 0);
		calculateReflections(result, reflectionColor);
		Vector3 t = new Vector3(0, 0, 0);

		if (d.dot(n) < 0) {
			refract(d, n, result.refractionIndex, t);
			c = -d.dot(n);
			kr = 1;
			kg = 1;
			kb = 1;
		} else {
			kr = (float) Math.exp(-result.refractiveAttenuation.X);
			kg = (float) Math.exp(-result.refractiveAttenuation.Y);
			kb = (float) Math.exp(-result.refractiveAttenuation.Z);

			if (refract(d, n.multiply(-1), 1 / result.refractionIndex, t))
				c = t.dot(n);
			else {

				color.X += kr * reflectionColor.X;
				color.Y += kg * reflectionColor.Y;
				color.Z += kb * reflectionColor.Z;
				return;
			}
		}

		float R0 = (result.refractionIndex - 1) * (result.refractionIndex - 1) / ((result.refractionIndex + 1) * (result.refractionIndex + 1));
		float R = (float) (R0 + (1 - R0) * Math.pow(1 - c, 5));

		Ray newRay = new Ray();
		newRay.Origin = result.ray.Origin.plus(result.ray.Direction.multiply(result.distance));
		newRay.setDirection(t);
		RaycastResult refrResult = raycastScene(newRay, 0.001f, Integer.MAX_VALUE);

		Vector3 refractionColor = new Vector3(0, 0, 0);
		Vector3 temp = new Vector3(backGround.getColorFromDirection(newRay.Direction));
		if (refrResult != null) {
			temp = new Vector3(getShadingColor(refrResult));
		}

		refractionColor.X = temp.X;
		refractionColor.Y = temp.Y;
		refractionColor.Z = temp.Z;

		color.X += kr * (R * reflectionColor.X + (1 - R) * refractionColor.X);
		color.Y += kr * (R * reflectionColor.Y + (1 - R) * refractionColor.Y);
		color.Z += kr * (R * reflectionColor.Z + (1 - R) * refractionColor.Z);
	}

	private boolean refract(Vector3 d, Vector3 n, float nt, Vector3 t) {
		float dn = d.dot(n);
		float factor = 1 - ((1 - dn * dn) / nt * nt);
		if (factor < 0)
			return false;

		Vector3 partOne = (d.minus(n.multiply(dn)).multiply(1 / nt));
		Vector3 partTwo = n.multiply((float) Math.sqrt(factor));
		Vector3 temp = partOne.minus(partTwo);
		t.X = temp.X;
		t.Y = temp.Y;
		t.Z = temp.Z;
		return true;
	}
}
