package shading;

import java.awt.Color;

import raytracing.RaycastResult;
import structs.Vector3;

public class AmbientShader implements IShader {

	private Color color;

	public AmbientShader(Color c) {
		color = c;
	}

	@Override
	public Color getShading(RaycastResult result, Vector3 dirToLight, Color lightColor) {
		
		float r = (float)result.diffuseColor.getRed() / 255 * color.getRed() / 255;
		float g = (float)result.diffuseColor.getGreen() / 255 * color.getGreen() / 255;
		float b = (float)result.diffuseColor.getBlue() / 255 * color.getBlue() / 255;
		Color c = new Color(r, g, b);
		
		return c;
	}

}
