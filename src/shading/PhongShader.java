package shading;

import java.awt.Color;

import raytracing.RaycastResult;
import structs.Vector3;

public class PhongShader implements IShader {

	@Override
	public Color getShading(RaycastResult result, Vector3 dirToLight, Color lightColor) {
		if (result.normal == null)
			return new Color(0, 0, 0);

		Vector3 r = dirToLight.multiply(-1).plus(result.normal.multiply(2*dirToLight.dot(result.normal)));
		float temp = result.ray.Direction.multiply(-1).dot(r);
		if(temp < 0)
			temp = 0;
		float phongValue = (float) Math.pow(temp, result.specularIntensity);

		float red = (float) result.specularColor.getRed() / 255 * lightColor.getRed()* phongValue;
		float green = (float) result.specularColor.getGreen() / 255 * lightColor.getGreen()* phongValue;
		float blue = (float) result.specularColor.getBlue() / 255 * lightColor.getBlue()* phongValue;	
		
		if(red > 255)
			red = 255;
		if(green > 255)
			green = 255;
		if(blue > 255)
			blue = 255;
		
		Color c = new Color((int)red, (int)green, (int)blue);
		return c;
	}

}
