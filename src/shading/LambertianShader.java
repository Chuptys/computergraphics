package shading;

import java.awt.Color;

import raytracing.RaycastResult;
import structs.Vector3;

public class LambertianShader implements IShader {

	@Override
	public Color getShading(RaycastResult result, Vector3 dirToLight, Color lightColor) {
		if (result.normal == null)
			return result.diffuseColor;

		float theta = 0;
		float temp = result.normal.dot(dirToLight);
		if (temp > 0)
			theta = temp;

		float r = (float) result.diffuseColor.getRed() / 255 * lightColor.getRed() * theta;
		float g = (float) result.diffuseColor.getGreen() / 255 * lightColor.getGreen() * theta;
		float b = (float) result.diffuseColor.getBlue() / 255 * lightColor.getBlue() * theta;

		if (r > 255)
			r = 255;
		if (g > 255)
			g = 255;
		if (b > 255)
			b = 255;

		Color c = new Color((int) r, (int) g, (int) b);
		return c;
	}

}
