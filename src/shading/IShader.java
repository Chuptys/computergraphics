package shading;

import java.awt.Color;

import raytracing.RaycastResult;
import structs.Vector3;

public interface IShader {

	public Color getShading(RaycastResult result, Vector3 dirToLight, Color lightColor);
}
