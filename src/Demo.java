import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import lights.AreaLight;
import lights.DirectionalLight;
import lights.PointLight;
import parser.ImageLoader;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector3;
import utils.ImageBackGround;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.IWorldObject;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.BSPTree;
import acceleration.BVHNodeTree;
import acceleration.CompactGrid;
import animation.AnimatedCamera;
import animation.Animation;

public class Demo {

	public void stonehenge() {
		int size = 80;
		MyDimension dim = new MyDimension(16 * size, 9 * size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Stonehenge");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(120);
		Matrix t = Matrix.translation(new Vector3(-15, 1, 0));
		camera.setWorldMatrix(t);

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightPos = new Vector3(0, 1, 0);
		PointLight light = new PointLight(new Color(200, 200, 200), lightPos);

		TriangleMesh ground = MeshLoader.load("Data/OBJ/Stonehenge/StonehengeGround001");

		TriangleMesh part01 = MeshLoader.load("Data/OBJ/Stonehenge/StonehengePart01");
		part01.setNormalMap(ImageLoader.loadImage("Data/OBJ/Stonehenge/maps/sh_grey_normal.jpg"));
		TriangleMesh part02 = MeshLoader.load("Data/OBJ/Stonehenge/StonehengePart02");
		part02.setNormalMap(ImageLoader.loadImage("Data/OBJ/Stonehenge/maps/sh_grey_normal.jpg"));
		TriangleMesh part03 = MeshLoader.load("Data/OBJ/Stonehenge/StonehengePart03");
		part03.setNormalMap(ImageLoader.loadImage("Data/OBJ/Stonehenge/maps/sh_grey_normal.jpg"));
		TriangleMesh part04 = MeshLoader.load("Data/OBJ/Stonehenge/StonehengePart04");
		part04.setNormalMap(ImageLoader.loadImage("Data/OBJ/Stonehenge/maps/sh_grey_normal.jpg"));
		TriangleMesh part05 = MeshLoader.load("Data/OBJ/Stonehenge/StonehengePart05");
		part05.setNormalMap(ImageLoader.loadImage("Data/OBJ/Stonehenge/maps/sh_grey_normal.jpg"));

		Sphere sphere = new Sphere(lightPos, 0.2f);
		sphere.diffuseColor = new Color(255, 255, 255);
		sphere.castsShadow = false;
		sphere.selfIllumination = 10;

		CompactGrid grid = new CompactGrid();
		grid.addObject(part01);
		grid.addObject(part02);
		grid.addObject(part03);
		grid.addObject(part04);
		grid.addObject(part05);
		grid.addObject(sphere);
		grid.addObject(ground);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.setAntialiasing(4);

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);

		// scene.backGround = new
		// ImageBackGround(ImageLoader.loadImage("Data/Resources/StarMap.png"));

		AnimatedCamera animCam = new AnimatedCamera(camera);
		animCam.addPosition(new Vector3(-30, 1.5f, 0), 0);
		animCam.addRotationYAfter((float) Math.PI * 2, 8000);

		Animation animation = new Animation(scene, camera, panel, 24, "Stonehenge");
		animation.addAnimatable(animCam);

		animation.render(0, 8000);

		// scene.render(panel, camera);
	}

	public void reventon() {
		int size = 30;
		MyDimension dim = new MyDimension(16 * size, 9 * size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Lamborghini Reventon");

		CompactGrid grid = new CompactGrid();

		// Color bodyColor = new Color(62,81,72);
		// Color bodyColor = new Color(100,20,20);
		Color bodyColor = new Color(200, 175, 15);
		// Color bodyColor = new Color(5,5,5);
		// Color bodyColor = new Color(200,200,200);

		// Color floorColor = new Color(10,10,10);
		Color floorColor = new Color(0, 0, 0);

		String pth = "Data/OBJ/Revento/";
		TriangleMesh backLightCask = MeshLoader.load(pth + "BackLightCask");
		backLightCask.diffuseColor = new Color(5, 5, 5);
		backLightCask.specularIntensity = 164;
		grid.addObject(backLightCask);

		TriangleMesh backLightGlass = MeshLoader.load(pth + "BackLightGlass");
		backLightGlass.diffuseColor = new Color(100, 150, 150);
		backLightGlass.castsShadow = false;
		backLightGlass.refractive = true;
		backLightGlass.refractionIndex = 1.2f;
		backLightGlass.specularIntensity = 256;
		backLightGlass.refractiveAttenuation = new Vector3(0.01f, 0.01f, 0.01f);
		backLightGlass.specularColor = new Color(200, 255, 255);
		grid.addObject(backLightGlass);

		TriangleMesh backLightLEDRed = MeshLoader.load(pth + "BackLightLEDRed");
		backLightLEDRed.diffuseColor = new Color(255, 0, 0);
		backLightLEDRed.specularIntensity = 256;
		backLightLEDRed.specularColor = new Color(255, 255, 255);
		backLightLEDRed.selfIllumination = 8;
		grid.addObject(backLightLEDRed);

		TriangleMesh backLightLEDWhite = MeshLoader.load(pth + "BackLightLEDWhite");
		backLightLEDWhite.diffuseColor = new Color(255, 255, 255);
		backLightLEDWhite.specularIntensity = 256;
		backLightLEDWhite.specularColor = new Color(255, 255, 255);
		backLightLEDWhite.selfIllumination = 5;
		grid.addObject(backLightLEDWhite);

		TriangleMesh backLightPart01 = MeshLoader.load(pth + "BackLightPart01");
		backLightPart01.diffuseColor = new Color(5, 5, 5);
		backLightPart01.specularIntensity = 164;
		grid.addObject(backLightPart01);

		TriangleMesh backLightPart02 = MeshLoader.load(pth + "BackLightPart02");
		backLightPart02.diffuseColor = new Color(100, 100, 100);
		backLightPart02.specularIntensity = 164;
		backLightPart02.specularColor = new Color(150, 150, 150);
		grid.addObject(backLightPart02);

		TriangleMesh backLightPart03 = MeshLoader.load(pth + "BackLightPart03");
		backLightPart03.diffuseColor = new Color(150, 30, 30);
		backLightPart03.castsShadow = false;
		backLightPart03.refractive = true;
		backLightPart03.refractionIndex = 1.8f;
		backLightPart03.specularIntensity = 256;
		backLightPart03.refractiveAttenuation = new Vector3(0.01f, 0.01f, 0.01f);
		backLightPart03.specularColor = new Color(255, 100, 100);
		grid.addObject(backLightPart03);

		TriangleMesh backLogo = MeshLoader.load(pth + "BackLogo");
		backLogo.diffuseColor = new Color(0, 0, 0);
		backLogo.specularIntensity = 256;
		backLogo.specularColor = new Color(255, 255, 255);
		grid.addObject(backLogo);

		TriangleMesh backTopRed = MeshLoader.load(pth + "BackTopRed");
		backTopRed.diffuseColor = new Color(150, 30, 30);
		backTopRed.castsShadow = false;
		backTopRed.refractive = true;
		backTopRed.refractionIndex = 1.8f;
		backTopRed.specularIntensity = 256;
		backTopRed.refractiveAttenuation = new Vector3(0.01f, 0.01f, 0.01f);
		backTopRed.specularColor = new Color(255, 100, 100);
		grid.addObject(backTopRed);

		TriangleMesh backVents = MeshLoader.load(pth + "BackVents");
		backVents.diffuseColor = new Color(5, 5, 5);
		backVents.specularIntensity = 256;
		grid.addObject(backVents);

		TriangleMesh blackStuffInterior = MeshLoader.load(pth + "BlackStuffInterior");
		blackStuffInterior.diffuseColor = new Color(5, 5, 5);
		blackStuffInterior.specularIntensity = 64;
		grid.addObject(blackStuffInterior);

		TriangleMesh body = MeshLoader.load(pth + "Body");
		body.diffuseColor = bodyColor;
		body.reflective = true;
		body.specularIntensity = 256;
		body.specularColor = new Color(100, 100, 100);
		grid.addObject(body);

		TriangleMesh brakes = MeshLoader.load(pth + "Brakes");
		brakes.diffuseColor = new Color(245, 184, 0);
		brakes.reflective = true;
		brakes.specularIntensity = 128;
		brakes.specularColor = new Color(245, 184, 0);
		grid.addObject(brakes);

		TriangleMesh brakeText = MeshLoader.load(pth + "BrakeText");
		brakeText.diffuseColor = new Color(0, 0, 0);
		brakeText.specularIntensity = 256;
		brakeText.specularColor = new Color(255, 255, 255);
		grid.addObject(brakeText);

		TriangleMesh chassis = MeshLoader.load(pth + "Chassis");
		chassis.diffuseColor = new Color(0, 0, 0);
		chassis.specularIntensity = 16;
		grid.addObject(chassis);

		TriangleMesh chromeStuff = MeshLoader.load(pth + "ChromeStuff");
		// chromeStuff.diffuseColor = new Color(200,200,200);
		chromeStuff.diffuseColor = new Color(50, 50, 50);
		chromeStuff.reflective = true;
		chromeStuff.specularIntensity = 256;
		chromeStuff.specularColor = new Color(200, 200, 200);
		grid.addObject(chromeStuff);

		TriangleMesh darkSidePanels = MeshLoader.load(pth + "DarkSidePanels");
		darkSidePanels.diffuseColor = new Color(5, 5, 5);
		darkSidePanels.specularIntensity = 256;
		grid.addObject(darkSidePanels);

		TriangleMesh diskBrakes = MeshLoader.load(pth + "DiskBrakes");
		diskBrakes.diffuseColor = new Color(255, 255, 255);
		diskBrakes.reflective = true;
		diskBrakes.specularIntensity = 64;
		diskBrakes.specularColor = new Color(255, 255, 255);
		grid.addObject(diskBrakes);

		TriangleMesh doorKeySlots = MeshLoader.load(pth + "DoorKeySlots");
		doorKeySlots.diffuseColor = new Color(5, 5, 5);
		doorKeySlots.specularIntensity = 256;
		doorKeySlots.specularColor = new Color(100, 100, 100);
		grid.addObject(doorKeySlots);

		TriangleMesh doorRims = MeshLoader.load(pth + "DoorRims");
		doorRims.diffuseColor = new Color(5, 5, 5);
		doorRims.reflective = true;
		doorRims.specularColor = new Color(50, 50, 50);
		doorRims.specularIntensity = 128;
		grid.addObject(doorRims);

		TriangleMesh doorsInterior = MeshLoader.load(pth + "DoorsInterior");
		doorsInterior.diffuseColor = new Color(30, 30, 30);
		doorsInterior.specularIntensity = 64;
		grid.addObject(doorsInterior);

		TriangleMesh exhaust = MeshLoader.load(pth + "Exhaust");
		exhaust.diffuseColor = new Color(100, 100, 100);
		exhaust.reflective = true;
		exhaust.specularIntensity = 256;
		exhaust.specularColor = new Color(150, 150, 150);
		grid.addObject(exhaust);

		TriangleMesh frontLED = MeshLoader.load(pth + "FrontLED001");
		frontLED.diffuseColor = new Color(255, 255, 255);
		frontLED.specularIntensity = 256;
		frontLED.specularColor = new Color(255, 255, 255);
		frontLED.selfIllumination = 5;
		grid.addObject(frontLED);

		TriangleMesh frontLightPart01 = MeshLoader.load(pth + "FrontLightPart01");
		frontLightPart01.diffuseColor = new Color(255, 255, 255);
		frontLightPart01.specularIntensity = 256;
		frontLightPart01.specularColor = new Color(255, 255, 255);
		frontLightPart01.selfIllumination = 5;
		grid.addObject(frontLightPart01);

		TriangleMesh frontLightPart02 = MeshLoader.load(pth + "FrontLightPart02");
		frontLightPart02.diffuseColor = new Color(0, 150, 255);
		frontLightPart02.specularIntensity = 256;
		frontLightPart02.specularColor = new Color(255, 255, 255);
		frontLightPart02.selfIllumination = 5;
		grid.addObject(frontLightPart02);

		TriangleMesh frontLightPart03 = MeshLoader.load(pth + "FrontLightPart03001");
		frontLightPart03.diffuseColor = new Color(5, 5, 5);
		frontLightPart03.specularIntensity = 256;
		frontLightPart03.specularColor = new Color(200, 200, 200);
		grid.addObject(frontLightPart03);

		TriangleMesh frontObject = MeshLoader.load(pth + "FrontObject");
		frontObject.diffuseColor = new Color(10, 10, 10);
		frontObject.specularIntensity = 256;
		frontObject.specularColor = new Color(100, 100, 100);
		grid.addObject(frontObject);

		TriangleMesh glassClear = MeshLoader.load(pth + "GlassClear001");
		glassClear.diffuseColor = new Color(50, 100, 100);
		glassClear.castsShadow = false;
		glassClear.refractive = true;
		glassClear.refractionIndex = 1.4f;
		glassClear.specularColor = new Color(100, 100, 100);
		grid.addObject(glassClear);

		TriangleMesh glassInteriorLights = MeshLoader.load(pth + "GlassInteriorLights");
		glassInteriorLights.diffuseColor = new Color(200, 255, 255);
		glassInteriorLights.refractive = true;
		glassInteriorLights.refractionIndex = 1.7f;
		glassInteriorLights.specularColor = new Color(100, 100, 100);
		grid.addObject(glassInteriorLights);

		TriangleMesh interior01 = MeshLoader.load(pth + "Interior01");
		interior01.diffuseColor = new Color(12, 12, 10);
		interior01.specularIntensity = 4;
		grid.addObject(interior01);

		TriangleMesh interiorStuff = MeshLoader.load(pth + "InteriorStuff");
		interiorStuff.diffuseColor = new Color(15, 15, 15);
		interiorStuff.specularIntensity = 4;
		grid.addObject(interiorStuff);

		TriangleMesh interiorBottom = MeshLoader.load(pth + "InteriorBottom");
		interiorBottom.diffuseColor = new Color(5, 5, 5);
		interiorBottom.specularIntensity = 4;
		interiorBottom.specularColor = new Color(0, 0, 0);
		grid.addObject(interiorBottom);

		TriangleMesh mirrorCasks = MeshLoader.load(pth + "MirrorCasks");
		mirrorCasks.diffuseColor = bodyColor;
		mirrorCasks.reflective = true;
		mirrorCasks.specularIntensity = 256;
		mirrorCasks.specularColor = new Color(100, 100, 100);
		grid.addObject(mirrorCasks);

		TriangleMesh mirrors = MeshLoader.load(pth + "Mirrors");
		mirrors.diffuseColor = new Color(0, 0, 0);
		mirrors.reflective = true;
		mirrors.specularIntensity = 256;
		mirrors.specularColor = new Color(255, 255, 255);
		grid.addObject(mirrors);

		TriangleMesh motorLights = MeshLoader.load(pth + "MotorLights");
		motorLights.diffuseColor = new Color(255, 255, 255);
		motorLights.specularIntensity = 256;
		motorLights.specularColor = new Color(255, 255, 255);
		motorLights.selfIllumination = 5;
		grid.addObject(motorLights);

		TriangleMesh motorPart01 = MeshLoader.load(pth + "MotorPart01");
		motorPart01.diffuseColor = new Color(150, 150, 150);
		motorPart01.reflective = true;
		motorPart01.specularIntensity = 256;
		motorPart01.specularColor = new Color(255, 255, 255);
		grid.addObject(motorPart01);

		TriangleMesh motorPart02 = MeshLoader.load(pth + "MotorPart02");
		motorPart02.diffuseColor = new Color(10, 10, 10);
		motorPart02.reflective = true;
		motorPart02.specularIntensity = 256;
		motorPart02.specularColor = new Color(200, 200, 200);
		grid.addObject(motorPart02);

		TriangleMesh motorPart03 = MeshLoader.load(pth + "MotorPart03");
		motorPart03.diffuseColor = new Color(100, 100, 100);
		motorPart03.reflective = true;
		motorPart03.specularIntensity = 256;
		motorPart03.specularColor = new Color(255, 255, 255);
		grid.addObject(motorPart03);

		TriangleMesh motorText = MeshLoader.load(pth + "MotorText");
		motorText.diffuseColor = new Color(200, 200, 200);
		motorText.reflective = true;
		motorText.specularIntensity = 256;
		motorText.specularColor = new Color(200, 200, 200);
		grid.addObject(motorText);

		TriangleMesh noseLogoPart01 = MeshLoader.load(pth + "NoseLogoPart01001");
		noseLogoPart01.diffuseColor = new Color(10, 10, 10);
		noseLogoPart01.reflective = true;
		noseLogoPart01.specularIntensity = 256;
		noseLogoPart01.specularColor = new Color(100, 100, 100);
		grid.addObject(noseLogoPart01);

		TriangleMesh noseLogoPart02 = MeshLoader.load(pth + "NoseLogoPart02");
		noseLogoPart02.diffuseColor = new Color(245, 184, 0);
		noseLogoPart02.reflective = true;
		noseLogoPart02.specularIntensity = 256;
		noseLogoPart02.specularColor = new Color(245, 184, 0);
		grid.addObject(noseLogoPart02);

		TriangleMesh rimsBolts = MeshLoader.load(pth + "RimBolts");
		rimsBolts.diffuseColor = new Color(50, 50, 50);
		rimsBolts.reflective = true;
		rimsBolts.specularIntensity = 256;
		rimsBolts.specularColor = new Color(100, 100, 100);
		grid.addObject(rimsBolts);

		TriangleMesh rimLogoPart01 = MeshLoader.load(pth + "RimLogoPart01001");
		rimLogoPart01.diffuseColor = new Color(50, 50, 20);
		grid.addObject(rimLogoPart01);

		TriangleMesh rimLogoPart02 = MeshLoader.load(pth + "RimLogoPart02");
		rimLogoPart02.diffuseColor = new Color(245, 184, 0);
		grid.addObject(rimLogoPart02);

		TriangleMesh rims = MeshLoader.load(pth + "Rims");
		rims.diffuseColor = new Color(2, 2, 2);
		rims.reflective = true;
		rims.specularIntensity = 256;
		rims.specularColor = new Color(200, 200, 200);
		grid.addObject(rims);

		TriangleMesh rollCage = MeshLoader.load(pth + "RollCage");
		rollCage.diffuseColor = new Color(5, 5, 5);
		grid.addObject(rollCage);

		TriangleMesh seatBelts = MeshLoader.load(pth + "SeatBelts");
		seatBelts.diffuseColor = new Color(15, 15, 15);
		seatBelts.specularIntensity = 64;
		grid.addObject(seatBelts);

		TriangleMesh seatsPart01 = MeshLoader.load(pth + "SeatsPart01");
		grid.addObject(seatsPart01);

		TriangleMesh seatsPart02 = MeshLoader.load(pth + "SeatsPart02");
		seatsPart02.diffuseColor = new Color(10, 10, 10);
		seatsPart02.specularIntensity = 16;
		grid.addObject(seatsPart02);

		TriangleMesh seatsPart03 = MeshLoader.load(pth + "SeatsPart03");
		grid.addObject(seatsPart03);

		TriangleMesh sideExhaust = MeshLoader.load(pth + "SideExhaust");
		sideExhaust.diffuseColor = new Color(5, 5, 5);
		sideExhaust.reflective = true;
		sideExhaust.specularIntensity = 256;
		sideExhaust.specularColor = new Color(150, 150, 150);
		grid.addObject(sideExhaust);

		TriangleMesh stuurPart01 = MeshLoader.load(pth + "StuurPart01");
		stuurPart01.diffuseColor = new Color(10, 10, 10);
		stuurPart01.specularIntensity = 164;
		grid.addObject(stuurPart01);

		TriangleMesh stuurPart02 = MeshLoader.load(pth + "StuurPart02");
		stuurPart02.diffuseColor = new Color(2, 2, 2);
		stuurPart02.specularIntensity = 256;
		stuurPart02.specularColor = new Color(75, 75, 75);
		grid.addObject(stuurPart02);

		TriangleMesh stuurPart03 = MeshLoader.load(pth + "StuurPart03");
		stuurPart03.diffuseColor = new Color(0, 0, 0);
		stuurPart03.reflective = true;
		stuurPart03.specularIntensity = 256;
		stuurPart03.specularColor = new Color(255, 255, 255);
		grid.addObject(stuurPart03);

		TriangleMesh tyres = MeshLoader.load(pth + "Tyres001");
		tyres.diffuseColor = new Color(2, 2, 2);
		tyres.specularIntensity = 16;
		tyres.specularColor = new Color(50, 50, 50);
		grid.addObject(tyres);

		TriangleMesh wheelBolts = MeshLoader.load(pth + "WheelBolts");
		wheelBolts.diffuseColor = new Color(10, 10, 10);
		wheelBolts.reflective = true;
		wheelBolts.specularIntensity = 256;
		grid.addObject(wheelBolts);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/planeBig");
		plane.diffuseColor = floorColor;
		plane.reflective = true;
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, 0, 0)));

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/**
		 * Cameras
		 */
		PerspectiveCamera frontCamera = new PerspectiveCamera();
		frontCamera.setPixelDimensions(dim);
		frontCamera.setViewplaneDistance(75);
		frontCamera.setWorldMatrix(Matrix.translation(new Vector3(-300, 30, 0)));

		PerspectiveCamera frontSideCamera = new PerspectiveCamera();
		frontSideCamera.setPixelDimensions(dim);
		frontSideCamera.setViewplaneDistance(75);
		frontSideCamera.setWorldMatrix(Matrix.translation(new Vector3(-100, 20, 20)));

		PerspectiveCamera backCamera = new PerspectiveCamera();
		backCamera.setPixelDimensions(dim);
		backCamera.setViewplaneDistance(75);
		backCamera.setWorldMatrix(Matrix.translation(new Vector3(250, 35, 0)).multiply(Matrix.rotationY((float) Math.PI)));

		PerspectiveCamera sideCamera = new PerspectiveCamera();
		sideCamera.setPixelDimensions(dim);
		sideCamera.setViewplaneDistance(10);
		sideCamera.setWorldMatrix(Matrix.translation(new Vector3(-5, 30, 100)).multiply(Matrix.rotationY((float) Math.PI * 0.5f)));
		sideCamera.setFOV(0.5f);

		PerspectiveCamera wheelCamera = new PerspectiveCamera();
		wheelCamera.setPixelDimensions(dim);
		wheelCamera.setViewplaneDistance(75);
		wheelCamera.setWorldMatrix(Matrix.translation(new Vector3(-40, 10, 100)).multiply(Matrix.rotationY((float) Math.PI * 0.5f)));

		PerspectiveCamera topCamera = new PerspectiveCamera();
		topCamera.setPixelDimensions(dim);
		topCamera.setViewplaneDistance(75);
		Matrix topRotY = Matrix.rotationY((float) Math.PI * 0.5f);
		Matrix topRotZ = Matrix.rotationZ(-(float) Math.PI * 0.5f);
		Matrix topTrans = Matrix.translation(new Vector3(-10, 300, 0));
		topCamera.setWorldMatrix(topTrans.multiply(topRotY.multiply(topRotZ)));

		PerspectiveCamera topCamera02 = new PerspectiveCamera();
		topCamera02.setPixelDimensions(dim);
		topCamera02.setViewplaneDistance(75);
		Matrix top02RotY = Matrix.rotationY((float) Math.PI * 0f);
		Matrix top02RotZ = Matrix.rotationZ(-(float) Math.PI * 0.5f);
		Matrix top02Trans = Matrix.translation(new Vector3(30, 150, 0));
		topCamera02.setWorldMatrix(top02Trans.multiply(top02RotY.multiply(top02RotZ)));

		PerspectiveCamera backSideCamera = new PerspectiveCamera();
		backSideCamera.setPixelDimensions(dim);
		backSideCamera.setViewplaneDistance(75);
		backSideCamera.setWorldMatrix(Matrix.translation(new Vector3(150, 30, 30)).multiply(Matrix.rotationY((float) Math.PI)));

		PerspectiveCamera interiorCamera = new PerspectiveCamera();
		interiorCamera.setPixelDimensions(dim);
		interiorCamera.setViewplaneDistance(100);
		Matrix intRotY = Matrix.rotationY((float) Math.PI * 0.75f);
		Matrix intRotZ = Matrix.rotationZ(-(float) Math.PI * 0f);
		Matrix intTrans = Matrix.translation(new Vector3(75, 30, 100));
		interiorCamera.setWorldMatrix(intTrans.multiply(intRotY.multiply(intRotZ)));

		PerspectiveCamera interiorCamera02 = new PerspectiveCamera();
		interiorCamera02.setPixelDimensions(dim);
		interiorCamera02.setViewplaneDistance(35);
		Matrix int02RotY = Matrix.rotationY(-(float) Math.PI * 0.2f);
		Matrix int02RotZ = Matrix.rotationZ(-(float) Math.PI * 0.1f);
		Matrix int02Trans = Matrix.translation(new Vector3(-50, 35, -20));
		interiorCamera02.setWorldMatrix(int02Trans.multiply(int02RotY.multiply(int02RotZ)));

		PerspectiveCamera sweetShot01 = new PerspectiveCamera();
		sweetShot01.setPixelDimensions(dim);
		sweetShot01.setViewplaneDistance(25);
		Matrix sw01Trans = Matrix.translation(new Vector3(-120, 35, 60));
		Matrix sw01RotX = Matrix.rotationX(-(float) Math.PI * 0.05f);
		Matrix sw01RotY = Matrix.rotationY((float) Math.PI * 0.15f);
		Matrix sw01RotZ = Matrix.rotationZ(-(float) Math.PI * 0.25f);
		sweetShot01.setWorldMatrix(sw01Trans.multiply(sw01RotY.multiply(sw01RotX)));

		PerspectiveCamera sweetShot02 = new PerspectiveCamera();
		sweetShot02.setPixelDimensions(dim);
		sweetShot02.setViewplaneDistance(50);
		Matrix sw02Trans = Matrix.translation(new Vector3(-10, 22, 185));
		Matrix sw02RotX = Matrix.rotationX(-(float) Math.PI * 0f);
		Matrix sw02RotY = Matrix.rotationY((float) Math.PI * 0.5f);
		Matrix sw02RotZ = Matrix.rotationZ((float) Math.PI * 0.02f);
		sweetShot02.setWorldMatrix(sw02Trans.multiply(sw02RotY.multiply(sw02RotZ)));

		/**
		 * Lights
		 */
		int lightQuality = 50;

		PointLight light01 = new PointLight(new Color(255, 255, 255), new Vector3(-50, 100, 100));
		PointLight light02 = new PointLight(new Color(255, 255, 255), new Vector3(15, 100, 0));
		PointLight light03 = new PointLight(new Color(255, 255, 255), new Vector3(0, 150, 30));

		AreaLight light01Area = new AreaLight(new Vector3(-100, 100, 100), new Vector3(-10, 0, -10), new Vector3(10, 10, -10), new Color(255, 255, 255), lightQuality);
		AreaLight light03Area = new AreaLight(new Vector3(50, 50, 100), new Vector3(-10, 0, 10), new Vector3(-10, 10, -10), new Color(255, 255, 255), lightQuality);

		PointLight interiorLight = new PointLight(new Color(255, 255, 255), new Vector3(-5, 30, 0));
		DirectionalLight sun = new DirectionalLight(new Vector3(1, -1, 0), new Color(150, 150, 150));

		/**
		 * Shaders + Scene
		 */
		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 2;
		// scene.setAntialiasing(4);
		// scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		scene.cameras.add(frontCamera);
		scene.cameras.add(frontSideCamera);
		scene.cameras.add(sideCamera);
		scene.cameras.add(wheelCamera);
		scene.cameras.add(topCamera);
		scene.cameras.add(topCamera02);
		scene.cameras.add(sweetShot01);
		scene.cameras.add(sweetShot02);
		scene.cameras.add(backSideCamera);
		scene.cameras.add(interiorCamera);
		scene.cameras.add(interiorCamera02);

		scene.lights.add(sun);
		// scene.lights.add(interiorLight);
		// scene.lights.add(light01);
		// scene.lights.add(light01Area);
		// scene.lights.add(light02);
		scene.lights.add(light03);
		// scene.lights.add(light03Area);

		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		ImageBackGround bg = new ImageBackGround(ImageLoader.loadImage("Data/Resources/PanoBridgeEdited.jpg"));
		bg.yRot = (float) Math.PI * 1.75f; // for backShots
		scene.backGround = bg;

		// scene.backGround = new
		// ImageBackGround(ImageLoader.loadImage("Data/Resources/PanoEifelEdited.png"));

		scene.render(panel, frontCamera);

		// System.exit(0);
	}

	public void accStatistics01() {
		int size = 350;
		MyDimension dim = new MyDimension(size, size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Acc - Stat");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(75);
		Matrix t = Matrix.translation(new Vector3(-75, 10, 25));
		camera.setWorldMatrix(t);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightPos = new Vector3(-100, 100, -100);
		PointLight light = new PointLight(new Color(255, 255, 255), lightPos);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));
		plane.diffuseColor = new Color(148, 255, 112);

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(plane);
		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));

		ArrayList<Sphere> spheres = new ArrayList<Sphere>();

		CompactGrid grid = new CompactGrid();
		BVHNodeTree tree = new BVHNodeTree();

		for (int i = 10; i <= 1000000; i *= 10) {

			spheres.clear();
			int nbSpheres = i;
			Random rg = new Random();
			int xDim = 50;
			int yDim = 25;
			int zDim = 50;
			float radiusMax = 0.25f;
			for (int j = 0; j < nbSpheres; j++) {
				float x = rg.nextFloat() * xDim;
				float y = rg.nextFloat() * yDim;
				float z = rg.nextFloat() * zDim;
				float radius = rg.nextFloat() * radiusMax;

				Sphere s = new Sphere(new Vector3(x, y, z), radius);
				s.diffuseColor = new Color(rg.nextFloat(), rg.nextFloat(), rg.nextFloat());
				spheres.add(s);
			}

			// /**
			// * Grid
			// */
			// for(IWorldObject o : spheres)
			// {
			// grid.addObject(o);
			// }
			// try {
			// grid.initialize();
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// scene.worldObjects.add(grid);
			// scene.render(panel, camera);
			// scene.worldObjects.remove(grid);
			// grid = new CompactGrid();
			//
			// /**
			// * Tree
			// */
			// tree = new BVHNodeTree();
			// for(IWorldObject o : spheres)
			// {
			// tree.addObject(o);
			// }
			// try {
			// tree.initialize();
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// scene.worldObjects.add(tree);
			// scene.render(panel, camera);
			// scene.worldObjects.remove(tree);
			// tree = new BVHNodeTree();

			/**
			 * Barebones
			 */
			System.out.println("nbSpheres: " + i);
			for (IWorldObject o : spheres) {
				scene.worldObjects.add(o);
			}
			scene.render(panel, camera);
			for (IWorldObject o : spheres) {
				scene.worldObjects.remove(o);
			}
		}

		System.out.println("Statistics done!");
	}

	public void accStatistics02() {
		int size = 350;
		MyDimension dim = new MyDimension(size, size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Acc - Stat");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(75);
		Matrix t = Matrix.translation(new Vector3(-75, 10, 25));
		camera.setWorldMatrix(t);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightPos = new Vector3(-100, 100, -100);
		PointLight light = new PointLight(new Color(255, 255, 255), lightPos);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));
		plane.diffuseColor = new Color(148, 255, 112);

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(plane);
		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));

		ArrayList<Sphere> spheres = new ArrayList<Sphere>();

		CompactGrid grid = new CompactGrid();

		for (int i = 1; i <= 15; i++) {

			spheres.clear();
			int nbSpheres = 100000;
			Random rg = new Random();
			int xDim = 50;
			int yDim = 25;
			int zDim = 50;
			float radiusMax = 0.25f;
			for (int j = 0; j < nbSpheres; j++) {
				float x = rg.nextFloat() * xDim;
				float y = rg.nextFloat() * yDim;
				float z = rg.nextFloat() * zDim;
				float radius = rg.nextFloat() * radiusMax;

				Sphere s = new Sphere(new Vector3(x, y, z), radius);
				s.diffuseColor = new Color(rg.nextFloat(), rg.nextFloat(), rg.nextFloat());
				spheres.add(s);
			}

			/**
			 * Grid
			 */
			grid.density = i;
			for (IWorldObject o : spheres) {
				grid.addObject(o);
			}
			try {
				grid.initialize();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			scene.worldObjects.add(grid);
			scene.render(panel, camera);
			scene.worldObjects.remove(grid);
			grid = new CompactGrid();

		}

		System.out.println("Statistics done!");
	}

	public void accCorrectness() {
		int size = 50;
		MyDimension dim = new MyDimension(size*16, size*9);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Acc - Correctness");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(75);
		Matrix t = Matrix.translation(new Vector3(-75, 12, 25));
		camera.setWorldMatrix(t);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightPos = new Vector3(-100, 100, -100);
		PointLight light = new PointLight(new Color(255, 255, 255), lightPos);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));
		plane.diffuseColor = new Color(148, 255, 112);
		plane.reflective = true;

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(plane);
		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));

		ArrayList<Sphere> spheres = new ArrayList<Sphere>();
		int nbSpheres = 1000;
		Random rg = new Random();
		int xDim = 50;
		int yDim = 25;
		int zDim = 50;
		float radiusMax = 1f;
		for (int j = 0; j < nbSpheres; j++) {
			float x = rg.nextFloat() * xDim;
			float y = rg.nextFloat() * yDim;
			float z = rg.nextFloat() * zDim;
			float radius = rg.nextFloat() * radiusMax;

			Sphere s = new Sphere(new Vector3(x, y, z), radius);
			s.diffuseColor = new Color(rg.nextFloat(), rg.nextFloat(), rg.nextFloat());
			spheres.add(s);
		}

		/**
		 * Grid
		 */
		CompactGrid grid = new CompactGrid();
		for (IWorldObject o : spheres) {
			grid.addObject(o);
		}
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		scene.worldObjects.add(grid);
		scene.render(panel, camera, "DATA/CompactGrid.png");

		/**
		 * Tree
		 */
		BVHNodeTree tree = new BVHNodeTree();
		for (IWorldObject o : spheres) {
			tree.addObject(o);
		}
		try {
			tree.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		scene.worldObjects.add(tree);
		scene.render(panel, camera, "DATA/BVHTree.png");
		
		/**
		 * Barebones
		 */
		for (IWorldObject o : spheres) {
			scene.worldObjects.add(o);
		}
		scene.render(panel, camera, "DATA/Barebones.png");

	}

	
	public void basics() {
		
		/**
		 * Panel
		 */
		int size = 500;
		MyDimension dim = new MyDimension(size, size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Basics");

		/**
		 * Camera
		 */
		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-20, 3.5f, 0));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.0f);
		Matrix world = t.multiply(rY.multiply(rZ));
		camera.setWorldMatrix(world);

		/**
		 * Shaders
		 */
		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		/**
		 * Lights
		 */
		PointLight light = new PointLight(new Color(200,200,200), new Vector3(-20,100,70));		
		PointLight light02 = new PointLight(new Color(50,50,50), new Vector3(10,100,-70));	
		
		/**
		 * WorldObjects
		 */
		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.diffuseColor = new Color(200,200,200);
		
		Sphere sphere = new Sphere(new Vector3(2,1.5f,-2), 1);
		sphere.diffuseColor = new Color(200,50,50);
		sphere.specularIntensity = 16;
		
		Sphere sphere02 = new Sphere(new Vector3(2,3.5f,1f), 1.5f);
		sphere02.diffuseColor = new Color(50,200,50);
		sphere02.specularIntensity = 64;
		
		/**
		 * CompactGrid
		 */
		CompactGrid grid = new CompactGrid();
		grid.addObject(sphere);
		grid.addObject(sphere02);
		
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/**
		 * Scene setup
		 */
		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
//		scene.setAntialiasing(4);

		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.cameras.add(camera);
		scene.lights.add(light);	
		scene.lights.add(light02);
		
		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);
		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);		
	}

}
