package test;

import java.awt.Color;
import java.awt.Dimension;

import lights.PointLight;
import parser.ImageLoader;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;

public class TestRefraction {

	public void testBasics() {
		MyDimension dim = new MyDimension(750, 750);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("RefractionTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(150);
		Matrix t = Matrix.translation(new Vector3(-20, 5, 0));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(170, 170, 140), new Vector3(-5, 15, -5));

		TriangleMesh plane = MeshLoader.load("Data/OBJ/planeMedium");
		plane.setDiffuseMap(ImageLoader.loadImage("Data/Resources/colorCheckers.jpg"));
		plane.selfIllumination = 2;

		Sphere sphere = new Sphere(new Vector3(2, 3, 0), 1);
		sphere.diffuseColor = new Color(255, 255, 255);
		sphere.refractionIndex = 1.7f;
		sphere.specularColor = new Color(100, 100, 100);
		sphere.refractive = true;

		Sphere sphere02 = new Sphere(new Vector3(15, 1.75f, 2), 1);
		sphere02.diffuseColor = new Color(100, 255, 50);
		sphere02.specularIntensity = 128;
		sphere02.specularColor = new Color(5, 10, 5);
		sphere02.reflective = true;
		sphere02.selfIllumination = 1.5f;

		Sphere sphere03 = new Sphere(new Vector3(10, 1.5f, -2), 1.5f);
		sphere03.diffuseColor = new Color(255, 100, 100);
		sphere03.specularIntensity = 128;
		sphere03.specularColor = new Color(255, 75, 25);
		sphere03.reflective = true;

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.setAntialiasing(2);

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(sphere03);
		scene.worldObjects.add(sphere02);
		scene.worldObjects.add(sphere);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}

	public void testTriangleMesh() {
		MyDimension dim = new MyDimension(750, 750);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("RefractionTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(150);
		Matrix t = Matrix.translation(new Vector3(-20, 5, 0));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(170, 170, 140), new Vector3(-5, 15, -5));

		TriangleMesh plane = MeshLoader.load("Data/OBJ/planeMedium");
		plane.setDiffuseMap(ImageLoader.loadImage("Data/Resources/colorCheckers.jpg"));
		plane.selfIllumination = 2;

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot");
		mesh.setWorldMatrix(Matrix.translation(new Vector3(5, 0, 0)).multiply(Matrix.rotationY((float) -Math.PI * 0.75f)));
		mesh.refractionIndex = 1.8f;
		mesh.specularColor = new Color(100, 100, 100);
		mesh.refractive = true;

		Sphere sphere02 = new Sphere(new Vector3(15, 1f, 3), 1);
		sphere02.diffuseColor = new Color(100, 255, 50);
		sphere02.specularIntensity = 128;
		sphere02.specularColor = new Color(5, 10, 5);
		sphere02.reflective = true;
		sphere02.selfIllumination = 1.5f;

		Sphere sphere03 = new Sphere(new Vector3(10, 1.5f, -2), 1.5f);
		sphere03.diffuseColor = new Color(255, 100, 100);
		sphere03.specularIntensity = 128;
		sphere03.specularColor = new Color(255, 75, 25);
		sphere03.reflective = true;

		CompactGrid grid = new CompactGrid();
		grid.addObject(sphere03);
		grid.addObject(sphere02);
		grid.addObject(mesh);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.setAntialiasing(2);

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}

	public void test01() {
		int size = 70;
		MyDimension dim = new MyDimension(16 * size, 9 * size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("RefractionTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(80);
		Matrix t = Matrix.translation(new Vector3(-20, 11f, 30));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0.25f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.05f);
		Matrix world = t.multiply(rY.multiply(rZ));
		camera.setWorldMatrix(world);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/planeMedium");
		plane.setDiffuseMap(ImageLoader.loadImage("Data/OBJ/TestModel001/maps/diffuseMap.png"));
		plane.setNormalMap(ImageLoader.loadImage("Data/OBJ/TestModel001/maps/normalMap.png"));
		plane.selfIllumination = 2;

		TriangleMesh box01 = MeshLoader.load("Data/OBJ/thinBox");
		box01.setWorldMatrix(Matrix.translation(new Vector3(5, 0, 0)));
		box01.diffuseColor = new Color(100, 255, 200);
		box01.refractionIndex = 1.8f;
		box01.specularColor = new Color(100, 100, 100);
		box01.refractive = true;

		TriangleMesh box02 = MeshLoader.load("Data/OBJ/thinBox");
		box02.setWorldMatrix(Matrix.translation(new Vector3(9f, 0, 2)).multiply(Matrix.rotationY((float) -Math.PI * 0.25f)));
		box02.diffuseColor = new Color(100, 255, 200);
		box02.refractionIndex = 1.8f;
		box02.specularColor = new Color(100, 100, 100);
		box02.refractive = true;

		TriangleMesh box03 = MeshLoader.load("Data/OBJ/thinBox");
		box03.setWorldMatrix(Matrix.translation(new Vector3(12f, 0, 5)).multiply(Matrix.rotationY((float) -Math.PI * 0.5f)));
		box03.diffuseColor = new Color(100, 255, 200);
		box03.refractionIndex = 1.8f;
		box03.specularColor = new Color(100, 100, 100);
		box03.refractive = true;

		Sphere sphere01 = new Sphere(new Vector3(16, 1.2f, 2), 1.2f);
		sphere01.diffuseColor = new Color(100, 255, 100);
		sphere01.specularIntensity = 128;
		sphere01.specularColor = new Color(100, 100, 255);
		sphere01.reflective = true;

		Sphere sphere02 = new Sphere(new Vector3(0, 1f, 3), 1);
		sphere02.diffuseColor = new Color(100, 255, 50);
		sphere02.specularIntensity = 128;
		sphere02.specularColor = new Color(5, 10, 5);
		sphere02.reflective = true;
		sphere02.selfIllumination = 1.5f;

		Sphere sphere03 = new Sphere(new Vector3(10, 1.5f, -2), 1.5f);
		sphere03.diffuseColor = new Color(255, 100, 100);
		sphere03.specularIntensity = 128;
		sphere03.specularColor = new Color(255, 75, 25);
		sphere03.reflective = true;

		CompactGrid grid = new CompactGrid();
		grid.addObject(sphere03);
		grid.addObject(sphere02);
		grid.addObject(sphere01);
		grid.addObject(box01);
		grid.addObject(box02);
		grid.addObject(box03);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		PointLight light = new PointLight(new Color(170, 170, 140), new Vector3(-5, 15, -5));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.setAntialiasing(4);

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}

}
