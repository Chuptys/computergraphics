package test;

import java.awt.Color;
import java.awt.Dimension;

import lights.PointLight;
import parser.ImageLoader;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector2;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;

public class TestTexturing {
	
	public void testTextureSphere()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Spherical TextureMapping");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(25);
		Matrix t = Matrix.translation(new Vector3(-10,2.5f,0));
		Matrix world = t;
		camera.setWorldMatrix(world);
		
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(255,255,255), new Vector3(-10,10,10));
				
		Sphere sphere = new Sphere(new Vector3(2,2,0), 2);
		sphere.setDiffuseMap(ImageLoader.loadImage("Data/Resources/worldmap.jpg"));
		sphere.specularColor = new Color(50,50,100);

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(sphere);
		scene.setAntialiasing(4);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testTextureBasicTriangleMesh()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("TriangleMesh TextureMapping");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(25);
		Matrix t = Matrix.translation(new Vector3(-50,5f,0));
		Matrix world = t;
		camera.setWorldMatrix(world);
		
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(255,255,255), new Vector3(0,50,0));
				
		TriangleMesh texturedPlane = MeshLoader.load("Data/OBJ/TestModel001/RockPlane");
		texturedPlane.setWorldMatrix(Matrix.rotationZ((float)Math.PI*0.25f));
		texturedPlane.specularColor = new Color(50,50,50);
		CompactGrid grid = new CompactGrid();
		grid.addObject(texturedPlane);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.setAntialiasing(4);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testSphereBump()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Spherical BumpMapping");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(25);
		Matrix t = Matrix.translation(new Vector3(-10,0,0));
		Matrix world = t;
		camera.setWorldMatrix(world);
		
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(255,255,255), new Vector3(-10,2,10));
				
		Sphere sphere = new Sphere(new Vector3(2,0,0), 2);
		sphere.setDiffuseMap(ImageLoader.loadImage("Data/Resources/worldmap.jpg"));
		sphere.setBumpMap(ImageLoader.loadImage("Data/Resources/worldmapNormal.png"));

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(sphere);
		scene.setAntialiasing(4);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testBasicBump()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("TriangleMesh TextureMapping");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(25);
		Matrix t = Matrix.translation(new Vector3(-50,3f,0));
		Matrix world = t;
		camera.setWorldMatrix(world);
		
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(255,255,255), new Vector3(0,10,15));
				
		TriangleMesh texturedPlane = MeshLoader.load("Data/OBJ/TestModel001/RockPlane");
		texturedPlane.setWorldMatrix(Matrix.rotationY((float)Math.PI*0.25f).multiply(Matrix.rotationZ((float)Math.PI*0.25f)));
		texturedPlane.hasDiffuseMap = false;
		texturedPlane.setNormalMap(ImageLoader.loadImage("Data/OBJ/TestModel001/maps/normalMap.png"));		
		
		Sphere s = new Sphere(new Vector3(0,8,15), 2);
		
		CompactGrid grid = new CompactGrid();
		grid.addObject(texturedPlane);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.setAntialiasing(4);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testDiffuseBump()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("TriangleMesh TextureMapping");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(75);
		Matrix t = Matrix.translation(new Vector3(-50,1f,0));
		Matrix world = t;
		camera.setWorldMatrix(world);
		
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		Vector3 lightPos = new Vector3(0,5,0);
		PointLight light = new PointLight(new Color(255,255,255), lightPos);
		PointLight light02 = new PointLight(new Color(150,150,150), lightPos);
		Vector3 lightDir = new Vector3(1,0,-1);
		lightDir.normalize();
//		DirectionalLight light = new DirectionalLight(lightDir, new Color(255,255,255));
				
		TriangleMesh texturedPlane = MeshLoader.load("Data/OBJ/TestModel003/testPlane");
		texturedPlane.setWorldMatrix((Matrix.rotationZ((float)Math.PI*0.25f)));		
		texturedPlane.setDiffuseMap(ImageLoader.loadImage("Data/OBJ/TestModel003/maps/diffuseMap.png"));
		texturedPlane.setNormalMap(ImageLoader.loadImage("Data/OBJ/TestModel003/maps/normalMap.png"));		
//		texturedPlane.hasBumpMap = false;
//		texturedPlane.hasDiffuseMap = false;
		
		Sphere s = new Sphere(lightPos, 0.5f);
		s.castsShadow = false;
		
		CompactGrid grid = new CompactGrid();
		grid.addObject(texturedPlane);		
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scene scene = new Scene();
		
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.lights.add(light02);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
//		scene.worldObjects.add(s);
		scene.setAntialiasing(4);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testRenderTriangleMesh()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Spherical TextureMapping");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(25);
		Matrix t = Matrix.translation(new Vector3(-100,35f,0));
		Matrix world = t;
		camera.setWorldMatrix(world);
		
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(255,255,255), new Vector3(-50,50,50));
				
		TriangleMesh mesh = MeshLoader.load("Data/OBJ/TestModel002/CGTest_Normal");
		mesh.setWorldMatrix(Matrix.rotationY((float)-Math.PI*0.5f));
		
		CompactGrid grid = new CompactGrid();
		grid.addObject(mesh);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
//		scene.setAntialiasing(4);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}

}
