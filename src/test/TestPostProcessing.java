package test;

import java.awt.image.BufferedImage;

import parser.ImageLoader;
import postprocessing.GodRayCalculator;
import postprocessing.ImageSampler;
import structs.MyDimension;
import structs.Vector2;
import utils.RenderPanel;

public class TestPostProcessing {

	public void testGodRays() {
		BufferedImage img = null;
		// img = ImageIO.read(new File("Data/testImages/Rodwen002.png"));
		// img = ImageIO.read(new
		// File("Data/testImages/NeithPosed00504_cropped.png"));
		// img = ImageIO.read(new
		// File("Data/testImages/NeithPosed004_edited01.png"));
		img = ImageLoader.loadImage("Data/testImages/stonehenge005.png");

		ImageSampler sampler = new ImageSampler(img);
		RenderPanel panel = new RenderPanel(new MyDimension(sampler.getWidth(), sampler.getHeight()));
		panel.setTitle("Godray test");

		GodRayCalculator godrayCalc = new GodRayCalculator();
		// godrayCalc.processImage(sampler, new Vector2(175, 300), panel);
		// godrayCalc.processImage(sampler, new Vector2(670,350), panel);
		// godrayCalc.processImage(sampler, new Vector2(975,130), panel);
		godrayCalc.processImage(sampler, new Vector2(400, 200), panel);
	}

	public void stonehenge() {

		GodRayCalculator godrayCalc = new GodRayCalculator();
		godrayCalc.Density = 0.75f;
		godrayCalc.NUM_SAMPLES = 175;
		godrayCalc.Weight = 0.05f;
		godrayCalc.Decay = 0.989f;
		godrayCalc.exposure = 1;
		
		BufferedImage img = ImageLoader.loadImage("Data/animation/stonehenge0001.png");
		ImageSampler sampler = new ImageSampler(img);
		RenderPanel panel = new RenderPanel(new MyDimension(sampler.getWidth(), sampler.getHeight()));

		for (int i = 1; i <= 193; i++) {
			StringBuilder nb = new StringBuilder("0000");
			String indexStr = String.valueOf(i);
			for (int j = 0; j < indexStr.length(); j++) {
				nb.setCharAt(nb.length() - indexStr.length() + j, indexStr.charAt(j));
			}
			img = ImageLoader.loadImage("Data/animation/stonehenge" + nb + ".png");

			sampler = new ImageSampler(img);
			godrayCalc.path = "Data/postprocessing/stonehenge" + nb + ".png";
			godrayCalc.processImage(sampler, new Vector2(634, 372), panel);
		}
	}
}
