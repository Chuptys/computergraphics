package test;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Random;

import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.Vector2;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.BVHNodeTree;

public class TestBVH {

	public void testRenderObjectsInGrid() {
		Sphere s01 = new Sphere(new Vector3(-1, -1, -1), 0.5f);
		Sphere s02 = new Sphere(new Vector3(1, 1, 1), 0.5f);
		Sphere s03 = new Sphere(new Vector3(1, -1, -1), 0.5f);
		Sphere s04 = new Sphere(new Vector3(-1, 1, 1), 0.5f);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));

		BVHNodeTree bvhTree = new BVHNodeTree();
		bvhTree.addObject(s01);
		bvhTree.addObject(s02);
		bvhTree.addObject(s03);
		bvhTree.addObject(s04);
		bvhTree.initialize();

		MyDimension dim = new MyDimension(800, 800);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("BVHNodes initial renderTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-15, 0, -15));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		AmbientShader ambient = new AmbientShader(new Color(10, 10, 10));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(200, 200, 200), new Vector3(-10, 5, 0));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		// scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		// scene.worldObjects.add(s01);
		// scene.worldObjects.add(s02);
		// scene.worldObjects.add(s03);
		// scene.worldObjects.add(s04);
		scene.worldObjects.add(plane);
		scene.worldObjects.add(bvhTree);

		scene.render(panel, camera);
	}

	public void stressTest() {
		BVHNodeTree bvhTree = new BVHNodeTree();

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));
		plane.diffuseColor = new Color(148, 255, 112);
		plane.reflective = true;

		ArrayList<Sphere> spheres = new ArrayList<Sphere>();

		int nbSpheres = 100000;
		Random rg = new Random();
		int xDim = 50;
		int yDim = 25;
		int zDim = 50;
		float radiusMax = 0.25f;
		for (int i = 0; i < nbSpheres; i++) {
			float x = rg.nextFloat() * xDim;
			float y = rg.nextFloat() * yDim;
			float z = rg.nextFloat() * zDim;
			float radius = rg.nextFloat() * radiusMax;

			Sphere s = new Sphere(new Vector3(x, y, z), radius);
			s.diffuseColor = new Color(rg.nextFloat(), rg.nextFloat(), rg.nextFloat());
			// s.reflective = true;
			spheres.add(s);
			bvhTree.addObject(s);
		}

		bvhTree.initialize();

		MyDimension dim = new MyDimension(1000, 600);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("BVHNodeTree stressTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-75, 10, -75));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light01 = new PointLight(new Color(255, 255, 255), new Vector3(-100, 50, 0));
		PointLight light02 = new PointLight(new Color(255, 255, 255), new Vector3(-100, 50, -100));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;

		scene.cameras.add(camera);
		scene.lights.add(light01);
		scene.lights.add(light02);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		scene.worldObjects.add(plane);
		scene.worldObjects.add(bvhTree);

		// for(Sphere sp : spheres)
		// {
		// scene.worldObjects.add(sp);
		// }

		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));
		scene.render(panel, camera);
	}

	public void testAddTriangleMesh() {
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("CompactGrid TriangleMesh");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20, 20));
		camera.setViewplaneDistance(250);
		Matrix t = Matrix.translation(new Vector3(0, 10, 50));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0.5f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.05f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(200, 200, 200), new Vector3(15, 25, 15));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot");
		mesh.setWorldMatrix(Matrix.rotationY((float) -Math.PI * 0.25f));

		BVHNodeTree bvhTree = new BVHNodeTree();
		bvhTree.addObject(mesh);
		bvhTree.initialize();

		Scene scene = new Scene();
		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		scene.worldObjects.add(bvhTree);
		// scene.worldObjects.add(mesh);

		scene.render(panel, camera);
	}

}
