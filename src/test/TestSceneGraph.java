package test;

import java.awt.Color;
import java.util.ArrayList;

import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import scenegraph.SceneGraphNode;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;
import animation.AnimatedSceneGraphNode;
import animation.Animation;
import animation.IAnimatable;

public class TestSceneGraph {

	public void testBasics() {
		
		int size = 80;

		MyDimension dim = new MyDimension(16*size, 9*size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Scenegraph");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(50);
		Matrix t = Matrix.translation(new Vector3(-150, -5, 0));
		camera.setWorldMatrix(t);

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		
		ArrayList<SceneGraphNode> nodes = new ArrayList<SceneGraphNode>();
		ArrayList<TriangleMesh> meshes = new ArrayList<TriangleMesh>();
		ArrayList<IAnimatable> animatables = new ArrayList<IAnimatable>();

		String hangerPath = "Data/OBJ/SceneGraph/hanger";
		String teapotPath = "Data/OBJ/teapot";
		TriangleMesh rootMesh01 = MeshLoader.load(hangerPath);
		TriangleMesh rootMesh02 = MeshLoader.load(teapotPath);
		meshes.add(rootMesh01);
		meshes.add(rootMesh02);
		
		Vector3 pos = new Vector3(0, 10, 0);
		SceneGraphNode root = new SceneGraphNode(Matrix.translation(pos));
		root.addWorldObject(rootMesh01);
		root.addWorldObject(rootMesh02);
		AnimatedSceneGraphNode rootAnim = new AnimatedSceneGraphNode(root);
		rootAnim.addPosition(pos, 0);
		rootAnim.addRotationY((float)Math.PI*2,  4000);
		animatables.add(rootAnim);
		nodes.add(root);

		float yOffset = -5f;
		float zOffset = 9.7f;
		int nbLevels = 7;

		SceneGraphNode prevNode = root;
		for (int i = 1; i <= nbLevels; i++) {
			TriangleMesh teapot = MeshLoader.load(teapotPath);
			meshes.add(teapot);
			Vector3 leftPos = new Vector3(0, yOffset, -zOffset);
			SceneGraphNode leftNode = new SceneGraphNode(Matrix.translation(leftPos));
			leftNode.addWorldObject(teapot);
			
			AnimatedSceneGraphNode teapotAnim = new AnimatedSceneGraphNode(leftNode);
			teapotAnim.addPosition(leftPos, 0);
			teapotAnim.addRotationY((float)Math.PI*2,  4000);
			animatables.add(teapotAnim);
			
			nodes.add(leftNode);

			
			
			TriangleMesh hanger = null;
			if (i == nbLevels) {
				hanger = MeshLoader.load(teapotPath);
			} else {
				hanger = MeshLoader.load(hangerPath);
			}
			meshes.add(hanger);
			Vector3 rightPos = new Vector3(0, yOffset, zOffset);
			SceneGraphNode rightNode = new SceneGraphNode(Matrix.translation(rightPos));
			rightNode.addWorldObject(hanger);
			
			AnimatedSceneGraphNode hangerAnim = new AnimatedSceneGraphNode(rightNode);
			hangerAnim.addPosition(rightPos, 0);
			hangerAnim.addRotationY((float)Math.PI*2,  4000);
			animatables.add(hangerAnim);
			
			nodes.add(rightNode);

			prevNode.addChild(leftNode);
			prevNode.addChild(rightNode);

			prevNode = rightNode;
		}
		
		for(IAnimatable anim : animatables)
		{
			anim.updateAtTime(0);
		}

		CompactGrid grid = new CompactGrid();

		for (TriangleMesh m : meshes) {
			grid.addObject(m);
		}

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		
		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0,-27,0)));
		plane.diffuseColor = new Color(150,150,150);
		
		PointLight light = new PointLight(new Color(255, 75, 75), new Vector3(-30, 30, 30));
		PointLight light02 = new PointLight(new Color(75, 255, 75), new Vector3(30, 30, -30));
		PointLight light03 = new PointLight(new Color(75, 75, 255), new Vector3(-30, 30, -30));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.setAntialiasing(4);

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.lights.add(light02);
		scene.lights.add(light03);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		Animation anim = new Animation(scene, camera, panel, 24, "SceneGraphAnimation");
		for(IAnimatable a : animatables)
			anim.addAnimatable(a);
		
		anim.addCompactGrid(grid);
		
		for(SceneGraphNode n : nodes)
			anim.addSceneGraphNode(n);
		
		anim.render(0, 4000);
//		scene.render(panel, camera);
	}
}
