package test;

import java.awt.Color;

import lights.DirectionalLight;
import parser.ImageLoader;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.Vector3;
import utils.ImageBackGround;
import utils.RenderPanel;
import utils.Scene;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;

public class TestEnvironmentMapping {

	public void testBasics() {
		int size = 10;
		MyDimension dim = new MyDimension(75 * size, 50 * size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("EnvironmentMapping");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix world = Matrix.translation(new Vector3(20, 2, 20)).multiply(Matrix.rotationY((float) Math.PI * 0.75f));
		camera.setWorldMatrix(world);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightdir = new Vector3(1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(200, 200, 200));

		TriangleMesh teapot = MeshLoader.load("Data/OBJ/teapot");
		teapot.setWorldMatrix(Matrix.translation(new Vector3(-1, 1, 0)));
		teapot.reflective = true;
		teapot.specularColor = new Color(150, 150, 150);
		teapot.specularIntensity = 128;

		Sphere sphere = new Sphere(new Vector3(3, 1, 0), 1.5f);
		sphere.specularIntensity = 128;
		sphere.specularColor = new Color(150, 150, 150);
		sphere.reflective = true;

		CompactGrid grid = new CompactGrid();
		grid.addObject(sphere);
		grid.addObject(teapot);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.setAntialiasing(4);

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);

		scene.backGround = new ImageBackGround(ImageLoader.loadImage("Data/Resources/PanoLake.jpg"));

		scene.render(panel, camera);

	}
}
