package test;

import java.awt.Color;
import java.awt.Dimension;

import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import raytracing.Ray;
import raytracing.RaycastResult;
import structs.Matrix;
import structs.MyDimension;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.Triangle;
import worldobjects.TriangleMesh;

public class TestRaytracer {

	public void testHalfCube() {

		MyDimension dim = new MyDimension(350, 350);

		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("RaytraceNoVisibilityCheck");

		Vector3 v1 = new Vector3(10, 0, -10);
		Vector3 v2 = new Vector3(10, 0, 10);
		Vector3 v3 = new Vector3(30, 0, -10);
		Vector3 v4 = new Vector3(30, 0, 10);
		Vector3 v5 = new Vector3(10, 20, -10);
		Vector3 v6 = new Vector3(10, 20, 10);
		Vector3 v7 = new Vector3(30, 20, -10);
		Vector3 v8 = new Vector3(30, 20, 10);

		Triangle t1 = new Triangle(v1, v2, v5);
		Triangle t2 = new Triangle(v5, v2, v6);
		Triangle t3 = new Triangle(v5, v8, v7);
		Triangle t4 = new Triangle(v5, v6, v8);
		Triangle t5 = new Triangle(v2, v8, v6);
		Triangle t6 = new Triangle(v2, v4, v8);

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(25);
		Matrix translation = Matrix.translation(new Vector3(-10, 40, 25));
		Matrix rotZ = Matrix.rotationZ((float) -Math.PI * 0.18f);
		Matrix rotY = Matrix.rotationY((float) Math.PI * 0.25f);
		Matrix temp = translation.multiply(rotY);
		Matrix world = temp.multiply(rotZ);
		camera.setWorldMatrix(world);

		Ray ray = new Ray();
		RaycastResult result = new RaycastResult();

		panel.clear();
		for (int col = 0; col < dim.width; col++) {
			for (int row = 0; row < dim.height; row++) {
				ray = camera.getRay(row, col)[0];
				if (t1.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, 1, 0, 0);
				else if (t2.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, 1, 1, 0);
				else if (t3.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, 0, 1, 0);
				else if (t4.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, 0, 1, 1);
				else if (t5.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, 1, 0, 1);
				else if (t6.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, 0, 0, 1);
			}
		}
		panel.redraw();
	}

	public void testSphere() {
		MyDimension dim = new MyDimension(350, 350);

		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("RaytraceSphere");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(25);
		Matrix translation = Matrix.translation(new Vector3(-10, 10, -10));
		Matrix rotZ = Matrix.rotationZ((float) -Math.PI * 0.05f);
		Matrix rotY = Matrix.rotationY((float) -Math.PI * 0.1f);
		Matrix temp = translation.multiply(rotY);
		Matrix world = temp.multiply(rotZ);
		camera.setWorldMatrix(world);

		Ray ray = new Ray();
		RaycastResult result = new RaycastResult();

		Sphere sphere = new Sphere(new Vector3(10, 0, -5), 5);
		Sphere sphere02 = new Sphere(new Vector3(20, 10, 10), 5);

		panel.clear();
		for (int col = 0; col < dim.width; col++) {
			for (int row = 0; row < dim.height; row++) {
				ray = camera.getRay(row, col)[0];
				if (sphere.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, Color.red);
				else if (sphere02.isHit(ray, 0, Integer.MAX_VALUE, result))
					panel.drawPixel(row, col, Color.blue);
			}
		}
		panel.redraw();
	}

	public void testImportedObj() {
		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot.obj");

		MyDimension dim = new MyDimension(350, 350);

		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("RaytraceObj");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(25);

		Matrix transl = Matrix.translation(new Vector3(-1, 2, 5));
		Matrix rotY = Matrix.rotationY((float) Math.PI * 0.45f);
		Matrix rotZ = Matrix.rotationZ((float) -Math.PI * 0.05f);
		Matrix cam = transl.multiply(rotZ).multiply(rotY);
		camera.setWorldMatrix(cam);

		Ray ray = new Ray();
		RaycastResult result = new RaycastResult();

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.worldObjects.add(mesh);

		scene.backGround = new SimpleBackGround(Color.cyan);

		scene.render(panel, camera);
	}

	public void testScene() {
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("RaytraceScene");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(25);
		Matrix translation = Matrix.translation(new Vector3(10, 2, 20));
		Matrix rotZ = Matrix.rotationZ((float) -Math.PI * 0.1f);
		Matrix rotY = Matrix.rotationY((float) Math.PI * 0.6f);
		Matrix cam = translation.multiply(rotY).multiply(rotZ);
		camera.setWorldMatrix(cam);

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot.obj");
		Sphere sphere01 = new Sphere(new Vector3(10, -8, -5), 2);
		sphere01.diffuseColor = Color.red;
		Sphere sphere02 = new Sphere(new Vector3(-6, 0, 0), 2);
		sphere02.diffuseColor = Color.blue;

		Triangle triangle = new Triangle(new Vector3(0, -15, 0), new Vector3(10, -15, 0), new Vector3(10, -15, 10));
		triangle.diffuseColor = Color.green;

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.worldObjects.add(sphere01);
		scene.worldObjects.add(mesh);
		scene.worldObjects.add(sphere02);
		scene.worldObjects.add(triangle);

		scene.backGround = new SimpleBackGround(Color.cyan);

		scene.render(panel, camera);
	}
}
