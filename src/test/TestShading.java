package test;

import java.awt.Color;
import java.awt.Dimension;

import lights.DirectionalLight;
import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector2;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;

public class TestShading {

	public void testLambertianShading()
	{
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("DirectionalLight LabertianShading");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(25);
		
		LambertianShader shader = new LambertianShader();
		Vector3 lightdir = new Vector3(1,-0.5f, 0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(255,255,255));

		Sphere sphere = new Sphere(new Vector3(20, 0, 0), 5);
		sphere.diffuseColor = new Color(100,200,50);

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(shader);
		scene.worldObjects.add(sphere);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testLambertianShadingObj()
	{
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("DirectionalLight LabertianShading");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(35);
		Matrix t = Matrix.translation(new Vector3(10,2,10));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.75f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);
		
		LambertianShader shader = new LambertianShader();
		Vector3 lightdir = new Vector3(-1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(255,0,0));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot");

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(shader);
		scene.worldObjects.add(mesh);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testAmbientLight()
	{
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("DirectionalLight LabertianShading + Ambient");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(35);
		Matrix t = Matrix.translation(new Vector3(10,2,10));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.75f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);
		
		LambertianShader lambertian = new LambertianShader();
		AmbientShader ambient = new AmbientShader(new Color(100,100,100));
		
		Vector3 lightdir = new Vector3(-1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(255,0,0));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot");

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambertian);
		scene.shaders.add(ambient);
		scene.worldObjects.add(mesh);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testPhongShading()
	{
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("DirectionalLight PhongShading");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(25);
//		Matrix t = Matrix.translation(new Vector3(10,2,10));
//		Matrix rY = Matrix.rotationY((float)Math.PI*0.75f);
		Matrix t = Matrix.translation(new Vector3(0,2,10));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.5f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);
		
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		Vector3 lightdir = new Vector3(-1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(200,200,200));

		TriangleMesh teapot = MeshLoader.load("Data/OBJ/teapot");		
		teapot.setWorldMatrix(Matrix.rotationY((float) -Math.PI*0.25f));
		
		Sphere sphere = new Sphere(new Vector3(0,1,0), 2);
		sphere.diffuseColor = new Color(255,255,255);
		sphere.specularIntensity = 64;
		sphere.specularColor = new Color(75,100,200);

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(teapot);
		//scene.worldObjects.add(sphere);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testShadows()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("DirectionalLight Shadows");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(200);
		Matrix t = Matrix.translation(new Vector3(-19,3.5f,20));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.25f);
		Matrix rZ = Matrix.rotationZ((float)-Math.PI*0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);
		
		AmbientShader ambient = new AmbientShader(new Color(20,20,20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		Vector3 lightdir = new Vector3(-0.5f, -1f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(150,150,150));
		
		Vector3 lightdir02 = new Vector3(0.5f, -1.5f,-0.5f);
		lightdir02.normalize();
		DirectionalLight light02 = new DirectionalLight(lightdir02, new Color(100,100,50));

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		
		Sphere sphere = new Sphere(new Vector3(2,1.5f,-2), 1);
		sphere.diffuseColor = new Color(255,255,255);
		sphere.specularIntensity = 64;
		sphere.specularColor = new Color(75,100,200);
		
		Sphere sphere02 = new Sphere(new Vector3(3,2f,0), 1);
		sphere02.diffuseColor = new Color(100,255,100);
		sphere02.specularIntensity = 64;
		sphere02.specularColor = new Color(75,100,200);

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.lights.add(light02);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(sphere02);
		scene.worldObjects.add(sphere);
		scene.worldObjects.add(plane);
//		scene.setAntialiasing(8);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void testPointLight()
	{
		MyDimension dim = new MyDimension(800, 800);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("PointLight Shadows");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-20,8f,0));
		Matrix rZ = Matrix.rotationZ((float)-Math.PI*0.04f);
		Matrix world = t.multiply(rZ);
		camera.setWorldMatrix(world);
		
		AmbientShader ambient = new AmbientShader(new Color(10,10,10));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(80,0,0), new Vector3(8,2,0));
		PointLight light02 = new PointLight(new Color(0,80,0), new Vector3(16,2,0));
		PointLight light03 = new PointLight(new Color(0,0,80), new Vector3(0,2,0));
		PointLight light04 = new PointLight(new Color(80,0,0), new Vector3(32,2,0));
		PointLight light05 = new PointLight(new Color(0,80,0), new Vector3(24,2,0));
		PointLight light06 = new PointLight(new Color(0,0,80), new Vector3(38,2,0));

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		
		Sphere sphere = new Sphere(new Vector3(5,1,-2), 1);
		sphere.diffuseColor = new Color(255,255,255);
		sphere.specularIntensity = 64;
		sphere.specularColor = new Color(75,100,200);
		
		Sphere sphere03 = new Sphere(new Vector3(15,1,-1.75f), 1);
		sphere.diffuseColor = new Color(255,255,255);
		sphere.specularIntensity = 64;
		sphere.specularColor = new Color(75,100,200);
		
		Sphere sphere04 = new Sphere(new Vector3(25,1,-1.5f), 1);
		sphere.diffuseColor = new Color(255,255,255);
		sphere.specularIntensity = 64;
		sphere.specularColor = new Color(75,100,200);
		
		Sphere sphere02 = new Sphere(new Vector3(5,1,2), 1);
		sphere02.diffuseColor = new Color(100,255,100);
		sphere02.specularIntensity = 64;
		sphere02.specularColor = new Color(75,100,200);
		
		Sphere sphere05 = new Sphere(new Vector3(15,1,1.75f), 1);
		sphere02.diffuseColor = new Color(100,255,100);
		sphere02.specularIntensity = 64;
		sphere02.specularColor = new Color(75,100,200);
		
		Sphere sphere06 = new Sphere(new Vector3(25,1,1.5f), 1);
		sphere02.diffuseColor = new Color(100,255,100);
		sphere02.specularIntensity = 64;
		sphere02.specularColor = new Color(75,100,200);
		
		Sphere sphereBig = new Sphere(new Vector3(50,5,0), 4);
		sphere02.diffuseColor = new Color(100,255,100);
		sphere02.specularIntensity = 64;
		sphere02.specularColor = new Color(75,100,200);

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.lights.add(light02);
		scene.lights.add(light03);
		scene.lights.add(light04);
		scene.lights.add(light05);
		scene.lights.add(light06);
		//scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
				
		scene.worldObjects.add(sphere);
		scene.worldObjects.add(sphere02);
		scene.worldObjects.add(sphere03);
		scene.worldObjects.add(sphere04);
		scene.worldObjects.add(sphere05);
		scene.worldObjects.add(sphere06);
		scene.worldObjects.add(sphereBig);
		scene.worldObjects.add(plane);


		scene.render(panel, camera);
	}
	
	public void testReflection()
	{
		MyDimension dim = new MyDimension(750, 750);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("ReflectionTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(150);
		Matrix t = Matrix.translation(new Vector3(-19,3.5f,20));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.25f);
		Matrix rZ = Matrix.rotationZ((float)-Math.PI*0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);
		
		AmbientShader ambient = new AmbientShader(new Color(20,20,20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(200,200,200), new Vector3(0,5,-5));
		

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.reflective = true;
				
		Sphere sphere = new Sphere(new Vector3(2,1,-2), 1);
		sphere.diffuseColor = new Color(100,100,255);
		sphere.specularIntensity = 64;
		sphere.specularColor = new Color(75,100,200);
		
		Sphere sphere02 = new Sphere(new Vector3(3,2f,0), 1);
		sphere02.diffuseColor = new Color(100,255,100);
		sphere02.specularIntensity = 16;
		sphere02.specularColor = new Color(75,100,200);
		sphere02.reflective = true;
		
		Sphere sphere03 = new Sphere(new Vector3(5,4f,-5), 1.5f);
		sphere03.diffuseColor = new Color(255,100,100);
		sphere03.specularIntensity = 128;
		sphere03.specularColor = new Color(75,100,200);
		sphere03.reflective = true;

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(sphere03);
		scene.worldObjects.add(sphere02);
		scene.worldObjects.add(sphere);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(new Color(20,20,20));

		scene.render(panel, camera);
	}
	
	public void testBug()
	{
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("BugTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20,20));
		camera.setViewplaneDistance(35);
		Matrix t = Matrix.translation(new Vector3(-10,0,0));
		camera.setWorldMatrix(t);
		
		LambertianShader lambert = new LambertianShader();
				
		PointLight pointLight02 = new PointLight(new Color(255,255,255),new Vector3(-5,-3,10));

		TriangleMesh sphereMesh = MeshLoader.load("Data/OBJ/sphereLowRes");
				
		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		
		scene.lights.add(pointLight02);
		
		scene.shaders.add(lambert);
		scene.worldObjects.add(sphereMesh);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void uberTest()
	{
		//MyDimension dim = new MyDimension(500, 850);
		MyDimension dim = new MyDimension(150, 255);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Meet Rodwen");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(80);
		Matrix t = Matrix.translation(new Vector3(-50,6,-50));
		Matrix rY = Matrix.rotationY((float)-Math.PI*0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);
		
		LambertianShader lambertian = new LambertianShader();
		AmbientShader ambient = new AmbientShader(new Color(20,20,20));
		PhongShader phong = new PhongShader();
		

		TriangleMesh robe = MeshLoader.load("Data/OBJ/Robe031");
		robe.diffuseColor = new Color(100,100,100);
		TriangleMesh belt = MeshLoader.load("Data/OBJ/Belt");
		belt.diffuseColor = new Color(50,50,50);
		TriangleMesh body = MeshLoader.load("Data/OBJ/Body03101");		
		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.diffuseColor = new Color(0,60,100);
		plane.reflective = true;

		Sphere sphere = new Sphere(new Vector3(0,7.85f,-2.5f), 1f);
		sphere.diffuseColor = new Color(255,150,0);
		sphere.specularIntensity = 64;
		sphere.reflective = true;
		sphere.castsShadow = false;
		sphere.selfIllumination = 5;
		
		Vector3 lightdir = new Vector3(1, -0.5f, 0.7f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(0,150,255));
		
		PointLight pointLight = new PointLight(new Color(200,150,0), new Vector3(0,7.85f,-2.5f));
		PointLight pointLight02 = new PointLight(new Color(200,150,0), new Vector3(0,7.85f,-2.5f));
		
		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		
		scene.cameras.add(camera);
		
		scene.lights.add(light);
		scene.lights.add(pointLight);
		scene.lights.add(pointLight02);
		
		scene.shaders.add(lambertian);
		scene.shaders.add(ambient);
		scene.shaders.add(phong);
		
		scene.worldObjects.add(robe);
		scene.worldObjects.add(belt);
		scene.worldObjects.add(body);
		scene.worldObjects.add(sphere);
		scene.worldObjects.add(plane);

//		scene.backgroundColor = new Color(170,223,233);

		scene.render(panel, camera);
	}
}
