package test;

import java.awt.Color;
import java.awt.Dimension;

import lights.DirectionalLight;
import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector2;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.BVHNodeTree;
import acceleration.CompactGrid;

public class TestFalseColorImage {
	
	public void basicTest()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("False Color test");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20, 20));
		camera.setViewplaneDistance(50);
		Matrix t = Matrix.translation(new Vector3(0, 4, 15));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0.5f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.05f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(200, 200, 200), new Vector3(15, 25, 15));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot");
		mesh.setWorldMatrix(Matrix.rotationY((float) -Math.PI * 0.25f));

		CompactGrid grid = new CompactGrid();
		grid.addObject(mesh);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);
		scene.shadowsEnabled = true;
		scene.falseColorEnabled = true;
		
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		scene.worldObjects.add(grid);

		scene.render(panel, camera);
	}
	
	public void basicTest02()
	{
		MyDimension dim = new MyDimension(500, 850);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Rodwen falseColor");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(240);
		Matrix t = Matrix.translation(new Vector3(-35, 6, -50));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.3f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);		

		TriangleMesh robe = MeshLoader.load("Data/OBJ/Robe031");
		robe.diffuseColor = new Color(100, 100, 100);
		TriangleMesh belt = MeshLoader.load("Data/OBJ/Belt");
		belt.diffuseColor = new Color(50, 50, 50);
		TriangleMesh body = MeshLoader.load("Data/OBJ/Body03101");
		TriangleMesh hair = MeshLoader.load("Data/OBJ/hair052");
		hair.diffuseColor = new Color(50,50,50);
		TriangleMesh hairJewel = MeshLoader.load("Data/OBJ/hairJewel052");
		hairJewel.diffuseColor = new Color(255,255,255);
		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.diffuseColor = new Color(0, 60, 100);
		plane.reflective = true;
		plane.setWorldMatrix(Matrix.translation(new Vector3(0,-2,0)));

		Sphere sphere = new Sphere(new Vector3(0, 7.85f, -2.5f), 1f);
		sphere.diffuseColor = new Color(255, 150, 0);
		sphere.specularIntensity = 64;
		sphere.reflective = true;
		sphere.castsShadow = false;
		sphere.selfIllumination = 5;
		
		CompactGrid grid = new CompactGrid();
		grid.addObject(robe);
		grid.addObject(sphere);
		grid.addObject(body);
		grid.addObject(belt);
		grid.addObject(hair);
		grid.addObject(hairJewel);
//		grid.addObject(plane);
		
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LambertianShader lambertian = new LambertianShader();
		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		PhongShader phong = new PhongShader();
		
		Vector3 lightdir = new Vector3(1, -0.5f, 0.7f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(0, 150, 255));

		PointLight pointLight = new PointLight(new Color(200, 150, 0), new Vector3(0, 7.85f, -2.5f));
		PointLight pointLight02 = new PointLight(new Color(200, 150, 0), new Vector3(0, 7.85f, -2.5f));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.falseColorEnabled = true;

		scene.cameras.add(camera);

		scene.lights.add(light);
		scene.lights.add(pointLight);
		scene.lights.add(pointLight02);

		scene.shaders.add(lambertian);
		scene.shaders.add(ambient);
		scene.shaders.add(phong);

		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
	
	public void basicTest03()
	{
		MyDimension dim = new MyDimension(500, 850);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Rodwen falseColor");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(240);
		Matrix t = Matrix.translation(new Vector3(-35, 6, -50));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.3f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);		

		TriangleMesh robe = MeshLoader.load("Data/OBJ/Robe031");
		robe.diffuseColor = new Color(100, 100, 100);
		TriangleMesh belt = MeshLoader.load("Data/OBJ/Belt");
		belt.diffuseColor = new Color(50, 50, 50);
		TriangleMesh body = MeshLoader.load("Data/OBJ/Body03101");
		TriangleMesh hair = MeshLoader.load("Data/OBJ/hair052");
		hair.diffuseColor = new Color(50,50,50);
		TriangleMesh hairJewel = MeshLoader.load("Data/OBJ/hairJewel052");
		hairJewel.diffuseColor = new Color(255,255,255);
		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.diffuseColor = new Color(0, 60, 100);
		plane.reflective = true;
		plane.setWorldMatrix(Matrix.translation(new Vector3(0,-2,0)));

		Sphere sphere = new Sphere(new Vector3(0, 7.85f, -2.5f), 1f);
		sphere.diffuseColor = new Color(255, 150, 0);
		sphere.specularIntensity = 64;
		sphere.reflective = true;
		sphere.castsShadow = false;
		sphere.selfIllumination = 5;
		
		BVHNodeTree bvhTree = new BVHNodeTree();
		bvhTree.addObject(robe);
		bvhTree.addObject(sphere);
		bvhTree.addObject(body);
		bvhTree.addObject(belt);
		bvhTree.addObject(hair);
		bvhTree.addObject(hairJewel);
		
		bvhTree.initialize();
		
		LambertianShader lambertian = new LambertianShader();
		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		PhongShader phong = new PhongShader();
		
		Vector3 lightdir = new Vector3(1, -0.5f, 0.7f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(0, 150, 255));

		PointLight pointLight = new PointLight(new Color(200, 150, 0), new Vector3(0, 7.85f, -2.5f));
		PointLight pointLight02 = new PointLight(new Color(200, 150, 0), new Vector3(0, 7.85f, -2.5f));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.falseColorEnabled = true;

		scene.cameras.add(camera);

		scene.lights.add(light);
		scene.lights.add(pointLight);
		scene.lights.add(pointLight02);

		scene.shaders.add(lambertian);
		scene.shaders.add(ambient);
		scene.shaders.add(phong);

		scene.worldObjects.add(bvhTree);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}

}
