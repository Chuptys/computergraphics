package test;

import java.awt.Color;

import lights.PointLight;
import parser.ImageLoader;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;

public class TestGlossyReflection {
	
	public void testBasics()
	{
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Glossy Reflections Test");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(150);
		Matrix t = Matrix.translation(new Vector3(-20, 5, 0));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(170, 170, 140), new Vector3(-5, 15, -5));

		TriangleMesh plane = MeshLoader.load("Data/OBJ/planeMedium");
		plane.setDiffuseMap(ImageLoader.loadImage("Data/Resources/colorCheckers.jpg"));
		plane.selfIllumination = 2;

		
		Sphere sphere02 = new Sphere(new Vector3(15, 1.75f, 2), 1);
		sphere02.diffuseColor = new Color(100, 255, 100);
		sphere02.specularIntensity = 128;
		sphere02.specularColor = new Color(75, 255, 25);
		sphere02.reflective = true;

		Sphere sphere03 = new Sphere(new Vector3(10, 1.5f, -2), 1.5f);
		sphere03.diffuseColor = new Color(255, 75, 75);
		sphere03.specularIntensity = 128;
		sphere03.specularColor = new Color(255, 75, 25);
		sphere03.reflective = true;
		sphere03.glossy = true;
		sphere03.glossAmount = 0.05f;
		
		CompactGrid grid = new CompactGrid();
		grid.addObject(sphere03);
		grid.addObject(sphere02);
		
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.glossQuality = 100;
		scene.setAntialiasing(4);

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}

}
