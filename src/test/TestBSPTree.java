package test;

import java.awt.Color;

import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.BSPTree;

@Deprecated
public class TestBSPTree {
	
	public void testInitialize()
	{
		BSPTree bspTree = new BSPTree();

		Sphere s01 = new Sphere(new Vector3(-1, -1, -1), 0.5f);
		Sphere s02 = new Sphere(new Vector3(1, 1, 1), 0.5f);
		Sphere s03 = new Sphere(new Vector3(1, -1, -1), 0.5f);
		Sphere s04 = new Sphere(new Vector3(-1, 1, 1), 0.5f);

		bspTree.addObject(s01);
		bspTree.addObject(s02);
		bspTree.addObject(s03);
		bspTree.addObject(s04);

		try {
			bspTree.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testRenderObjectsInTree() {
		Sphere s01 = new Sphere(new Vector3(-1, -1, -1), 0.5f);
		Sphere s02 = new Sphere(new Vector3(1, 1, 1), 0.5f);
		Sphere s03 = new Sphere(new Vector3(1, -1, -1), 0.5f);
		Sphere s04 = new Sphere(new Vector3(-1, 1, 1), 0.5f);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));

		BSPTree tree = new BSPTree();
		
		tree.addObject(s01);
		tree.addObject(s02);
		tree.addObject(s03);
		tree.addObject(s04);

		try {
			tree.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyDimension dim = new MyDimension(800, 800);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("BSPTree initial renderTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-15, 0, -15));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		AmbientShader ambient = new AmbientShader(new Color(10, 10, 10));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(200, 200, 200), new Vector3(-10, 5, 0));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		// scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

//		 scene.worldObjects.add(s01);
//		 scene.worldObjects.add(s02);
//		 scene.worldObjects.add(s03);
//		 scene.worldObjects.add(s04);
		scene.worldObjects.add(plane);
		scene.worldObjects.add(tree);

		scene.render(panel, camera);
	}

}
