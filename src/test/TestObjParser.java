package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import parser.ObjParser;
import worldobjects.TriangleMesh;

public class TestObjParser {
	
	public void testParseBasicObj()
	{
		ObjParser parser = new ObjParser();
		File file = new File("Data/OBJ/cube.obj");
		
		FileInputStream fileStream = null;
		try {
			fileStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		parser.parse(fileStream);
		
		TriangleMesh mesh = parser.getParsedObject();
		
		System.out.println("testParseBasicObj completed");
	}

}
