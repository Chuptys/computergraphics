package test;

import java.awt.Color;

import lights.DirectionalLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.TriangleMesh;

public class TestMatrix {
	
	public void testMultiply()
	{
		Matrix t = Matrix.translation(new Vector3(1,2,3));
		Matrix r = Matrix.rotationX((float)Math.PI);
		Matrix s = Matrix.scale(new Vector3(2,2,2));
		
		Matrix x = t.multiply(s);
		
		Matrix m = (t.multiply(r)).multiply(s);
		float[][] data = m.getData();
		
		if(data[0][0] == 2 && data[0][3] == 1 && data[1][1] == -2 && data[1][3] == 2 && data[2][2] == -2 && data[2][3] == 3 && data[3][3] == 1)
			System.out.println("Matrix multiplication SUCCEEDED!");
		else
			System.out.println("Matrix multiplication FAILED!");
		
	}
	
	public void testVectorInteraction()
	{
		Matrix ry = Matrix.rotationY((float)-Math.PI*0.5f);
		Matrix rx = Matrix.rotationX((float)-Math.PI*0.5f);
		
		Matrix rot = rx.multiply(ry);
		
		Vector3 v = new Vector3(1,0,0);
		v.transform(rot);
		
		System.out.println("end");
	}
	
	public void testTranslateObj()
	{
		MyDimension dim = new MyDimension(150, 150);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Test transform triangleMesh");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(50);
		Matrix t = Matrix.translation(new Vector3(10,3,10));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.75f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);		
		
		LambertianShader shader = new LambertianShader();
		PhongShader phong = new PhongShader();
		Vector3 lightdir = new Vector3(-1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(255,0,0));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot.obj");
		Matrix tranf = Matrix.rotationY((float)-Math.PI*0.5f).multiply(Matrix.rotationZ((float)Math.PI*0.02f));
		mesh.setWorldMatrix(tranf);
		
		
		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(shader);
		scene.shaders.add(phong);
		scene.worldObjects.add(mesh);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}

}
