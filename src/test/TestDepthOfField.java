package test;

import java.awt.Color;
import java.util.Random;

import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;

public class TestDepthOfField {

	public void basicTest() {
		MyDimension dim = new MyDimension(750, 400);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Depth of Field test");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(30);
		Matrix t = Matrix.translation(new Vector3(-10, 2f, 1.5f));
		Matrix world = t;
		camera.setWorldMatrix(world);
		camera.setDepthOfField(0.15f, 50);
		camera.setFOV(0.15f);

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(255, 255, 255), new Vector3(0, 5, 5));

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.diffuseColor = new Color(255, 255, 255);
		plane.selfIllumination = 4;

		int nbSpheresPerLine = 30;
		int nbSphereLines = 5;
		float spacing = 2.25f;
		Random rg = new Random();
		CompactGrid grid = new CompactGrid();
		for (int i = 0; i < nbSpheresPerLine; i++) {
			for (int j = 1; j < nbSphereLines; j++) {
				Sphere s = new Sphere(new Vector3(i * spacing, j*0.75f, i * spacing * 0.15f), 0.25f);
				s.diffuseColor = new Color(rg.nextInt(255), rg.nextInt(255), rg.nextInt(255));
				grid.addObject(s);
			}
		}

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
}
