package test;

import java.awt.Color;

import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;

public class TestAntialiasing {

	
	public void basicTest()
	{
		MyDimension dim = new MyDimension(750, 750);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("16x Supersampling Test");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(150);
		Matrix t = Matrix.translation(new Vector3(-19,3.5f,20));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.25f);
		Matrix rZ = Matrix.rotationZ((float)-Math.PI*0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);
		
		AmbientShader ambient = new AmbientShader(new Color(20,20,20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();
		
		PointLight light = new PointLight(new Color(200,200,200), new Vector3(0,5,-5));
		

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.reflective = true;
				
		Sphere sphere = new Sphere(new Vector3(2,1,-2), 1);
		sphere.diffuseColor = new Color(100,100,255);
		sphere.specularIntensity = 64;
		sphere.specularColor = new Color(75,100,200);
		
		Sphere sphere02 = new Sphere(new Vector3(3,2f,0), 1);
		sphere02.diffuseColor = new Color(100,255,100);
		sphere02.specularIntensity = 16;
		sphere02.specularColor = new Color(75,100,200);
		sphere02.reflective = true;
		
		Sphere sphere03 = new Sphere(new Vector3(5,4f,-5), 1.5f);
		sphere03.diffuseColor = new Color(255,100,100);
		sphere03.specularIntensity = 128;
		sphere03.specularColor = new Color(75,100,200);
		sphere03.reflective = true;

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.setAntialiasing(4);
		
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(sphere03);
		scene.worldObjects.add(sphere02);
		scene.worldObjects.add(sphere);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(new Color(20,20,20));

		scene.render(panel, camera);
	}
}
