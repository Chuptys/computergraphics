package test;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Random;

import lights.DirectionalLight;
import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector2;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;

public class TestCompactGrid {

	public void testAddObjectsAndInitialize() {
		CompactGrid grid = new CompactGrid();

		Sphere s01 = new Sphere(new Vector3(-1, -1, -1), 0.5f);
		Sphere s02 = new Sphere(new Vector3(1, 1, 1), 0.5f);
		Sphere s03 = new Sphere(new Vector3(1, -1, -1), 0.5f);
		Sphere s04 = new Sphere(new Vector3(-1, 1, 1), 0.5f);

		grid.addObject(s01);
		grid.addObject(s02);
		grid.addObject(s03);
		grid.addObject(s04);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testShowObjectsAdded() {
		CompactGrid grid = new CompactGrid();

		Sphere s01 = new Sphere(new Vector3(-1, -1, -1), 0.5f);
		s01.diffuseColor = new Color(255, 0, 0);

		Sphere s02 = new Sphere(new Vector3(1, 1, 1), 0.5f);
		s02.diffuseColor = new Color(0, 255, 0);

		Sphere s03 = new Sphere(new Vector3(1, -1, -1), 0.5f);
		s03.diffuseColor = new Color(0, 0, 255);

		Sphere s04 = new Sphere(new Vector3(-1, 1, 1), 0.5f);
		s04.diffuseColor = new Color(255, 255, 255);

		grid.addObject(s01);
		grid.addObject(s02);
		grid.addObject(s03);
		grid.addObject(s04);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArrayList<Sphere> points = grid.getTestSpheres();

		MyDimension dim = new MyDimension(800, 800);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("CompactGrid debug test");
		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-15, 3, -15));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);
		LambertianShader lambert = new LambertianShader();
		PointLight light = new PointLight(new Color(255, 255, 255), new Vector3(-10, 10, -10));
		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);

		for (Sphere s : points) {
			scene.worldObjects.add(s);
		}

		scene.render(panel, camera);
	}

	public void testRenderObjectsInGrid() {
		CompactGrid grid = new CompactGrid();

		Sphere s01 = new Sphere(new Vector3(-1, -1, -1), 0.5f);
		Sphere s02 = new Sphere(new Vector3(1, 1, 1), 0.5f);
		Sphere s03 = new Sphere(new Vector3(1, -1, -1), 0.5f);
		Sphere s04 = new Sphere(new Vector3(-1, 1, 1), 0.5f);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));

		grid.addObject(s01);
		grid.addObject(s02);
		grid.addObject(s03);
		grid.addObject(s04);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyDimension dim = new MyDimension(800, 800);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("CompactGrid initial renderTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-15, 0, -15));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		AmbientShader ambient = new AmbientShader(new Color(10, 10, 10));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(200, 200, 200), new Vector3(-10, 5, 0));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		// scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		// scene.worldObjects.add(s01);
		// scene.worldObjects.add(s02);
		// scene.worldObjects.add(s03);
		// scene.worldObjects.add(s04);
		scene.worldObjects.add(plane);
		scene.worldObjects.add(grid);


		scene.render(panel, camera);
	}

	public void testDisableSomeShadows() {
		CompactGrid grid = new CompactGrid();

		Sphere s01 = new Sphere(new Vector3(-1, -1, -1), 0.5f);
		s01.castsShadow = false;
		Sphere s02 = new Sphere(new Vector3(1, 1, 1), 0.5f);
		Sphere s03 = new Sphere(new Vector3(1, -1, -1), 0.5f);
		Sphere s04 = new Sphere(new Vector3(-1, 1, 1), 0.5f);

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));

		grid.addObject(s01);
		grid.addObject(s02);
		grid.addObject(s03);
		grid.addObject(s04);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyDimension dim = new MyDimension(800, 800);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("CompactGrid Shadows partially disabled test");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-15, 0, -15));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(200, 200, 200), new Vector3(-10, 5, 0));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(plane);
		scene.worldObjects.add(grid);
		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);
		scene.render(panel, camera);

	}

	public void stressTest() {
		CompactGrid grid = new CompactGrid();
		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));
		plane.diffuseColor = new Color(148, 255, 112);
		plane.reflective = true;

		ArrayList<Sphere> spheres = new ArrayList<Sphere>();

		int nbSpheres = 100000;
		Random rg = new Random();
		int xDim = 50;
		int yDim = 25;
		int zDim = 50;
		float radiusMax = 0.25f;
		for (int i = 0; i < nbSpheres; i++) {
			float x = rg.nextFloat() * xDim;
			float y = rg.nextFloat() * yDim;
			float z = rg.nextFloat() * zDim;
			float radius = rg.nextFloat() * radiusMax;

			Sphere s = new Sphere(new Vector3(x, y, z), radius);
			s.diffuseColor = new Color(rg.nextFloat(), rg.nextFloat(), rg.nextFloat());
			// s.reflective = true;
			spheres.add(s);
			grid.addObject(s);
		}

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyDimension dim = new MyDimension(1000, 600);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("CompactGrid stressTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(100);
		Matrix t = Matrix.translation(new Vector3(-75, 10, -75));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light01 = new PointLight(new Color(255, 255, 255), new Vector3(-100, 50, 0));
		PointLight light02 = new PointLight(new Color(255, 255, 255), new Vector3(-100, 50, -100));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;

		scene.cameras.add(camera);
		scene.lights.add(light01);
		scene.lights.add(light02);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		scene.worldObjects.add(plane);
		scene.worldObjects.add(grid);

		// for(Sphere sp : spheres)
		// {
		// scene.worldObjects.add(sp);
		// }

		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));
		scene.render(panel, camera);
	}

	public void testAddTriangleMesh() {
		MyDimension dim = new MyDimension(500, 500);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("CompactGrid TriangleMesh");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20, 20));
		camera.setViewplaneDistance(250);
		Matrix t = Matrix.translation(new Vector3(0, 10, 50));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0.5f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.05f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(200, 200, 200), new Vector3(15, 25, 15));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot");
		mesh.setWorldMatrix(Matrix.rotationY((float) -Math.PI * 0.25f));

		CompactGrid grid = new CompactGrid();
		grid.addObject(mesh);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));
		scene.shadowsEnabled = true;
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);

		scene.worldObjects.add(grid);
		// scene.worldObjects.add(mesh);

		scene.render(panel, camera);
	}

	public void uberTest() {
		MyDimension dim = new MyDimension(500, 850);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Rodwen");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(300);
		Matrix t = Matrix.translation(new Vector3(-50, 6, -50));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.25f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		TriangleMesh robe = MeshLoader.load("Data/OBJ/Robe031");
		robe.diffuseColor = new Color(100, 100, 100);
		TriangleMesh belt = MeshLoader.load("Data/OBJ/Belt");
		belt.diffuseColor = new Color(50, 50, 50);
		TriangleMesh body = MeshLoader.load("Data/OBJ/Body03101");		
		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
		plane.diffuseColor = new Color(0, 60, 100);
		plane.reflective = true;
		plane.setWorldMatrix(Matrix.translation(new Vector3(0, -2, 0)));

		Sphere sphere = new Sphere(new Vector3(0, 7.85f, -2.5f), 1f);
		sphere.diffuseColor = new Color(255, 150, 0);
		sphere.specularIntensity = 64;
		sphere.reflective = true;
		sphere.castsShadow = false;
		sphere.selfIllumination = 5;

		CompactGrid grid = new CompactGrid();
		grid.addObject(robe);
		// grid.addObject(sphere);
		grid.addObject(body);
		grid.addObject(belt);

		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LambertianShader lambertian = new LambertianShader();
		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		PhongShader phong = new PhongShader();

		Vector3 lightdir = new Vector3(1, -0.5f, 0.7f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(0, 150, 255));

		PointLight pointLight = new PointLight(new Color(200, 150, 0), new Vector3(0, 7.85f, -2.5f));
		PointLight pointLight02 = new PointLight(new Color(200, 150, 0), new Vector3(0, 7.85f, -2.5f));

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;

		scene.cameras.add(camera);

		scene.lights.add(light);
		scene.lights.add(pointLight);
		scene.lights.add(pointLight02);

		scene.shaders.add(lambertian);
		scene.shaders.add(ambient);
		scene.shaders.add(phong);

		scene.worldObjects.add(grid);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(new Color(170, 223, 233));

		scene.render(panel, camera);
	}

	public void testBug() {
		MyDimension dim = new MyDimension(750, 750);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("BugTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(150);
		Matrix t = Matrix.translation(new Vector3(-20, 5, 0));
		Matrix rY = Matrix.rotationY((float) Math.PI * 0f);
		Matrix rZ = Matrix.rotationZ((float) -Math.PI * 0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);

		AmbientShader ambient = new AmbientShader(new Color(20, 20, 20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		PointLight light = new PointLight(new Color(170, 170, 140), new Vector3(-5, 15, -5));

		Sphere sphere = new Sphere(new Vector3(2, 3, 0), 1);
		sphere.diffuseColor = new Color(255, 255, 255);

		Sphere sphere02 = new Sphere(new Vector3(15, 1.75f, 2), 1);
		sphere02.diffuseColor = new Color(100, 255, 50);

		Sphere sphere03 = new Sphere(new Vector3(10, 1.5f, -2), 1.5f);
		sphere03.diffuseColor = new Color(255, 100, 100);
		
		CompactGrid grid = new CompactGrid();
		grid.addObject(sphere03);
		grid.addObject(sphere02);
		grid.addObject(sphere);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;

		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		
		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		scene.render(panel, camera);
	}
}
