package test;

import structs.MyDimension;
import structs.Vector2;
import utils.Rasterizer;
import utils.RenderPanel;

public class TestRasterizer {

	private RenderPanel panel;

	public TestRasterizer() {
		panel = new RenderPanel(new MyDimension(450, 450));
	}

	public void RenderOneTriangle() {
		Vector2 a = new Vector2(50, 50);
		Vector2 b = new Vector2(50, 400);
		Vector2 c = new Vector2(400, 50);
		Rasterizer.DrawTriangle(panel, a, b, c);
		
		panel.setTitle("RenderOneTriangle");
	}

	public void RenderTwoAdjacentTriangles() {

		Vector2 a = new Vector2(50, 50);
		Vector2 b = new Vector2(50, 400);
		Vector2 c = new Vector2(400, 50);
		Vector2 d = new Vector2(400, 400);
		Rasterizer.DrawTriangle(panel, a, b, c);
		Rasterizer.DrawTriangle(panel, d, b, c);

		panel.setTitle("RenderTwoAdjacentTriangles");
	}

	public void RenderTwoAdjacentTrianglesOnOffScreenPointLine() {
		Vector2 a = new Vector2(50, 50);
		Vector2 b = new Vector2(50, 400);
		Vector2 c = new Vector2(400, 50);
		Vector2 d = new Vector2(400, 400);
		Rasterizer.DrawTriangle(panel, a, b, d);
		Rasterizer.DrawTriangle(panel, a, c, d);

		panel.setTitle("RenderTwoAdjacentTrianglesOnOffscreenPointLine");
	}
}
