package test;

import java.awt.Color;
import java.awt.Dimension;

import lights.DirectionalLight;
import lights.PointLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.StandardValues;
import structs.Vector2;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;
import acceleration.CompactGrid;
import animation.AnimatedCamera;
import animation.AnimatedPointLight;
import animation.AnimatedTriangleMesh;
import animation.Animation;

public class TestAnimation {

	public void testBasics() {
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("AnimationTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20, 20));
		camera.setViewplaneDistance(25);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightdir = new Vector3(-1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(200, 200, 200));

		Sphere s01 = new Sphere(new Vector3(10, 0, 0), 1);
		s01.diffuseColor = new Color(255, 0, 0);
		Sphere s02 = new Sphere(new Vector3(0, 0, -10), 1);
		s02.diffuseColor = new Color(0, 0, 255);
		Sphere s03 = new Sphere(new Vector3(-10, 0, 0), 1);
		s03.diffuseColor = new Color(0, 255, 255);
		Sphere s04 = new Sphere(new Vector3(0, 0, 10), 1);
		s04.diffuseColor = new Color(0, 255, 0);

		CompactGrid grid = new CompactGrid();
		grid.addObject(s01);
		grid.addObject(s02);
		grid.addObject(s03);
		grid.addObject(s04);
		try {
			grid.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		AnimatedCamera animCam = new AnimatedCamera(camera);
		animCam.addRotationY((float) Math.PI * 2, 4000);

		Animation animation = new Animation(scene, camera, panel, 24, "test");
		animation.addAnimatable(animCam);

		animation.render(0, 4000);
	}

	public void testTriangleMesh() {
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("AnimationTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20, 20));
		camera.setViewplaneDistance(25);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightdir = new Vector3(1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(200, 200, 200));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/sphereMidRes");

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(mesh);
		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		Animation animation = new Animation(scene, camera, panel, 24, "MeshAnim");
		AnimatedTriangleMesh animMesh = new AnimatedTriangleMesh(mesh);
		animMesh.addPosition(new Vector3(10, 0, 0), 0);
		animMesh.addRotationY((float) Math.PI * 2, 2000);
		animation.addAnimatable(animMesh);
		animation.render(0, 2000);
	}

	public void testTriangleMeshInGrid() {
		MyDimension dim = new MyDimension(350, 350);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("AnimationTest");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDimensions(new Vector2(20, 20));
		camera.setViewplaneDistance(25);

		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightdir = new Vector3(1, -0.5f, -0.5f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(200, 200, 200));

		TriangleMesh mesh = MeshLoader.load("Data/OBJ/teapot");

		CompactGrid grid = new CompactGrid();
		grid.addObject(mesh);

		Scene scene = new Scene();
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		scene.backGround = new SimpleBackGround(StandardValues.BACKGROUNDCOLOR_01);

		Animation animation = new Animation(scene, camera, panel, 24, "MeshAnim");
		AnimatedTriangleMesh animMesh = new AnimatedTriangleMesh(mesh);
		animMesh.addPosition(new Vector3(10, -2, 0), 0);
		animMesh.addRotationY((float) Math.PI * 2, 2000);

		animation.addAnimatable(animMesh);
		animation.addCompactGrid(grid);

		animation.render(0, 2000);
	}

	public void testRodwen() {
		int size = 10;
		MyDimension dim = new MyDimension(50 * size, 85 * size);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("Rodwen animation");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(240);
		Matrix t = Matrix.translation(new Vector3(-35, 6, -50));
		Matrix rY = Matrix.rotationY((float) -Math.PI * 0.3f);
		Matrix world = t.multiply(rY);
		camera.setWorldMatrix(world);

		TriangleMesh robe = MeshLoader.load("Data/OBJ/Robe031");
		robe.diffuseColor = new Color(100, 100, 100);
		TriangleMesh belt = MeshLoader.load("Data/OBJ/Belt");
		belt.diffuseColor = new Color(50, 50, 50);
		TriangleMesh body = MeshLoader.load("Data/OBJ/Body03101");
		TriangleMesh hair = MeshLoader.load("Data/OBJ/hair052");
		hair.diffuseColor = new Color(50, 50, 50);
		TriangleMesh tiara = MeshLoader.load("Data/OBJ/Tiara");
		tiara.diffuseColor = new Color(255, 255, 255);
		
		TriangleMesh necklace = MeshLoader.load("Data/OBJ/Necklace");
		necklace.diffuseColor = new Color(150, 150, 150);
		necklace.reflective = true;
		necklace.specularColor = new Color(255,255,255);
		necklace.selfIllumination = 2;
		
		TriangleMesh eyes = MeshLoader.load("Data/OBJ/eyes");
		eyes.diffuseColor = new Color(150,150,150);
		eyes.reflective = true;
		eyes.specularColor = new Color(255,255,255);

		TriangleMesh sphere = MeshLoader.load("Data/OBJ/rodwenSphere");
		sphere.diffuseColor = new Color(255, 255, 0);
		sphere.specularIntensity = 64;
		sphere.reflective = true;
		sphere.specularColor = new Color(255, 150, 100);
		sphere.castsShadow = false;
		sphere.selfIllumination = 5;

		Sphere s01 = new Sphere(new Vector3(0, 7.85f, -2.5f), 0.5f);
		s01.diffuseColor = new Color(0, 255, 0);

		CompactGrid grid = new CompactGrid();
		grid.addObject(robe);
		grid.addObject(sphere);
		grid.addObject(body);
		grid.addObject(belt);
		grid.addObject(hair);
		grid.addObject(tiara);
		grid.addObject(eyes);
		grid.addObject(necklace);
		// grid.addObject(s01);

		LambertianShader lambertian = new LambertianShader();
		PhongShader phong = new PhongShader();

		Vector3 lightdir = new Vector3(1, -0.5f, 0.7f);
		lightdir.normalize();
		DirectionalLight light = new DirectionalLight(lightdir, new Color(0, 150, 255));

		Vector3 lightPos = new Vector3(0, 7.85f, -2.5f);
		PointLight pointLight = new PointLight(new Color(200, 150, 0), lightPos);
		PointLight pointLight02 = new PointLight(new Color(200, 150, 0), lightPos);

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
		scene.setAntialiasing(4);
		
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.lights.add(pointLight);
		scene.lights.add(pointLight02);
		scene.shaders.add(lambertian);
		scene.shaders.add(phong);
		scene.worldObjects.add(grid);
		
		Animation animation = new Animation(scene, camera, panel, 24, "Rodwen");

		AnimatedTriangleMesh robeAnim = new AnimatedTriangleMesh(robe);
		robeAnim.addRotationY((float) Math.PI * 2, 4000);
		AnimatedTriangleMesh tiaraAnim = new AnimatedTriangleMesh(tiara);
		tiaraAnim.addRotationY((float) Math.PI * 2, 4000);
		AnimatedTriangleMesh beltAnim = new AnimatedTriangleMesh(belt);
		beltAnim.addRotationY((float) Math.PI * 2, 4000);
		AnimatedTriangleMesh bodyAnim = new AnimatedTriangleMesh(body);
		bodyAnim.addRotationY((float) Math.PI * 2, 4000);
		AnimatedTriangleMesh hairAnim = new AnimatedTriangleMesh(hair);
		hairAnim.addRotationY((float) Math.PI * 2, 4000);
		AnimatedTriangleMesh sphereAnim = new AnimatedTriangleMesh(sphere);
		sphereAnim.addRotationY((float) Math.PI * 2, 4000);
		AnimatedTriangleMesh eyesAnim = new AnimatedTriangleMesh(eyes);
		eyesAnim.addRotationY((float) Math.PI * 2, 4000);
		AnimatedTriangleMesh necklaceAnim = new AnimatedTriangleMesh(necklace);
		necklaceAnim.addRotationY((float) Math.PI * 2, 4000);

		AnimatedPointLight pLightAnim01 = new AnimatedPointLight(pointLight);
		pLightAnim01.addPosition(lightPos, 0);
		pLightAnim01.addRotationY((float) Math.PI * 2, 4000);
		AnimatedPointLight pLightAnim02 = new AnimatedPointLight(pointLight02);
		pLightAnim02.addPosition(lightPos, 0);
		pLightAnim02.addRotationY((float) Math.PI * 2, 4000);

		animation.addAnimatable(robeAnim);
		animation.addAnimatable(sphereAnim);
		animation.addAnimatable(beltAnim);
		animation.addAnimatable(hairAnim);
		animation.addAnimatable(bodyAnim);
		animation.addAnimatable(tiaraAnim);
		animation.addAnimatable(necklaceAnim);
		animation.addAnimatable(eyesAnim);

		animation.addAnimatable(pLightAnim01);
		animation.addAnimatable(pLightAnim02);

		animation.addCompactGrid(grid);

		animation.render(0, 4000);
	}
}
