package test;

import java.awt.Color;

import lights.AreaLight;
import parser.MeshLoader;
import raytracing.PerspectiveCamera;
import shading.AmbientShader;
import shading.LambertianShader;
import shading.PhongShader;
import structs.Matrix;
import structs.MyDimension;
import structs.Vector3;
import utils.RenderPanel;
import utils.Scene;
import utils.SimpleBackGround;
import worldobjects.Sphere;
import worldobjects.TriangleMesh;

public class TestSoftShadows {
	
	public void testAreaLight()
	{
		Vector3 pos = new Vector3(0,5,-5);
		Vector3 v1 = new Vector3(0,2,0);
		Vector3 v2 = new Vector3(0,0,2);
		AreaLight light = new AreaLight(pos, pos.plus(v1), pos.plus(v2), new Color(200,200,200), 36);
		
		
		MyDimension dim = new MyDimension(750, 750);
		RenderPanel panel = new RenderPanel(dim);
		panel.setTitle("SoftShadows AreaLight Test");

		PerspectiveCamera camera = new PerspectiveCamera();
		camera.setPixelDimensions(dim);
		camera.setViewplaneDistance(150);
		Matrix t = Matrix.translation(new Vector3(-19,3.5f,20));
		Matrix rY = Matrix.rotationY((float)Math.PI*0.25f);
		Matrix rZ = Matrix.rotationZ((float)-Math.PI*0.02f);
		Matrix world = t.multiply(rY).multiply(rZ);
		camera.setWorldMatrix(world);
		
		AmbientShader ambient = new AmbientShader(new Color(20,20,20));
		LambertianShader lambert = new LambertianShader();
		PhongShader phong = new PhongShader();		

		TriangleMesh plane = MeshLoader.load("Data/OBJ/plane");
				
		Sphere sphere = new Sphere(new Vector3(2,1,-2), 1);
		sphere.diffuseColor = new Color(100,100,255);
		
		Sphere sphere02 = new Sphere(new Vector3(3,2f,0), 1);
		sphere02.diffuseColor = new Color(100,255,100);
		
		Sphere sphere03 = new Sphere(new Vector3(5,4f,-5), 1.5f);
		sphere03.diffuseColor = new Color(255,100,100);

		Scene scene = new Scene();
		scene.shadowsEnabled = true;
		scene.reflectionsEnabled = true;
		scene.reflectionsDepth = 5;
//		scene.setAntialiasing(4);
		
		scene.cameras.add(camera);
		scene.lights.add(light);
		scene.shaders.add(ambient);
		scene.shaders.add(lambert);
		scene.shaders.add(phong);
		scene.worldObjects.add(sphere03);
		scene.worldObjects.add(sphere02);
		scene.worldObjects.add(sphere);
		scene.worldObjects.add(plane);

		scene.backGround = new SimpleBackGround(new Color(20,20,20));

		scene.render(panel, camera);
	}

}
