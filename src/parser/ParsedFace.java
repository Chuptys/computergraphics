package parser;

import java.util.ArrayList;

public class ParsedFace {
	
	public ArrayList<Integer> verts = new ArrayList<Integer>();
	public ArrayList<Integer> texcoords = new ArrayList<Integer>();
	public ArrayList<Integer> normals = new ArrayList<Integer>();
	
	public void addEntry(int v, int vt, int vn)
	{
		verts.add(v);
		texcoords.add(vt);
		normals.add(vn);
	}
}
