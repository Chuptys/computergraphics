package parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import worldobjects.TriangleMesh;

public class MeshLoader {

	public static TriangleMesh load(String path) {
		TriangleMesh mesh = null;

		ObjParser objParser = new ObjParser();
		File objFile = new File(path + ".obj");
		FileInputStream objFileStream = null;

		try {
			objFileStream = new FileInputStream(objFile);
			objParser.parse(objFileStream);

			mesh = objParser.getParsedObject();
			parseMaterial(mesh, path);

		} catch (FileNotFoundException e) {
			System.err.println("Failed to load obj @ " +path);
		}

		System.out.println("Parsed file: " + path);
		return mesh;
	}

	private static void parseMaterial(TriangleMesh mesh, String path) {
		MtlParser mtlParser = new MtlParser();
		File mtlFile = new File(path + ".mtl");
		FileInputStream mtlFileStream = null;
		try {
			mtlFileStream = new FileInputStream(mtlFile);
			mtlParser.Parse(mtlFileStream, mesh, path);
		} catch (FileNotFoundException e) {
			System.err.println("Failed to load mtl @ "+path);
		}
	}
}
