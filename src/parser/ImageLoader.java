package parser;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader {

	
	public static BufferedImage loadImage(String path)
	{
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(path));
		} catch (IOException e) {
			System.err.println("Failed to load img @ " +path);
		}
		
		System.out.println("Loaded image: "+path);
		return img;
	}
}
