package parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import structs.Matrix;
import structs.Vector3;
import worldobjects.TriangleMesh;

public class ObjParser {

	private final String VERTEX = "v";
	private final String FACE = "f";
	private final String TEXCOORD = "vt";
	private final String NORMAL = "vn";	

	private ArrayList<Vector3> vertices = new ArrayList<Vector3>();
	private ArrayList<Vector3> normals = new ArrayList<Vector3>();
	private ArrayList<ParsedFace> faces = new ArrayList<ParsedFace>();
	private ArrayList<Vector3> texCoords = new ArrayList<Vector3>();

	public void parse(InputStream fileStream) {

		BufferedReader buffer = new BufferedReader(new InputStreamReader(fileStream));
		String line;

		try {
			while ((line = buffer.readLine()) != null) {
				StringTokenizer parts = new StringTokenizer(line, " ");
				int numTokens = parts.countTokens();
				if (numTokens == 0)
					continue;
				String type = parts.nextToken();

				if (type.equals(VERTEX)) {
					float x = Float.parseFloat(parts.nextToken());
					float y = Float.parseFloat(parts.nextToken());
					float z = Float.parseFloat(parts.nextToken());
					vertices.add(new Vector3(x, y, z));
				} else if (type.equals(FACE)) {

					ParsedFace face = new ParsedFace();

					String next;
					while (parts.hasMoreTokens()) {
						next = parts.nextToken();
						StringTokenizer subParts = new StringTokenizer(next, "/");
						int v = Integer.parseInt(subParts.nextToken());
						int vt = 0;
						int vn = 0;
						if (subParts.hasMoreTokens())
							vt = Integer.parseInt(subParts.nextToken());
						if (subParts.hasMoreTokens())
							vn = Integer.parseInt(subParts.nextToken());
						else
						{
							vn = vt;
							vt = 0;
						}
						face.addEntry(v, vt, vn);
					}

					faces.add(face);
				} else if (type.equals(TEXCOORD)) { // Note: 2D texCoords
													// assumed
					Vector3 texCoord = new Vector3(0, 0, 0);
					texCoord.X = Float.parseFloat(parts.nextToken());
					texCoord.Y = Float.parseFloat(parts.nextToken()) * -1f;
					texCoords.add(texCoord);
				} else if (type.equals(NORMAL)) {
					float x = Float.parseFloat(parts.nextToken());
					float y = Float.parseFloat(parts.nextToken());
					float z = Float.parseFloat(parts.nextToken());
					normals.add(new Vector3(x, y, z));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public TriangleMesh getParsedObject() {
		
		TriangleMesh mesh = new TriangleMesh(vertices.size(), normals.size(), faces.size(), texCoords.size());
		for (int i = 0; i < vertices.size(); i++) {
			mesh.oriVerts[i] = vertices.get(i);					
		}
		for(int l = 0; l<normals.size();l++)
		{
			normals.get(l).normalize();
			mesh.oriNormals[l] = normals.get(l);
		}
		for(int k=0; k< texCoords.size();k++)
		{
			mesh.texCoords[k] = texCoords.get(k);
		}
		
		for (int j = 0; j < faces.size(); j++) {
			ParsedFace f = faces.get(j);
			mesh.triangleVerts[j][0] = f.verts.get(0);
			mesh.triangleVerts[j][1] = f.verts.get(1);
			mesh.triangleVerts[j][2] = f.verts.get(2);
			mesh.triangleNormals[j][0] = f.normals.get(0);
			mesh.triangleNormals[j][1] = f.normals.get(1);
			mesh.triangleNormals[j][2] = f.normals.get(2);
			mesh.triangleTexCoords[j][0] = f.texcoords.get(0);
			mesh.triangleTexCoords[j][1] = f.texcoords.get(1);
			mesh.triangleTexCoords[j][2] = f.texcoords.get(2);
		}

		mesh.setWorldMatrix(Matrix.identity());

		return mesh;
	}

}
