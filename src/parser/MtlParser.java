package parser;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import worldobjects.TriangleMesh;

public class MtlParser {

	private final String DIFFUSE_COLOR = "\tKd";
	private final String DIFFUSE_TEX_MAP = "\tmap_Kd";

	public void Parse(InputStream fileStream, TriangleMesh mesh, String path) {

		BufferedReader buffer = new BufferedReader(new InputStreamReader(fileStream));
		String line;

		try {
			while ((line = buffer.readLine()) != null) {
				String[] parts = line.split(" ");
				if (parts.length == 0)
					continue;
				String type = parts[0];

				if (type.equals(DIFFUSE_COLOR)) {
					Color diffuseColor = new Color(Float.parseFloat(parts[1]), Float.parseFloat(parts[2]), Float.parseFloat(parts[3]));
					mesh.diffuseColor = diffuseColor;
				} 
				else if (type.equals(DIFFUSE_TEX_MAP)) {
					if (parts.length > 1) {
						
						
						Path dir = Paths.get(path);
						String texturePath = dir.getParent() + "\\" + parts[1];
						texturePath =  texturePath.replace("\\", "/");
						
						BufferedImage img = ImageIO.read(new File(texturePath));
						mesh.setDiffuseMap(img); //Assumes only one diffuse texture per mtl						
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
