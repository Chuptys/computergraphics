package postprocessing;

import java.awt.Color;
import java.awt.image.BufferedImage;

import structs.Vector2;

public class ImageSampler {

	private BufferedImage img;
	
	public ImageSampler(BufferedImage sourceImg)
	{
		img = sourceImg;
	}
	
	public int getWidth()
	{
		return img.getWidth();
	}
	
	public int getHeight()
	{
		return img.getHeight();
	}
	
	public Color getSample(Vector2 coord)
	{
		return new Color(img.getRGB((int)coord.X, (int)coord.Y));
	}
}
