package lights;

import java.awt.Color;

import structs.Vector3;

public interface ILight {
	
	/**
	 * Returns the color of this light.
	 * @return
	 */
	public Color getColor();
	
	/**
	 * Returns the direction from given point to the light.
	 * @param point
	 * @return
	 */
	public Vector3 getLightDirection(Vector3 point);

	/**
	 * Returns the distance between given point and this light.
	 * @param point
	 * @return
	 */
	public float getDistanceToLight(Vector3 point);
	
}
