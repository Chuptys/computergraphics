package lights;

import java.awt.Color;
import java.util.Random;

import structs.Vector3;

public class AreaLight implements ILight{

	private Vector3 position;
	private Vector3 a;
	private Vector3 b;
	
	private Color color;
	public int nbSamples;
	private Random randomGenerator;
	
	private Vector3 currentRandomAreaPoint;
	
	/**
	 * Creates an arealight at position pos with area spanned by vectors v1 & v2.
	 */
	public AreaLight(Vector3 pos, Vector3 v1, Vector3 v2, Color c, int nbSamples)
	{
		position = pos;
		a = v1;
		b = v2;
		color = c;
		this.nbSamples = nbSamples;
		randomGenerator = new Random();
	}
	
	@Override
	public Color getColor() {
		return color;
	}
	
	@Override
	public Vector3 getLightDirection(Vector3 point) {
		generateRandomPointOnArea();
		
		Vector3 ret = currentRandomAreaPoint.plus(point.multiply(-1));
		ret.normalize();
		return ret;
	}

	/**
	 * Returns distance to the random point generated when calling getLightDirection(Vector3 point)
	 */
	@Override
	public float getDistanceToLight(Vector3 point) {
		float xd = point.X - currentRandomAreaPoint.X;
		float yd = point.Y - currentRandomAreaPoint.Y;
		float zd = point.Z - currentRandomAreaPoint.Z;
		return (float) Math.sqrt(xd * xd + yd * yd + zd * zd);
	}
	
	private void generateRandomPointOnArea()
	{
		float ksi01 = randomGenerator.nextFloat();
		float ksi02 = randomGenerator.nextFloat();
		
		currentRandomAreaPoint = position.plus(a.multiply(ksi01).plus(b.multiply(ksi02)));
	}

}
