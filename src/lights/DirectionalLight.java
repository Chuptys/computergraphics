package lights;

import java.awt.Color;

import structs.Vector3;

public class DirectionalLight implements ILight{

	private Vector3 lightDirection;
	private Color color;
	
	public DirectionalLight(Vector3 lightDir, Color c)
	{
		lightDir.normalize();
		lightDirection = lightDir;
		color = c;
	}
	
	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public Vector3 getLightDirection(Vector3 point) {
		return lightDirection.multiply(-1);
	}

	@Override
	public float getDistanceToLight(Vector3 point) {
		return Integer.MAX_VALUE;
	}

}
