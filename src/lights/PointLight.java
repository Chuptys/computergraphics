package lights;

import java.awt.Color;

import structs.Vector3;

public class PointLight implements ILight {

	private Color color;
	private Vector3 position;

	public PointLight(Color c, Vector3 pos) {
		color = c;
		position = pos;
	}
	
	public void setPosition(Vector3 v)
	{
		position = v;
	}

	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public Vector3 getLightDirection(Vector3 point) {
		Vector3 ret = position.plus(point.multiply(-1));
		ret.normalize();
		return ret;
	}

	@Override
	public float getDistanceToLight(Vector3 point) {
		float xd = point.X - position.X;
		float yd = point.Y - position.Y;
		float zd = point.Z - position.Z;
		return (float) Math.sqrt(xd * xd + yd * yd + zd * zd);
	}
}
