package raytracing;

import structs.Vector3;

public class Ray {
	
	public Vector3 Direction; //should be unit!
	public Vector3 Origin;
	public boolean isShadowRay;
	
	public void setDirection(Vector3 dir)
	{
		Direction = dir;
		Direction.normalize();
	}
}
