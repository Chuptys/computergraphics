package raytracing;

import java.awt.Color;

import structs.StandardValues;
import structs.Vector3;

public class RaycastResult {

	public Ray ray;
	public float distance;
	public Color diffuseColor;
	public Color specularColor;
	public float specularIntensity;
	public boolean reflective;
	public boolean refractive;
	public Vector3 refractiveAttenuation;
	public float refractionIndex;
	public float selfllumination = 1;	
	public Vector3 normal;	
	public boolean glossy;
	public float glossAmount;
	public Vector3[] orthnormBasis = new Vector3[2];
	
	public RaycastResult(float dist, Ray ray)
	{
		distance = dist;	
		this.ray = ray;
		diffuseColor = StandardValues.DIFFUSE_COLOR;
		specularColor = StandardValues.SPECULAR_COLOR;	
		specularIntensity = StandardValues.SPECULAR_INTENSITY;
	}
	
	public RaycastResult()
	{
		distance = Integer.MAX_VALUE;
		ray = null;
		diffuseColor = StandardValues.DIFFUSE_COLOR;
		specularColor = StandardValues.SPECULAR_COLOR;	
		specularIntensity = StandardValues.SPECULAR_INTENSITY;
	}
	
	public void copyResultTo(RaycastResult copy)
	{
		copy.ray = ray;
		copy.distance = distance;	
		copy.diffuseColor = diffuseColor;
		copy.specularColor = specularColor;
		copy.specularIntensity = specularIntensity;
		copy.reflective = reflective;
		copy.selfllumination = selfllumination;
		copy.normal = normal;	
		copy.refractionIndex = refractionIndex;
		copy.refractive = refractive;
		copy.refractiveAttenuation = refractiveAttenuation;
		copy.glossy = glossy;
		copy.glossAmount = glossAmount;		
		copy.orthnormBasis[0] = orthnormBasis[0];
		copy.orthnormBasis[1] = orthnormBasis[1];
	}
}
