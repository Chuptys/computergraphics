package raytracing;

import java.util.Random;

import structs.Matrix;
import structs.MyDimension;
import structs.Vector2;
import structs.Vector3;

public class PerspectiveCamera implements ICamera {

	private Vector3 e;
	private Vector3 u;
	private Vector3 v;
	private Vector3 w;
	private Vector3 oriU;
	private Vector3 oriV;
	private Vector3 oriW;
	float l;
	float r;
	float t;
	float b;
	// private float viewplaneWidth;
	// private float viewplaneHeight;
	private float viewplaneDistance;
	private float pixelDimX;
	private float pixelDimY;
	
	private float eyeDimUp; //Dim of the eye plane in the up-direction
	private float eyeDimPar; //Dim of the eye plane in the direction parallel to the viewplane
	private int nbSamples = 1;
	private Random rg;

	public PerspectiveCamera() {
		u = new Vector3(0,0,1);
		oriU = new Vector3(0,0,1);
		v = new Vector3(0, 1, 0);
		oriV = new Vector3(0, 1, 0);
		w = new Vector3(-1,0,0);
		oriW = new Vector3(-1,0,0);
		setWorldMatrix(Matrix.identity());
		
		r = 25;
		l = -r;
		t = 25;
		b = -t;
		viewplaneDistance = 5;
		pixelDimX = 100;
		pixelDimY = 100;
		
		rg = new Random();
	}

	@Override
	public Ray[] getRay(float row, float column) {
		if (column > pixelDimX || row > pixelDimY)
			throw new IllegalArgumentException();		
		
		if(nbSamples == 1)
		{
			Ray[] ret = {generateRay(row, column, new Vector3(0,0,0))};
			return ret;
		}
		
		Ray[] ret = new Ray[nbSamples];
		Vector3 eyeOffset;
		for(int i=0; i<nbSamples;i++)
		{
			eyeOffset = v.multiply(eyeDimUp*rg.nextFloat()).plus(u.multiply(eyeDimPar*rg.nextFloat()));
			ret[i] = generateRay(row, column, eyeOffset);
		}
		
		return ret;
	}
	
	private Ray generateRay(float row, float column, Vector3 eyeOffset)
	{
		float uf = (float) (l + (r - l) * (column + 0.5) / pixelDimX);
		float vf = (float) (b + (t - b) * (row + 0.5) / pixelDimY);
		
		Ray ray = new Ray();
		ray.Origin = e.plus(eyeOffset);
		Vector3 v1 = w.multiply(-viewplaneDistance);
		Vector3 v2 = u.multiply(uf);
		Vector3 v3 = v.multiply(vf);
		ray.setDirection((v1.plus(v2)).plus(v3).minus(eyeOffset));
		return ray;
	}

	@Override
	public void setWorldMatrix(Matrix m) {
		e = m.getTranslation();
		Matrix r = m.getRotation();
		u = oriU.transform(r);
		v = oriV.transform(r);
		w = oriW.transform(r);
	}

	@Override
	public void setViewplaneDimensions(Vector2 dim) {
		// viewplaneHeight = dim.Y;
		// viewplaneWidth = dim.X;
		r = dim.X / 2;
		l = -r;
		t = dim.Y / 2;
		b = -t;
	}

	@Override
	public void setPixelDimensions(MyDimension dim) {
		pixelDimX = dim.width;
		pixelDimY = dim.height;
		setViewplaneDimensions(new Vector2(2*r, 2*t*dim.height/dim.width));
	}

	public void setViewplaneDistance(float focalLength) {
		viewplaneDistance = focalLength;
	}
	
	public void setDepthOfField(float maxOffset, int resolution)
	{
		eyeDimUp = maxOffset;
		eyeDimPar = maxOffset;
		nbSamples = resolution;
	}
	
	public void setFOV(float fov)
	{
		r*= fov;
		l = -r;
		t*=fov;
		b=-t;
	}

}
