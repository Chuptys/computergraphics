package raytracing;

import structs.Matrix;
import structs.MyDimension;
import structs.Vector2;

public interface ICamera {
	
	public void setWorldMatrix(Matrix m);
	public void setViewplaneDimensions(Vector2 dim);	
	public void setPixelDimensions(MyDimension dim);
	
	/*
	 * Returns a ray given the position of the pixel to render.
	 */
	public Ray[] getRay(float f, float g);
	
	
}
