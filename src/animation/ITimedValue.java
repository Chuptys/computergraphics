package animation;

public interface ITimedValue {

	public float getTime();
}
