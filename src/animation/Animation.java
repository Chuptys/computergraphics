package animation;

import java.util.ArrayList;

import acceleration.CompactGrid;

import raytracing.ICamera;
import scenegraph.SceneGraphNode;

import utils.RenderPanel;
import utils.Scene;

/**
 * Notes: The scene must contain all objects (also the ones to animate) to be
 * rendered. All objects that must be animated must have been added as
 * IAnimatable to the animation
 * 
 * @author Simon
 * 
 */
public class Animation {

	private int fps;
	private Scene scene;
	private ICamera camera;
	private RenderPanel renderPanel;
	private String name;
	private ArrayList<IAnimatable> animatables = new ArrayList<IAnimatable>();
	private ArrayList<CompactGrid> grids = new ArrayList<CompactGrid>();
	private ArrayList<SceneGraphNode> scenegraphNodes = new ArrayList<SceneGraphNode>();

	private int totalNbFrames;

	public Animation(Scene s, ICamera cam, RenderPanel rp, int framesPerSec, String name) {
		scene = s;
		scene.printInfo = false;
		camera = cam;
		renderPanel = rp;
		fps = framesPerSec;
		this.name = name;
	}

	public void render(float startTime, float endTime) {
		int index = 1;

		totalNbFrames = (int) ((endTime - startTime) * 0.001 * fps);
		float inc = (float) (1000f / fps);

		for (float currentTime = startTime; currentTime <= endTime; currentTime += inc) {
			for (IAnimatable a : animatables)
				a.updateAtTime(currentTime);
			
			for(SceneGraphNode n :scenegraphNodes)
				n.update();

			for (CompactGrid grid : grids) {
				try {
					grid.rebuild();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			StringBuilder nb = new StringBuilder("0000");
			String indexStr = String.valueOf(index);
			for (int i = 0; i < indexStr.length(); i++) {
				nb.setCharAt(nb.length() - indexStr.length() + i, indexStr.charAt(i));
			}

			System.out.println("Rendering frame " + index + " out of " + totalNbFrames);
			scene.render(renderPanel, camera, "Data/animation/" + name + nb + ".png");

			index++;
			sleep(100); // for avoiding CgPanel error
		}

	}

	/**
	 * Add all compactgrids that contain animated objects here
	 * 
	 * @param grid
	 */
	public void addCompactGrid(CompactGrid grid) {
		grids.add(grid);
	}
	
	public void addAnimatable(IAnimatable a)
	{
		animatables.add(a);
	}
	
	public void addSceneGraphNode(SceneGraphNode n)
	{
		scenegraphNodes.add(n);
	}
	
	private void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
