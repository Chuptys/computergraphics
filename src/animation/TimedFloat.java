package animation;


public class TimedFloat implements ITimedValue{

	public float value;
	public float time;
	
	public TimedFloat(float v, float t)
	{
		value = v;
		time = t;
	}
	
	@Override
	public float getTime() {
		return time;
	}
	
	public float getValue()
	{
		return value;
	}
}
