package animation;

import scenegraph.SceneGraphNode;

public class AnimatedSceneGraphNode extends AnimatedObject implements IAnimatable {

	private SceneGraphNode node;
	
	public AnimatedSceneGraphNode(SceneGraphNode sgNode)
	{
		super();
		node = sgNode;
		updateAtTime(0);
	}
	
	@Override
	public void updateAtTime(float time) {
		node.setLocalMatrix(getWorldMatrixAtTime(time));		
	}

}
