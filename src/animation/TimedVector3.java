package animation;

import structs.Vector3;

public class TimedVector3 implements ITimedValue {

	private Vector3 value;
	private float time;
	
	public TimedVector3(Vector3 v, float t)
	{
		value = v;
		time = t;
	}

	@Override
	public float getTime() {
		return time;
	}
	
	public Vector3 getValue()
	{
		return value;
	}
}
