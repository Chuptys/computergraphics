package animation;

import lights.PointLight;
import structs.Matrix;
import structs.Vector3;

/**
 * Setting a yRotation rotates the position around Y-axis (so first translate, then rotate)
 * (is ez for making circular paths)
 * @author Simon
 *
 */
public class AnimatedPointLight extends AnimatedObject implements IAnimatable{

	private PointLight object;
	
	public AnimatedPointLight(PointLight light) {
		super();
		object = light;
		updateAtTime(0);
	}	
	
	@Override
	public void updateAtTime(float time) {
		float rot = getYRotationAtTime(time);
		Matrix rotY = Matrix.rotationY(rot);
		Vector3 pos = getPositionAtTime(time).transform(rotY);
		
		object.setPosition(pos);		
	}
	
	

}
