package animation;

public interface IAnimatable {
	public void updateAtTime(float time);
}
