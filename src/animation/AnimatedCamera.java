package animation;

import java.util.ArrayList;

import raytracing.PerspectiveCamera;
import structs.Matrix;

public class AnimatedCamera extends AnimatedObject implements IAnimatable{
	
	private PerspectiveCamera object;
	private ArrayList<ITimedValue> yRotationsAfter = new ArrayList<ITimedValue>();

	public AnimatedCamera(PerspectiveCamera camera)
	{
		super();
		yRotationsAfter.add(new TimedFloat(0, 0));
		object = camera;		
		updateAtTime(0);
	}
	
	@Override
	public void updateAtTime(float time) {
		Matrix oriWorld = getWorldMatrixAtTime(time);	
		Matrix yRotAfter = Matrix.rotationY(getYRotationAfterAtTime(time));
		object.setWorldMatrix(yRotAfter.multiply(oriWorld));
	}
	
	public void addRotationYAfter(float rot, float time) {
		if (time < 0)
			return;

		removeEntriesAtTime(yRotationsAfter, time);
		yRotationsAfter.add(new TimedFloat(rot, time));
	}
	
	private float getYRotationAfterAtTime(float time) {
		TimedFloat t1 = (TimedFloat) getEntryJustBefore(yRotationsAfter, time);
		TimedFloat t2 = (TimedFloat) getEntryJustAfter(yRotationsAfter, time);

		if (t2 != null) {
			float percent = (time - t1.getTime()) / (t2.getTime() - t1.getTime());
			float diff = percent * (t2.getValue() - t1.getValue());

			return t1.getValue() + diff;
		} else
			return t1.getValue();
	}
	
	

}
