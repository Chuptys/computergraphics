package animation;

import worldobjects.Sphere;

public class AnimatedSphere extends AnimatedObject implements IAnimatable{

	private Sphere object;

	public AnimatedSphere(Sphere sphere) {
		super();
		object = sphere;
		updateAtTime(0);
	}

	@Override
	public void updateAtTime(float time) {
		object.center = getPositionAtTime(time);
	}

}
