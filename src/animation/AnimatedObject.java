package animation;

import java.util.ArrayList;

import structs.Matrix;
import structs.Vector3;

/**
 * All timing are in milliseconds
 * 
 * @author Simon
 * 
 */
public class AnimatedObject {

	private ArrayList<ITimedValue> positions = new ArrayList<ITimedValue>();
	private ArrayList<ITimedValue> xRotations = new ArrayList<ITimedValue>();
	private ArrayList<ITimedValue> yRotations = new ArrayList<ITimedValue>();
	private ArrayList<ITimedValue> zRotations = new ArrayList<ITimedValue>();

	public AnimatedObject() {
		positions.add(new TimedVector3(new Vector3(0, 0, 0), 0));
		xRotations.add(new TimedFloat(0, 0));
		yRotations.add(new TimedFloat(0, 0));
		zRotations.add(new TimedFloat(0, 0));
	}

	public void addPosition(Vector3 pos, float time) {
		if (time < 0)
			return;

		removeEntriesAtTime(positions, time);
		positions.add(new TimedVector3(pos, time));
	}

	public void addRotationX(float rot, float time) {
		if (time < 0)
			return;

		removeEntriesAtTime(xRotations, time);
		xRotations.add(new TimedFloat(rot, time));
	}

	public void addRotationY(float rot, float time) {
		if (time < 0)
			return;

		removeEntriesAtTime(yRotations, time);
		yRotations.add(new TimedFloat(rot, time));
	}

	public void addRotationZ(float rot, float time) {
		if (time < 0)
			return;

		removeEntriesAtTime(zRotations, time);
		zRotations.add(new TimedFloat(rot, time));
	}

	protected void removeEntriesAtTime(ArrayList<ITimedValue> list, float time) {

		for (int i = 0; i < list.size(); i++) {
			ITimedValue current = list.get(i);
			if (current.getTime() == time) {
				list.remove(i);
				i--;
			}
		}

	}

	public Vector3 getPositionAtTime(float time) {
		TimedVector3 v1 = (TimedVector3) getEntryJustBefore(positions, time);
		TimedVector3 v2 = (TimedVector3) getEntryJustAfter(positions, time);

		if (v2 != null) {
			float percent = (time - v1.getTime()) / (v2.getTime() - v1.getTime());
			float xDiff = percent * (v2.getValue().X - v1.getValue().X);
			float yDiff = percent * (v2.getValue().Y - v1.getValue().Y);
			float zDiff = percent * (v2.getValue().Z - v1.getValue().Z);

			return new Vector3(v1.getValue().X + xDiff, v1.getValue().Y + yDiff, v1.getValue().Z + zDiff);
		} else
			return v1.getValue();
	}

	public float getXRotationAtTime(float time) {
		TimedFloat t1 = (TimedFloat) getEntryJustBefore(xRotations, time);
		TimedFloat t2 = (TimedFloat) getEntryJustAfter(xRotations, time);

		if (t2 != null) {
			float percent = (time - t1.getTime()) / (t2.getTime() - t1.getTime());
			float diff = percent * (t2.getValue() - t1.getValue());

			return t1.getValue() + diff;
		} else
			return t1.getValue();
	}

	public float getYRotationAtTime(float time) {
		TimedFloat t1 = (TimedFloat) getEntryJustBefore(yRotations, time);
		TimedFloat t2 = (TimedFloat) getEntryJustAfter(yRotations, time);

		if (t2 != null) {
			float percent = (time - t1.getTime()) / (t2.getTime() - t1.getTime());
			float diff = percent * (t2.getValue() - t1.getValue());

			return t1.getValue() + diff;
		} else
			return t1.getValue();
	}

	public float getZRotationAtTime(float time) {
		TimedFloat t1 = (TimedFloat) getEntryJustBefore(zRotations, time);
		TimedFloat t2 = (TimedFloat) getEntryJustAfter(zRotations, time);

		if (t2 != null) {
			float percent = (time - t1.getTime()) / (t2.getTime() - t1.getTime());
			float diff = percent * (t2.getValue() - t1.getValue());

			return t1.getValue() + diff;
		} else
			return t1.getValue();
	}

	public Matrix getWorldMatrixAtTime(float time) {
		Matrix xRot = Matrix.rotationX(getXRotationAtTime(time));
		Matrix yRot = Matrix.rotationY(getYRotationAtTime(time));
		Matrix zRot = Matrix.rotationZ(getZRotationAtTime(time));
		Matrix pos = Matrix.translation(getPositionAtTime(time));
		Matrix ret = pos.multiply(yRot.multiply(zRot.multiply(xRot)));
		return ret;
	}

	protected ITimedValue getEntryJustAfter(ArrayList<ITimedValue> list, float time) {
		float diff = Float.MAX_VALUE;
		ITimedValue ret = null;

		for (ITimedValue value : list) {
			float cDiff = value.getTime() - time;
			if (cDiff > 0 && cDiff < diff) {
				diff = cDiff;
				ret = value;
			}
		}
		return ret;
	}

	protected ITimedValue getEntryJustBefore(ArrayList<ITimedValue> list, float time) {
		float diff = Float.NEGATIVE_INFINITY;
		ITimedValue ret = null;

		for (ITimedValue value : list) {
			float cDiff = value.getTime() - time;
			if (cDiff <= 0 && cDiff > diff) {
				diff = cDiff;
				ret = value;
			}
		}
		return ret;
	}
}
