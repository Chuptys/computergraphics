package animation;

import worldobjects.TriangleMesh;

public class AnimatedTriangleMesh extends AnimatedObject implements IAnimatable{

	private TriangleMesh object;
	
	public AnimatedTriangleMesh(TriangleMesh mesh)
	{
		super();
		object = mesh;
		updateAtTime(0);
	}
	
	@Override
	public void updateAtTime(float time)
	{
		object.setWorldMatrix(getWorldMatrixAtTime(time));
	}
}
